side<?php 
session_start();

require_once "authCookieSessionValidate.php";

if(!$isLoggedIn) {
    header("Location: ./");
}
date_default_timezone_set('Asia/Riyadh');
$today=date("Y-m-d");

?>

  <!-- Navbar -->
<?php
require 'sidebar.php';


 
if (isset($_POST['transfer_pt_btn'])) {
  // receive all input values from the form
 $transfer_id = $_POST['id'];
  $specialty_transfer = $_POST['specialty_transfer'];
  if ($specialty_transfer =="ICU"){
        // echo" ICU submitted";
        $query = "UPDATE  picupatients SET  DISDATE='".$today."', MORTALITY='Alive', DISTO='ICU'  WHERE ID='".$transfer_id."'";
          if (!$mysqli -> query( $query)) {
            echo("Error description: " . $mysqli -> error);
          }
  }
        else
    {
         $constulant_transfer =  $_POST['constulant_transfer'];


         $formationSQL = "SELECT * FROM picupatients WHERE ID='".$transfer_id."'";
         $result1 = $mysqli->query($formationSQL);
         $patient = $result1 -> fetch_array(MYSQLI_ASSOC);
        
        //  var_dump($patient );

         $patient['consultant_id']= $constulant_transfer;
         $patient['newassign']="1";
         $patient['ADMDATE']=$today;
    
/// transfer to new doctor

          $query = "INSERT INTO picupatients (MRN, PNAME, ADMDATE, ADMFROM, admissiondiagnosis, BED, nationality, gender, consultant_id, age, newassign)
           VALUES ('".$patient['MRN']."','".$patient['PNAME']."','".$patient['ADMDATE']."','".$patient['ADMFROM']."',
           '".$patient['admissiondiagnosis']."','".$patient['BED']."','".$patient['nationality']."','".$patient['gender']."','".$patient['consultant_id']."',
           '".$patient['age']."','".$patient['newassign']."') ";

        //   mysqli_query($mysqli, $query);

          if (!$mysqli -> query( $query)) {
            echo("Error description: " . $mysqli -> error);

           


          }
          $query = "UPDATE  picupatients SET  DISDATE='".$today."', MORTALITY='Alive', DISTO='Internal Transfer'  WHERE ID='".$transfer_id."'";
          if (!$mysqli -> query( $query)) {
            echo("Error description: " . $mysqli -> error);
          }
        //   // header('location: PICU-patients.php');
    }
}

  if (!in_array($user['position'],$access_PICU_patients)){
    
    echo "
    <div class='content-wrapper'>
    
  
    <section class='content'>
    <div class='container-fluid'>  
    <div class='alert alert-danger' role='alert'> you dont have permission to access this page, Contact you manager if you need to.
    </div>
    </div>
    </section>
    </div>
    ";
    require 'footer.php';

    exit();
  }
?>

     <style>
          .hidden{
                  display: none;
          }
          textarea {
    resize: none;
    overflow: hidden;
}

      </style>

        <script>


function auto_grow(element) {
    element.style.height = "5px";
    element.style.height = (element.scrollHeight)+"px";
}

	</script>
      
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
  
	<?php
   		
		$formationSQL = "SELECT * FROM picupatients WHERE DISDATE IS NULL";
		$result1 = $mysqli->query($formationSQL);
		$activepicupatints = $result1 -> fetch_all(MYSQLI_ASSOC);

    $formationSQL = "SELECT * FROM countries";
		$result1 = $mysqli->query($formationSQL);
		$countries = $result1 -> fetch_all(MYSQLI_ASSOC);

    $formationSQL = "SELECT * FROM members WHERE position = '3'";
		$result1 = $mysqli->query($formationSQL);
		$consultants = $result1 -> fetch_all(MYSQLI_ASSOC);
	?>



    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">DMC Patient List</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="dashboard.php">Home</a></li>
              <li class="breadcrumb-item active">DMC Patient List</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">  
      

<div class="row">

 <div id="mypresentersTable" class="col-md-12">

            <!-- /.info-box -->
            <?php 
                   $consultant_count=array();
                 
                   
            
            foreach ($consultants as $consultant){
              
              $old_n=0;
              $new_n=0;
              ?>




            <div class="card">
              <div  class="card-header">
                <h3 class="card-title"><i class="fas fa-user-tie text-info"></i> Dr. <?php echo $consultant['full_name'];  ?> Patient List</h3>
                <div id="addbtn" class='eachrow' style=' float: right; '>
  
                <!-- <a  class='btn btn-success'  href='#admiting_modal' data-toggle='modal'  style='color: aliceblue; line-height: 2;padding: 0px 15px;'>Add New Patient</a> -->
                  </td>
        
          </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <div class="row">
         
                          <table class="col-md-12" >
                            <thead   style="text-align: center;font-weight: 700;">
                            <tr>
                              <td class="col-md-1" >Bed #</td>
                              <td class="col-md-2">MRN</td>
                              <td class="col-md-2">Patient Name</td>
                              <td class="col-md-2">Admission Date</td>
                              <td class="col-md-1">LOS</td>
                              <td class="col-md-3">Diagnosis</td>
                              <td class="col-md-1"></td>
                            </tr>
                            </thead>
                                         <?php

                                        
                                                     foreach($activepicupatints as $s){

                                                           

                                                      if($s['consultant_id'] == $consultant['member_id'] ){

                                                        if(is_null($s['newassign'])){
                                                          $old_n++;
                    
                                                        } elseif ($s['newassign']=='1'){
                                                          $new_n++;
                                                        }

                                                      $decodedadmissiondx=json_decode($s['admissiondiagnosis']);
                                                  
                                                      $today = date("Y-m-d");
                                                      $today1 = strtotime(date("Y-m-d"));
                                                      // $datediff = $today - strtotime($s['ADMDATE']);
                                                      // $LOS = $today->date_diff($s['ADMDATE'])->format("%a");
                                                      $timeDiff = abs($today1 - strtotime($s['ADMDATE']));

                                                      $LOS = $timeDiff/86400;  // 86400 seconds in one day

                                                      // $LOS= round($datediff / (60 * 60 * 24));
                                                      

                                                      
                                                    echo"  
                                                   
                                                    <tr class='eachrow'  id='row".$s['ID']."'>
                                                    
                                                      <td style='  padding: 0px 1%; text-align: center;' class='eachcol bed'  scope='row' >";
                                                        if ($s['newassign']=='1'){
                                                          echo"<p><strong>New</strong></p>";
                                                        
                                                        }

                                                echo"
                                                      <input class='txtdata' name='bed' placeholder='Bed Number' value='".$s['BED']."' style='text-align: center;' >
                                                      <input class='txtdata' type='hidden' name='id' id='id' value='".$s['ID']."' style='text-align: center;width: 85%;' >
                                                     
                                                      </td>
                                                      
                                                      <td style='  padding: 0px 1%;' class='eachcol mrn' >
                                                        <input class='txtdata' name='mrn' value='".$s['MRN']."' style='text-align: center;'>
                                                      </td>

                                                      <td style='  padding: 0px 1%;' class='eachcol name'>
                                                      <input class='txtdata' name='name' value='".$s['PNAME']."' style='text-align: center;'>
                                                      </td>
                                                
                                                      <td style='  padding: 0px 1%;' class='eachcol admdate'  scope='row' >
                                                      <input type='text' onfocus='(this.type='date')' class='txtdata' name='admdate'  id='datepicker' value='".$s['ADMDATE']."' style='text-align: center;padding: 0px;' disabled>
                                                      </td>";

                                                      if ($LOS < 5){
                                                        echo"   <td style='  padding: 0px 1%;' class='eachcol admdate'    scope='row' >";
                                                      } elseif   ($LOS > 10){
                                                        echo"   <td style='  padding: 0px 1%; background: #f8d7da;' class='eachcol admdate'   scope='row' >";
                                                      }elseif   ($LOS >= 5){
                                                        echo"   <td style='  padding: 0px 1%; background: #fff3cd;' class='eachcol admdate'    scope='row' >";
                                                      }
                                                    

                                                      echo"
                                                      <input type='text' class='txtdata'  value='".$LOS."' style='text-align: center;padding: 0px;' disabled>
                                                      </td>
                                                      ";
                                                   
                                                 echo"
                                                      <td style='  padding: 0px 1%;' class='eachcol admissiondiagnosis'>
                                                      <select class='txtdata ddxname form-control' style='width: 100%;'  oninput='auto_grow(this)'  multiple='multiple' name='admissiondiagnosis'>
                                                      ";
                                                
                                                      if (is_array($decodedadmissiondx)){
                                                        
                                                        foreach($decodedadmissiondx as $key => $value)
                                                  {
                                                    $formationSQL = "SELECT * FROM icd10 WHERE id='".$value."'";
                                                    $result1 = $mysqli->query($formationSQL);
                                                    $dxlist = $result1 -> fetch_array(MYSQLI_ASSOC);
                                                     
                                                
                                                      echo '<option selected value="' . $dxlist['id'] . '">'.  $dxlist['name']. '</option>';
                                                  }}
                                                
                                                      echo"
                                                      </select></td>
                                                
                                                
                                                
                                                   <td style='  padding: 0px 1%;text-align: center;' class='eachcol id' scope='row' ><input class='txtdata' name='id' value='".$s['ID']."' style='display: none;'>
                                                   ";
                                                   if (in_array($user['position'],$access_PICU_control)){
                                                   echo "<a class='btn btn-info' href='#details_modal' data-book-id='".$s['ID']."' data-toggle='modal'  style='color: aliceblue;line-height: 2;margin-top: 3%;padding: 0px 10%;'>Modify</a>";
                                                   };
                                                  
                                                   if ($user['member_id'] == $s['consultant_id'] OR in_array($user['position'],$access_PICU_control)){
                                                   echo"
                                                   <a  class='btn btn-success'  href='#transfer_modal' data-toggle='modal'  data-book-id='".$s['ID']."'  style='color: aliceblue;line-height: 2;margin-top: 3%;padding: 0px 10%;'  id= 'Transfer'>Transfer</a>
                                                   <a  class='btn btn-danger'  href='#my_modal' data-toggle='modal'  data-book-id='".$s['ID']."'  style='color: aliceblue;line-height: 2;margin-top: 3%;padding: 0px 10%;'  id= 'discharge'>Discharge</a>
                                                   ";
                                                  }

                                                   echo"  </td>
                                                  </tr >
                                                        
                                                      ";
                                                        }
                                                     }


                                                     $consultant_count[$consultant['full_name']]['new']=$new_n;
                                                     $consultant_count[$consultant['full_name']]['old']=$old_n;
                                         ?> 
                        </table>


        
                    
    
                  
                  <!-- /.col -->
                </div>
                <!-- /.row -->
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
            <?php } ?>
</div>
			
      
 </div> <!--row -->
			
<div class="row">


<div id="mypresentersTable" class="col-md-12">

<!-- /.info-box -->
<div class="card">
  <div  class="card-header">
    <h3 class="card-title"><i class="fas fa-user-tie text-info"></i>Patient Count per Consultant</h3>
    <div id="addbtn" class='eachrow' style=' float: right; '>

    <!-- <a  class='btn btn-success'  href='#admiting_modal' data-toggle='modal'  style='color: aliceblue; line-height: 2;padding: 0px 15px;'>Add New Patient</a> -->
      </td>

</div>
  </div>
  <!-- /.card-header -->
  <div class="card-body">
    <div class="row">

              <table class="col-md-12" style="text-align: center;" >
                <thead   style="text-align: center;font-weight: 700;">
                <tr>
                  <td class="col-md-6" >Consultant</td>
                  <td class="col-md-2">Old Pateints</td>
                  <td class="col-md-2">New Patients</td>
                  <td class="col-md-2">Total Patients</td>
                </tr>
                </thead>
                             <?php
// var_dump($consultant_count);
                                    foreach ($consultant_count as $key => $cc){
                                     $total=   $cc['old']+$cc['new'];
                                        echo"  
                                       
                                        <tr class='eachrow'>
                                        
                                          <td style='  padding: 0px 1%;' class='eachcol'  scope='row' >
                                            <p>Dr. ".$key."</p>
                                          </td>
                                          
                                          <td style='  padding: 0px 1%;' class='eachcol' >
                                          <p>".$cc['old']."</p>
                                          </td>

                                          <td style='  padding: 0px 1%;' class='eachcol'>
                                          <p>".$cc['new']."</p>
                                          </td>
                                    
                                          <td style='  padding: 0px 1%;' class='eachcol'  scope='row' >
                                          
                                          <p>".$total."</p>
                                          </td>
                                          </tr >
                                          ";
                                       
                                     
                                            }
                                         

                             ?> 
            </table>



        

      
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </div>
  <!-- /.card-body -->
</div>
<!-- /.card -->

</div>

</div> <!--row -->
 

</div><!--/. container-fluid -->

            

 </section>
    <!-- /.content -->
    

<!-- PAGE SCRIPTS -->

<!-- AdminLTE App -->
<script src="dist/js/adminlte.js"></script>

<!-- OPTIONAL SCRIPTS -->
<script src="dist/js/demo.js"></script>



<!-- Modal patient details-->

<div class="modal" id="details_modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
     
          <h4 class="modal-title">Patient Details</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
    <!-- data retrived from JS -->
      <div class="modal-body">
      <div id="pdetailsdiv"></div>
      </div>
     
    
  </div>
</div>
</div>    


<!-- Modal patient transfer_modal-->

<div class="modal" id="transfer_modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
     
          <h4 class="modal-title">Transfer Patient</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
    <!-- data retrived from JS -->
      <div class="modal-body">
      <div id="ptransferdiv"></div>
      </div>
     
    
  </div>
</div>
</div>    

<!-- Modal -->

<div class="modal" id="my_modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
     
          <h4 class="modal-title">Discharge Patient</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <p>Kindly Review admission and discharge details</p>

        <div id="pdischargediv"></div>
       
      </div>

      <div class="modal-footer">

      </div>

          </form>
    </div>
  </div>
</div>
           



  </div>
  <!-- /.content-wrapper -->


  <script type="text/javascript">
      $(document).ready(function() {
        $('.select2').select2({
      placeholder: 'Select',
      
    } );
        $('.ddxname').select2({
            placeholder: 'Select',
            minimumInputLength: 4,
            ajax: {
                url: 'fetchicd10.php',
                dataType: 'json',
                delay: 250,
                data: function (data) {
                    return {
                        searchTerm: data.term // search term
                    };
                },
                processResults: function (response) {
                    return {
                        results:response
                    };
                },
                cache: true
            }
        });
      });

    </script>
  <script>





$(document).ready(function($) {
  
  $('.txtdata').on('change', function(){
var parent = $(this).parent('.eachcol').parent('.eachrow');
    var id = $(parent).find('.id').find('input').val();
    var bed = $(parent).find('.bed').find('input').val();
    var mrn = $(parent).find('.mrn').find('input').val();
    var name = $(parent).find('.name').find('input').val();
    // var admfrom = $(parent).find('.admfrom').find('select').val();
    var admdate = $(parent).find('.admdate').find('input').val();
    var admissiondiagnosis = $(parent).find('.admissiondiagnosis').find('select').val();
         
    //  alert (scot);

    var attribChanged = $(this).attr('name');
    data = {id: id, bed: bed, mrn: mrn,name: name,  admdate: admdate
      // , admfrom: admfrom
      , admissiondiagnosis:admissiondiagnosis, attribChanged: attribChanged};
    $.post('dmc-patients-update.php', data, function(data){
      // $(parent).html(data);
     
    });
    $(this).parent('.eachcol').css("backgroundColor", "#90EE90");

  });
});
</script>

<script>

// document.getElementById('addpatient').onclick = function(){
//   var parent = $(this).parent('.eachrow');
//   var attribChanged = $(this).attr('name');
//   data = {attribChanged: attribChanged};
//   $.post('dmc-patients-add.php', data, function(data){
//   $(parent).html(data);
//   location.reload();
// });
// }



$("textarea").each(function(textarea) {
    $(this).height( $(this)[0].scrollHeight );
});


  </script>


<script>
$(function() {
  $('input[name="admdate"]').daterangepicker({
    singleDatePicker: true,
    timePicker: false,
    timePicker24Hour: false,
    autoUpdateInput: false,
    showDropdowns: true,
    minYear: 2010,
    maxYear: parseInt(moment().format('YYYY'),10),
    locale: {
            format: 'YYYY-MM-DD'
        }
  }, ).on("apply.daterangepicker", function (e, picker) {
        picker.element.val(picker.startDate.format(picker.locale.format));
        var parent = $(this).parent('.eachcol').parent('.eachrow');
    var id = $(parent).find('.id').find('input').val();
    var bed = $(parent).find('.bed').find('input').val();
    var mrn = $(parent).find('.mrn').find('input').val();
    var name = $(parent).find('.name').find('input').val();
    // var admfrom = $(parent).find('.admfrom').find('select').val();
    var admdate = $(parent).find('.admdate').find('input').val();
    var admissiondiagnosis = $(parent).find('.admissiondiagnosis').find('select').val();
   
 
    //  alert (scot);

    var attribChanged = $(this).attr('name');
    data = {id: id, bed: bed, mrn: mrn,name: name,  admdate: admdate,
      // admfrom: admfrom,
       admissiondiagnosis:admissiondiagnosis, attribChanged: attribChanged};
    $.post('dmc-patients-update.php', data, function(data){
      // $(parent).html(data);
     
    });
    $(this).parent('.eachcol').css("backgroundColor", "#90EE90");
    });
});


</script>

<script>




</script>
<?php

require 'footer.php';

?>

<script>




$('#my_modal').on('show.bs.modal', function(e) {
 
  var bookId = $(e.relatedTarget).data('book-id');
  

 data = {bookId: bookId};
 $.post('dmc-patients-discharge.php', data, function(data){
 $('#pdischargediv').html(data);
     
    });
  
    // $(e.currentTarget).find('input[name="patientId"]').val(bookId);
    // disdate=document.getElementById('disdate').value='';
    // document.getElementById('finaldiagnosis').value='';
    // document.getElementById('disstatus').value='';
    // document.getElementById('disto').value='';
    // document.getElementById('disdate').style.backgroundColor = "";
    // document.getElementById('finaldiagnosis').style.backgroundColor = "";
    // document.getElementById('disto').style.backgroundColor = "";
    // document.getElementById('disstatus').style.backgroundColor = "";
});

$('#details_modal').on('show.bs.modal', function(e) {
 
//  var bookId = $(e.relatedTarget).data('book-id');
 var bookId = $(e.relatedTarget).data('book-id');
//  $(e.currentTarget).find('input[name="patientId"]').val(bookId);


 data = {bookId: bookId};
 $.post('dmc-patients-details.php', data, function(data){
 $('#pdetailsdiv').html(data);
     
    });
});


 $('#transfer_modal').on('show.bs.modal', function(e) {
 
 //  var bookId = $(e.relatedTarget).data('book-id');
  var myBookId1 = $(e.relatedTarget).data('book-id');
 //  $(e.currentTarget).find('input[name="patientId"]').val(bookId);
//  $(".modal-body #bookId").val( myBookId );
 

  data = {myBookId1: myBookId1};
  $.post('dmc-patients-ptransferdiv.php', data, function(data){
  $('#ptransferdiv').html(data);

      
     });
 });


 
</script>

