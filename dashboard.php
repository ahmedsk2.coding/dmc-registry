<?php 
session_start();

require_once "authCookieSessionValidate.php";
date_default_timezone_set('Asia/Riyadh');
$today=date("Y-m-d");

if(!$isLoggedIn) {
    header("Location: ./");
}

?>

  <!-- Navbar -->
<?php
require 'sidebar.php';
require ('dbconnect.php');
?>
<script src="https://cdn.jsdelivr.net/npm/chart.js"></script>

<?php
   		
       $formationSQL = "SELECT * FROM picupatients WHERE DISDATE IS NULL";
       $result1 = $mysqli->query($formationSQL);
       $activepicupatints = $result1 -> fetch_all(MYSQLI_ASSOC);

       

       $formationSQL = "SELECT * FROM countries";
       $result1 = $mysqli->query($formationSQL);
       $countries = $result1 -> fetch_all(MYSQLI_ASSOC);
   
       $formationSQL = "SELECT * FROM members WHERE position = '3' AND active = 1";
       $result1 = $mysqli->query($formationSQL);
       $consultants = $result1 -> fetch_all(MYSQLI_ASSOC);

       $query = "select * from settings";
       $result1 = $mysqli->query($query);
       $settings = $result1 -> fetch_array(MYSQLI_ASSOC);

       $shortlos=$settings['short_los'];
       $longlos=$settings['long_los'];
       $max_hospitalist=$settings['max_hospitalist'];
     ?>
   
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
  
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">DMC Internal Medicine Dashboard</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="dashboard.php">Home</a></li>
              <li class="breadcrumb-item active">Dashboard</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
         <div class="row mb-2">
                 <div class="col-sm-6">

            <div style=" font-weight: bold;" id="container"  class="rs-select2 select--no-search"  >
                Hello <?php echo $user['member_name']; ?>
		</div><!-- /.col -->							
		</div>
		</div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">  
      

      <div class="row">
      <div id="mypresentersTable" class="col-md-12">

      <div class="card">
  <div  class="card-header">
    <h3 class="card-title"><i class="nav-icon fas fa-tachometer-alt text-info"></i> 30 Days Overview</h3><i style=" float: right; " download="download.png" id="btn-Preview-Image" type="button" value="Download" class="fas fa-download"></i>
  </div>
  <!-- /.card-header -->
  <div class="card-body">
  <div class="row">

<div id="testttt" class="col-sm-12"> 
<div  class="chart-container" style="position: relative; height:40vh">
<canvas style="background: white" id="mymonthlyChart"></canvas>
</div>

<script>
        $(document).ready(function() {


            var element = $("#mymonthlyChart"); // global variable
            var getCanvas; // global variable
            var newData;

            $("#btn-Preview-Image").on('click', function() {
                html2canvas(element, {
                    onrendered: function(canvas) {
                        getCanvas = canvas;
                        var imgageData = getCanvas.toDataURL("image/png");
                        var a = document.createElement("a");
                        a.href = imgageData; //Image Base64 Goes here
                        a.download = "30 Days Overview.png"; //File name Here
                        a.click(); //Downloaded file
                    }
                });
            });


        });
    </script>

<?php
$label=array();
$admissions=array();
$discharges=array();


$date=new DateTime();
$n=0;
while($n < 31){
  $date1 = $date->format('Y-m-d');

  $formationSQL = "SELECT * FROM picupatients WHERE ADMDATE = '".$date1."' AND (current_location != 'ICU' or current_location is null)";
  $result1 = $mysqli->query($formationSQL);
  $admittedpcount = mysqli_num_rows($result1);
  $formationSQL = "SELECT * FROM picupatients WHERE DISDATE = '".$date1."' AND (current_location != 'ICU' or current_location is null)";
  $result1 = $mysqli->query($formationSQL);
  $dischargedpcount = mysqli_num_rows($result1);
  
  array_push($label,$date1);
  array_push($admissions,$admittedpcount);
  array_push($discharges,$dischargedpcount);
$n++;
$date->modify("-1 day");
}
// var_dump($chart);
// var_dump($consultants);

$label=array_reverse($label);
$admissions=array_reverse($admissions);
$discharges=array_reverse($discharges);
?>
  <script>
  
  var label = <?php echo json_encode($label); ?>;
  var admissions = <?php echo json_encode($admissions); ?>;
  var discharges = <?php echo json_encode($discharges); ?>;
  // var consultant = [];
  // var admissions = [];

  // for(var i=0; i<jArray.length; i++){
  //   consultant.push("Dr. " + jArray[i].full_name);
  //   admissions.push(jArray[i].admittions);
  //   // alert(jArray[i].full_name);
  //   }
    // alert(JSON.stringify(label));
  const mlabels = label;

  const mdata = {
    labels: mlabels,
    datasets: [{
      label: 'Admissions',
      backgroundColor: 'rgb(41, 134, 204, 0.3)',
      borderColor: 'rgb(41, 134, 204, 0.6)',
      data: admissions,
      fill: true
    },
    {
      label: 'Discharges',
      backgroundColor: 'rgb(204, 41, 134, 0.3)',
      borderColor: 'rgb(204, 41, 134, 0.6)',
      data: discharges,
      fill: true
    }]
  };
  const colors = ['red', 'blue', 'yellow', 'green'];
  const weekend =[];
  const mconfig = {
    type: 'line',
    
    data: mdata,
    options: {
      maintainAspectRatio: false,
    plugins: {
      filler: {
        propagate: false,
      },
      title: {
        display: true,
        text: 'Daily Admission and Discharge'
      }
    },
    pointBackgroundColor: '#fff',
    radius: 8,
    interaction: {
      intersect: false,
    },
    scales: {
      x: {
        ticks: {
          callback: function(value, index, ticks) {
                    var day =new Date(this.getLabelForValue(value));
                    // alert (day.getDay());
                    if (day.getDay() == 6 || day.getDay() == 5){
                                  // alert("this one");
                                  weekend.push(value);
                                }

                    return this.getLabelForValue(value);

                    },
          color: (c) => {
            if (weekend.includes(c.index)){
            return colors[0]
            }
          }
        }
      },
        }
  },
  };
</script>
<script>
  const mymonthlyChart = new Chart(
    document.getElementById('mymonthlyChart'),
    mconfig
  );
 
</script>

</div><!-- end colomn  -->

</div><!-- end row -->
</div><!-- end colomn  -->
</div><!-- end row -->
  </div>
  <!-- /.card-body -->
</div>
<!-- /.card -->






      <div class="row">
      <div id="mypresentersTable" class="col-md-12">

      <div class="card">
  <div  class="card-header">
    <h3 class="card-title"><i class="nav-icon fas fa-tachometer-alt text-info"></i> Daily Overview</h3><i style=" float: right; " download="download.png" id="btn-Preview-Image1" type="button" value="Download" class="fas fa-download"></i>
  </div>
  <!-- /.card-header -->
  <div class="card-body">
  <div class="row" style="background: white" id="dailygraphs">

<div class="col-sm-3">
  <canvas id="myChartdoughnut"></canvas>
<?php




// count hospitalists patient
$under_hospitalist=0;

$formationSQL = "SELECT * FROM members WHERE specialty_id = '1'";
$result1 = $mysqli->query($formationSQL);
$hospitalist = $result1 -> fetch_all(MYSQLI_ASSOC);


foreach($hospitalist as $h){
  $formationSQL = "SELECT * FROM picupatients WHERE DISDATE IS NULL AND (longterm IS NULL OR longterm = '') AND consultant_id='".$h['member_id']."' ";
  $result1 = $mysqli->query($formationSQL);
  $hospitalist_ps = mysqli_num_rows($result1);

  $under_hospitalist=$under_hospitalist+$hospitalist_ps;
}


// count hospitalists patient
$under_subs=0;

$formationSQL = "SELECT * FROM members WHERE specialty_id != '1'";
$result1 = $mysqli->query($formationSQL);
$subconsultant = $result1 -> fetch_all(MYSQLI_ASSOC);


foreach($subconsultant as $s){
  $formationSQL = "SELECT * FROM picupatients WHERE DISDATE IS NULL AND (longterm IS NULL OR longterm = '') AND consultant_id='".$s['member_id']."' ";
  $result1 = $mysqli->query($formationSQL);
  $subs_ps = mysqli_num_rows($result1);

  $under_subs=$under_subs+$subs_ps;
}


// count long term
  $formationSQL = "SELECT * FROM picupatients WHERE DISDATE IS NULL AND longterm = 'longterm'";
  $result1 = $mysqli->query($formationSQL);
  $longterm = mysqli_num_rows($result1);
  

//count TB
$tuber_count=0;


  // TB list

  $formationSQL = "SELECT dx_id FROM tb_list";
  $result1 = $mysqli->query($formationSQL);
  $tuber_list1 = $result1 -> fetch_all(MYSQLI_ASSOC);
  $tuber_list=array();
  foreach($tuber_list1 as $tuber){
    $tuber_list[]=$tuber['dx_id'];
  }

$formationSQL = "SELECT * FROM picupatients WHERE DISDATE IS NULL";
$result1 = $mysqli->query($formationSQL);
$activepicupatints = $result1 -> fetch_all(MYSQLI_ASSOC);

foreach($activepicupatints as $s){
$decodedadmissiondx=json_decode($s['admissiondiagnosis']);
if ($decodedadmissiondx){
  $is_tb_patient=array_intersect($decodedadmissiondx,$tuber_list);
}
if ( $is_tb_patient){
  $tuber_count++;
}
}

$under_subs=$under_subs-$tuber_count;
/// all counts are
$all_counts=[$under_hospitalist,$under_subs,$longterm,$tuber_count];
$label1=["Hospitalist","Sub Speciality","Long Term","TB"];
$totalps=$under_hospitalist+$under_subs+$longterm;
$title1 = "Current Active Patient Counts: " . $totalps . " In Total";
?>
  <script>
  
  
  var label1 = <?php echo json_encode($label1); ?>;
  var all_counts = <?php echo json_encode($all_counts); ?>;
  // var consultant = [];
  // var admissions = [];

  // for(var i=0; i<jArray.length; i++){
  //   consultant.push("Dr. " + jArray[i].full_name);
  //   admissions.push(jArray[i].admittions);
  //   // alert(jArray[i].full_name);
  //   }
    // alert(JSON.stringify(label));
  const labels1 = label1;

  const data1 = {
    labels: labels1,
    datasets: [{
      label: 'Current Active Patient Counts',
      backgroundColor:  [
      'rgb(255, 99, 132)',
      'rgb(54, 162, 235)',
      'rgb(255, 205, 86)',
      'rgb(36, 169, 115)'
    ],
      data: all_counts,
    }]
  };

  const config1 = {
    type: 'doughnut',
    data: data1,
    options: {
    responsive: true,
    plugins: {
      legend: {
        position: 'top',
      },
      title: {
        display: true,
        text: '<?php echo $title1; ?>',
      }
    }
  },
  };
</script>
<script>
  const myChartdoughnut = new Chart(
    document.getElementById('myChartdoughnut'),
    config1
  );
 
</script>

</div><!-- end colomn  -->

      <div class="col-sm-6">
      <div class="chart-container" style="position: relative; height:30vh">
  <canvas id="myChart"></canvas>
      </div>
<?php
$label=array();
$admissions=array();
$discharges=array();



foreach($consultants as $c){

  $formationSQL = "SELECT * FROM picupatients WHERE ADMDATE + INTERVAL 1 DAY >= '".$today."'  AND consultant_id='".$c['member_id']."' AND (current_location != 'ICU' or current_location is null)";
  $result1 = $mysqli->query($formationSQL);
  $admittedpcount = mysqli_num_rows($result1);
  $formationSQL = "SELECT * FROM picupatients WHERE DISDATE + INTERVAL 1 DAY >= '".$today."' AND consultant_id='".$c['member_id']."' AND (current_location != 'ICU' or current_location is null) ";
  $result1 = $mysqli->query($formationSQL);
  $dischargedpcount = mysqli_num_rows($result1);
  
  array_push($label,$c['full_name']);
  array_push($admissions,$admittedpcount);
  array_push($discharges,$dischargedpcount);

}
// var_dump($chart);
// var_dump($consultants);

?>
  <script>
  
  var label = <?php echo json_encode($label); ?>;
  var admissions = <?php echo json_encode($admissions); ?>;
  var discharges = <?php echo json_encode($discharges); ?>;
  // var consultant = [];
  // var admissions = [];

  // for(var i=0; i<jArray.length; i++){
  //   consultant.push("Dr. " + jArray[i].full_name);
  //   admissions.push(jArray[i].admittions);
  //   // alert(jArray[i].full_name);
  //   }
    // alert(JSON.stringify(label));
  const labels = label;

  const data = {
    labels: labels,
    datasets: [{
      label: 'Admissions',
      backgroundColor: 'rgb(41, 134, 204)',
      borderColor: 'rgb(41, 134, 204)',
      data: admissions,
    },
    {
      label: 'Discharges',
      backgroundColor: 'rgb(204, 41, 134)',
      borderColor: 'rgb(204, 41, 134)',
      data: discharges,
    }]
  };

  const config = {
    type: 'bar',
    data: data,
    options: {
      maintainAspectRatio: false,
    plugins: {
      title: {
        display: true,
        text: 'Admission / Discharge per Consultant since last day'
      },
    },
    responsive: true,
    scales: {
      x: {
        stacked: true,
      },
      y: {
        stacked: true
      }
    }
  }
  };
</script>
<script>
  const myChart = new Chart(
    document.getElementById('myChart'),
    config
  );
 
</script>

</div><!-- end colomn  -->



<div class="col-sm-3">
  <canvas id="myChartdoughnut1"></canvas>
<?php


// consultation count


$formationSQL = "SELECT * FROM consultations WHERE signoff_date + INTERVAL 1 DAY >= '".$today."'";
$result1 = $mysqli->query($formationSQL);
$signedoff = mysqli_num_rows($result1);

$formationSQL = "SELECT * FROM consultations WHERE signoff_date IS NULL";
$result1 = $mysqli->query($formationSQL);
$active = mysqli_num_rows($result1);

/// all counts are
$all_counts2=[$signedoff,$active];
$label2=["Signed Off","Active Consultations"];
?>
  <script>
  
  
  var label2 = <?php echo json_encode($label2); ?>;
  var all_count2 = <?php echo json_encode($all_counts2); ?>;
  // var consultant = [];
  // var admissions = [];

  // for(var i=0; i<jArray.length; i++){
  //   consultant.push("Dr. " + jArray[i].full_name);
  //   admissions.push(jArray[i].admittions);
  //   // alert(jArray[i].full_name);
  //   }
    // alert(JSON.stringify(label));
  const labels2 = label2;

  const data2 = {
    labels: labels2,
    datasets: [{
      label: 'Current Active Consultations Counts',
      backgroundColor:  [
      'rgb(255, 99, 132)',
      'rgb(54, 162, 235)',
      'rgb(255, 205, 86)',
      'rgb(36, 169, 115)'
    ],
      data: all_count2,
    }]
  };

  const config2 = {
    type: 'doughnut',
    data: data2,
    options: {
    responsive: true,
    plugins: {
      legend: {
        position: 'top',
      },
      title: {
        display: true,
        text: 'Consultations since last day'
      }
    }
  },
  };
</script>
<script>
  const myChartdoughnut1 = new Chart(
    document.getElementById('myChartdoughnut1'),
    config2
  );
 
</script>

</div><!-- end colomn  -->
</div><!-- end row -->


<script>
        $(document).ready(function() {


            var element = $("#dailygraphs"); // global variable
            var getCanvas; // global variable
            var newData;

            $("#btn-Preview-Image1").on('click', function() {
                html2canvas(element, {
                    onrendered: function(canvas) {
                        getCanvas = canvas;
                        var imgageData = getCanvas.toDataURL("image/png");
                        var a = document.createElement("a");
                        a.href = imgageData; //Image Base64 Goes here
                        a.download = "Daily Overview.png"; //File name Here
                        a.click(); //Downloaded file
                    }
                });
            });


        });
    </script>


</div><!-- end colomn  -->
</div><!-- end row -->
  </div>
  <!-- /.card-body -->
</div>
<!-- /.card -->

 
<div class="row">
      <div id="mypresentersTable" class="col-md-12">

      <div class="card">
  <div  class="card-header">
    <h3 class="card-title"><i class="nav-icon fas fa-tachometer-alt text-info"></i> <?php echo date("Y"); ?> Overview</h3>
  </div>
  <!-- /.card-header -->
  <div class="card-body">
  <div class="row">

  <div class="col-sm-6" style="height: fit-content;">
<?php

$formationSQL = "SELECT admissiondiagnosis FROM picupatients WHERE WEEK(ADMDATE) = '".date("W")."'";
  $result1 = $mysqli->query($formationSQL);
  $ddx = $result1 -> fetch_all(MYSQLI_ASSOC);

$dx_count=array();
// var_dump($ddx);
foreach ($ddx as $dx){
$dx_list=json_decode($dx['admissiondiagnosis']);
// var_dump($dx_list);
foreach ($dx_list as $dx){
        if(isset($dx_count[$dx])){
            $dx_count[$dx]++;
        }else{
            $dx_count[$dx]=1;
        }
    }
}

$dx_name_count=array();
foreach ($dx_count as $key => $value){
    $formationSQL = "SELECT * FROM icd10 WHERE id='".$key."'";
    $result1 = $mysqli->query($formationSQL);
    $dxname= $result1 -> fetch_array(MYSQLI_ASSOC);

    $dx_name_count[$dxname['name']]=$value;
}
arsort($dx_name_count);
// var_dump($dx_count);
// var_dump($dx_name_count);
?>

<table class="table">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">This Week Top 5 Dignosis</th>
      <th scope="col">Volume</th>
    </tr>
  </thead>
  <tbody>
      <?php
      $n=0;
      foreach ($dx_name_count as $key => $value){
          $n++;
          if ($n == 6){
              break;
          }
          echo"
    <tr>
      <th scope='row'>".$n."</th>
      <td>".$key."</td>
      <td>".$value."</td>
    </tr>";
}

    ?>
  </tbody>
</table>

</div><!-- end colomn  -->

<div class="col-sm-6" style="height: fit-content;">
<?php
/// LOS
$formationSQL = "SELECT ADMDATE, DISDATE FROM picupatients WHERE DISDATE IS NOT NULL AND MONTH(DISDATE) = '".date("m")."'  AND YEAR(DISDATE) = '".date("Y")."' AND (current_location != 'ICU' or current_location is null)";
  $result1 = $mysqli->query($formationSQL);
  $dates = $result1 -> fetch_all(MYSQLI_ASSOC);

$los=array();

foreach ($dates as $date){
    $timeDiff = abs(strtotime($date['ADMDATE']) - strtotime($date['DISDATE']));

    array_push($los,$timeDiff/86400);

}
// var_dump($los);
// $a = array_filter($los);
if(count($los)) {
    $average = array_sum($los)/count($los);
}else{
  $average=0;
}


///// Total Admission
$formationSQL = "SELECT ADMDATE FROM picupatients WHERE YEAR(ADMDATE) = '".date("Y")."' AND (current_location != 'ICU' or current_location is null)";
  $result1 = $mysqli->query($formationSQL);
  $admissioncount = mysqli_num_rows($result1);
  ///// Total discharges
$formationSQL = "SELECT DISDATE FROM picupatients WHERE YEAR(DISDATE) = '".date("Y")."' AND (current_location != 'ICU' or current_location is null)";
$result1 = $mysqli->query($formationSQL);
$dischargedcount = mysqli_num_rows($result1);
  ///// Total Consultations
  $formationSQL = "SELECT consultation_date FROM consultations WHERE YEAR(consultation_date) = '".date("Y")."'";
  $result1 = $mysqli->query($formationSQL);
  $totalconsults = mysqli_num_rows($result1);
  ///// Total Sign Offs
  $formationSQL = "SELECT signoff_date FROM consultations WHERE YEAR(signoff_date) = '".date("Y")."'";
  $result1 = $mysqli->query($formationSQL);
  $totalsignoffs = mysqli_num_rows($result1);

  
?>
     <?php 
      $patient_count=0;

                   $consultant_count=array();
                   $active_ICU_count=0;
                   $all_activepatients=0;
                   
              foreach ($consultants as $consultant){
        
             $tuber=0;
                $old_n=0;
                $new_n=0;
                $icu_n=0;
                $ward_n=0;
                $is_tb_patient1=array();
                $activepatients=0;
                // 


                foreach($activepicupatints as $s){
                 
                            

                  if($s['consultant_id'] == $consultant['member_id'] ){

                                      $decodedadmissiondx=json_decode($s['admissiondiagnosis']);
                      if ($decodedadmissiondx){
                      $is_tb_patient1=array_intersect($decodedadmissiondx,$tuber_list);
                    }
                    if (!empty($is_tb_patient1)){
                       $tuber++;
                    }     

                    $patient_count++;
                    if(is_null($s['newassign'])){
                      $old_n++;

                    } elseif ($s['newassign']=='1'){
                      $new_n++;
                    }

                    if($s['current_location'] == 'ICU'){
                      $icu_n++;
                      $active_ICU_count++;
                    } else{
                      $ward_n++;
                    }

                    if($s['current_location'] == 'ICU' OR $s['med_DISDATE'] OR $s['longterm'] OR  $is_tb_patient1){
                      
                    }else{
                      $activepatients++;
                      $all_activepatients++;
                    }
                  }
                }
                
                $consultant_count[$consultant['member_id']]['new']=$new_n;
                                                     $consultant_count[$consultant['member_id']]['old']=$old_n;
                                                     $consultant_count[$consultant['member_id']]['icu']=$icu_n;
                                                     $consultant_count[$consultant['member_id']]['ward']=$ward_n;
                                                     $consultant_count[$consultant['member_id']]['tb1']=$tuber;
                                                     $consultant_count[$consultant['member_id']]['activepatients']=$activepatients;
                                                     $consultant_count[$consultant['member_id']]['active']=$consultant['active'];
                                                     $consultant_count[$consultant['member_id']]['specialty_id']=$consultant['specialty_id'];
                                                     $consultant_count[$consultant['member_id']]['on_service']=$consultant['on_service'];
              }
              ?>

<div class="table-responsive text-nowrap">
<table class="table">
  <thead>
    <tr>
      <th scope="col" colspan="2">Average Length of Stay for <?php echo date("M"); ?></th>
      <th scope="col" colspan="2"><?php echo number_format((float)$average, 2, '.', ''); ?> Days</th>
    </tr>
    <tr>
      <th scope="col">Total Admissions for <?php echo date("Y"); ?></th>
      <th scope="col"><?php echo $admissioncount; ?> Patients</th>
      <th scope="col">Total Discharges</th>
      <th scope="col"><?php echo $dischargedcount; ?> Patients</th>
    </tr>
    <tr>
      <th scope="col">Total Consultations for <?php echo date("Y"); ?></th>
      <th scope="col"><?php echo $totalconsults; ?> Patients</th>
      <th scope="col">Total Sign Offs</th>
      <th scope="col"><?php echo $totalsignoffs; ?> Patients</th>
    </tr>
    <tr>
      <th scope="col">Currently in ICU</th>
      <th scope="col"><?php echo $active_ICU_count; ?> Patients</th>
      <th scope="col"></th>
      <th scope="col"></th>
    </tr>
    <tr>
      <th scope="col" colspan="2">Hospitalist capacity utilization for today</th>
      <?PHP
      $hospitalist_n=0;
       foreach ($consultant_count as $key => $cc){
        if ($cc['on_service'] =='1' && $cc['specialty_id'] =='1' && $cc['active'] =='1'){
          $hospitalist_n++;
        }
      }
      $capacity = $hospitalist_n * $max_hospitalist;
      
      ?>
      <th scope="col" colspan="2"><?php echo number_format((($all_activepatients/$capacity)*100), 2, '.', ''); ?> %</th>
    </tr>

    
  </thead>
  <tbody>
      <?php
//       $n=0;
//       foreach ($dx_name_count as $key => $value){
//           $n++;
//           echo"
//     <tr>
//       <th scope='row'>".$n."</th>
//       <td>".$key."</td>
//     </tr>";
// }
    ?>
  </tbody>
</table>
</div>
</div><!-- end colomn  -->


</div><!-- end row -->
</div><!-- end colomn  -->
</div><!-- end row -->
  </div>
  <!-- /.card-body -->
</div>




 
		
    <div class="row">


<div id="mypresentersTable" class="col-md-12">

<!-- /.info-box -->
<div class="card">
  <div  class="card-header">
    <h3 class="card-title"><i class="fas fa-user-tie text-info"></i> Patient Count per Consultant</h3>
    <div id="addbtn" class='eachrow' style=' float: right; '>

    <h3 class="card-title"><i class="fas fa-user-tie text-info"></i> Total Count: <?php echo $patient_count; ?></h3>
      
</div>
  </div>
  <!-- /.card-header -->
  <div class="card-body">
    <div class="row table-responsive">

    <table class="col-md-12" style="text-align: center;" >
                <thead   style="text-align: center;font-weight: 700;">
                <tr>
                  <td class="col-md-4" >Consultant</td>
                  <td class="col-md-1">Old Pateints</td>
                  <td class="col-md-1">New Patients</td>
                  <td class="col-md-6" colspan="4">All Patients</td>
                </tr>
                <tr>
                  <td class="col-md-4" ></td>
                  <td class="col-md-1"></td>
                  <td class="col-md-1"></td>
                  <td class="col-md-2">Active Patients</td>
                  <td class="col-md-2">Total Ward Patients</td>
                  <td class="col-md-1">ICU Patients</td>
                  <td class="col-md-1">TB Patients</td>

                </tr>
                </thead>
                             <?php
// var_dump($consultant_count);
// on service hospitalist consultant
                                    foreach ($consultant_count as $key => $cc){
                                      if ($cc['on_service'] =='1' && $cc['specialty_id'] =='1' && $cc['active'] =='1'){
                                    
                                     $total=   $cc['ward'];
                                        echo"  
                                       
                                        <tr class='eachrow'>
                                        <td>
                                        ";

                                        $formationSQL = "SELECT * FROM members WHERE member_id = '".$key."'";
                                        $result1 = $mysqli->query($formationSQL);
                                        $consultant_data = $result1 -> fetch_array(MYSQLI_ASSOC);
                                        // var_dump($consultant_count);

                                          echo"
                                          <p>Dr. ".$consultant_data['full_name']."</p>
                                        </td>";
                                        

                                       
                                        echo"
                                          
                                          <td style='  padding: 0px 1%;' class='eachcol' >
                                          <p>".$cc['old']."</p>
                                          </td>

                                          <td style='  padding: 0px 1%;' class='eachcol'>
                                          <p>".$cc['new']."</p>
                                          </td>
                                          <td style='  padding: 0px 1%;' class='eachcol'  scope='row' >
                                          
                                          <p>".$cc['activepatients']."</p>
                                          </td>
                                          <td style='  padding: 0px 1%;' class='eachcol'  scope='row' >
                                          
                                          <p>".$total."</p>
                                          </td>
                                          <td style='  padding: 0px 1%;' class='eachcol'  scope='row' >
                                          
                                          <p>".$cc['icu']."</p>
                                          </td>
                                           <td style='  padding: 0px 1%;' class='eachcol'  scope='row' >
                                          
                                          <p>".$cc['tb1']."</p>
                                          </td>
                                          </tr >
                                          ";
                                       
                                      }
                                      
                                            }

                                            // on service subs consultant
                                    foreach ($consultant_count as $key => $cc){
                                      if ($cc['on_service'] =='1' && $cc['specialty_id'] !=='1' && $cc['active'] =='1'){
                                    
                                     $total=   $cc['ward'];
                                        echo"  
                                       
                                        <tr class='eachrow'>
                                        <td>
                                        ";

                                        $formationSQL = "SELECT * FROM members WHERE member_id = '".$key."'";
                                        $result1 = $mysqli->query($formationSQL);
                                        $consultant_data = $result1 -> fetch_array(MYSQLI_ASSOC);
                                        // var_dump($consultant_count);

                                          echo"
                                          <p>Dr. ".$consultant_data['full_name']."</p>
                                        </td>";
                                        

                                       
                                        echo"
                                          
                                          <td style='  padding: 0px 1%;' class='eachcol' >
                                          <p>".$cc['old']."</p>
                                          </td>

                                          <td style='  padding: 0px 1%;' class='eachcol'>
                                          <p>".$cc['new']."</p>
                                          </td>

                                          <td style='  padding: 0px 1%;' class='eachcol'  scope='row' >
                                          
                                          <p>".$cc['activepatients']."</p>
                                          </td>
                                    
                                          <td style='  padding: 0px 1%;' class='eachcol'  scope='row' >
                                          
                                          <p>".$total."</p>
                                          </td>
                                          <td style='  padding: 0px 1%;' class='eachcol'  scope='row' >
                                          
                                          <p>".$cc['icu']."</p>
                                          </td>
                                        <td style='  padding: 0px 1%;' class='eachcol'  scope='row' >
                                          
                                          <p>".$cc['tb1']."</p>
                                          </td>
                                          </tr >
                                          ";
                                       
                                      }
                                    }
                                         // NOT on service consultant
                                         echo"  
                                         <tr class='eachrow'>
                                         <td colspan='7'>
                                        <p style='color: red;font-weight: bold;'> OFF Service: </p>
                                         </td>
                                         </tr>";
                                    foreach ($consultant_count as $key => $cc){
                                      if ($cc['on_service'] =='0' && $cc['active'] =='1'){
                                    
                                     $total=   $cc['ward'];
                                        echo"  
                                       
                                        
                                        <tr class='eachrow'>
                                        <td>
                                        ";

                                        $formationSQL = "SELECT * FROM members WHERE member_id = '".$key."'";
                                        $result1 = $mysqli->query($formationSQL);
                                        $consultant_data = $result1 -> fetch_array(MYSQLI_ASSOC);
                                        // var_dump($consultant_count);

                                          echo"
                                          <p>Dr. ".$consultant_data['full_name']."</p>
                                        </td>";
                                        

                                       
                                        echo"
                                          
                                          <td style='  padding: 0px 1%;' class='eachcol' >
                                          <p>".$cc['old']."</p>
                                          </td>

                                          <td style='  padding: 0px 1%;' class='eachcol'>
                                          <p>".$cc['new']."</p>
                                          </td>
                                          <td style='  padding: 0px 1%;' class='eachcol'  scope='row' >
                                          
                                          <p>".$cc['activepatients']."</p>
                                          </td>
                                          <td style='  padding: 0px 1%;' class='eachcol'  scope='row' >
                                          
                                          <p>".$total."</p>
                                          </td>
                                          <td style='  padding: 0px 1%;' class='eachcol'  scope='row' >
                                          
                                          <p>".$cc['icu']."</p>
                                          </td>
                                           <td style='  padding: 0px 1%;' class='eachcol'  scope='row' >
                                          
                                          <p>".$cc['tb1']."</p>
                                          </td>
                                          </tr >
                                          ";
                                       
                                      }
                                            }

                             ?> 
            </table>


        

      
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </div>
  <!-- /.card-body -->
</div>
<!-- /.card -->

</div>

</div> <!--row -->


 

</div><!--/. container-fluid -->

            

 </section>
    <!-- /.content -->
    </div>  

<!-- PAGE SCRIPTS -->

<!-- AdminLTE App -->
<script src="dist/js/adminlte.js"></script>

<!-- OPTIONAL SCRIPTS -->
<script src="dist/js/demo.js"></script>


 
  <!-- /.content-wrapper -->
<?php
require 'footer.php';
?>

