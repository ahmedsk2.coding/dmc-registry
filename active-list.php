<?php 
session_start();

require_once "authCookieSessionValidate.php";

if(!$isLoggedIn) {
    header("Location: ./");
}

?>

  <!-- Navbar -->
<?php
require ('dbconnect.php');
?>


<?php
   		
       $formationSQL = "SELECT * FROM picupatients WHERE DISDATE IS NULL";
       $result1 = $mysqli->query($formationSQL);
       $activepicupatints = $result1 -> fetch_all(MYSQLI_ASSOC);
   
       $formationSQL = "SELECT * FROM countries";
       $result1 = $mysqli->query($formationSQL);
       $countries = $result1 -> fetch_all(MYSQLI_ASSOC);
   
       $formationSQL = "SELECT * FROM members WHERE position = '3'";
       $result1 = $mysqli->query($formationSQL);
       $consultants = $result1 -> fetch_all(MYSQLI_ASSOC);

  // TB list

  $formationSQL = "SELECT dx_id FROM tb_list";
  $result1 = $mysqli->query($formationSQL);
  $tb_list1 = $result1 -> fetch_all(MYSQLI_ASSOC);
  $tb_list=array();
  foreach($tb_list1 as $tb){
    $tb_list[]=$tb['dx_id'];
  }
  //settings
       $query = "select * from settings";
       $result1 = $mysqli->query($query);
       $settings = $result1 -> fetch_array(MYSQLI_ASSOC);

       $shortlos=$settings['short_los'];
       $longlos=$settings['long_los'];
     ?>
   
  <!-- Content Wrapper. Contains page content -->

  <?php 
require ('dbconnect.php');
$user_id = $_SESSION["member_id"];
$access_PICU_patients=[0,2,3];
$access_PICU_endorsement=[0,2,4];
$access_PICU_control=[0];
$access_PICU_registrar=[0,2];
$access_PICU_consultant=[0,2,3];

$formationSQL = "SELECT * FROM members WHERE member_id = '".$user_id."'";
$result1 = $mysqli->query($formationSQL);
$user = $result1 -> fetch_array(MYSQLI_ASSOC);

$username1=$user['member_name'];

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <!-- <meta name="viewport" content="width=1024"> -->
 <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">

  <title>DMC | Dashboard</title>

 
   <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
      <!-- Icons font CSS-->
    <link href="vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">
    <link href="vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
    
  <!-- overlayScrollbars -->
  <!-- <link rel="stylesheet" href="plugins/overlayScrollbars/css/OverlayScrollbars.min.css"> -->
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
	 
    <!-- Vendor CSS-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
    
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>


    <link href="vendor/datepicker/daterangepicker.css" rel="stylesheet" media="all">
	 <!-- Main CSS-->
    <link href="css/main.css" rel="stylesheet" media="all">

<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.js"> </script>

<!-- <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script> -->
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

<script src="http://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.2/modernizr.js"></script>
<script>
	//paste this code under head tag or in a seperate js file.
	// Wait for window load
	$(window).load(function() {
		// Animate loader off screen
		$(".se-pre-con").fadeOut("slow");;
	});
</script>
<style>
.select2-container--default .select2-selection--multiple .select2-selection__rendered li {
    list-style: none;
    color: black;
    
}
.select2-container--default .select2-selection--single .select2-selection__rendered {

    line-height: 15px;
}
.select2-container--default .select2-selection--multiple .select2-selection__choice__remove {
    cursor: pointer;
    display: contents;
    font-weight: bold;
    margin-right: 2px;
}
.cancelBtn{
color: black;
}
input{
  text-align: center;
    background-color: white;
    border: 1px solid #aaa;
    border-radius: 4px;
    cursor: text;
}
textarea{
  width:100%;
  padding-left: 2%;
    padding-right: 2%;
    background-color: white;
    border: 1px solid #aaa;
    border-radius: 4px;
    cursor: text;
}
label{
  width: 100%;
}

.no-js #loader { display: none;  }
.js #loader { display: block; position: absolute; left: 100px; top: 0; }
.se-pre-con {
	position: fixed;
	left: 0%;
	top: 0px;
	width: 100%;
	height: 100%;
	z-index: 100;
	background: url(dist/img/Preloader_3.gif) center no-repeat #fff;
}

</style>	
</head>
<body class="hold-transition sidebar-mini layout-fixed layout-navbar-fixed layout-footer-fixed">


  <div class="se-pre-con"></div>                                               

   
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
        <div class="col-sm-3">
<img  style="max-width: 312px;max-height: 67px;" src="dist/img/logo.png">
          </div><!-- /.col -->
          <div class="col-sm-6" style="text-align: center;display: inline-grid;">
            <h1 class="m-0 text-dark">Active Patients</h1>
            <a href="dashboard.php">Dashboard</a>
          </div><!-- /.col -->
          <div class="col-sm-3">
          <img src="dist/img/ehc.png">
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">  
      
      <?php 
        // TB list

  $formationSQL = "SELECT dx_id FROM tb_list";
  $result1 = $mysqli->query($formationSQL);
  $tuber_list1 = $result1 -> fetch_all(MYSQLI_ASSOC);
  $tuber_list=array();
  foreach($tuber_list1 as $tuber){
    $tuber_list[]=$tuber['dx_id'];
  }
                   $consultant_count=array();
                 
                   $patient_count=0;

                  
                   $active_ICU_count=0;
            
           foreach ($consultants as $consultant){
        
             $tuber=0;
                $old_n=0;
                $new_n=0;
                $icu_n=0;
                $ward_n=0;
                $is_tb_patient1=array();
                $activepatients=0;
                // 


                foreach($activepicupatints as $s){
                 
                            

                  if($s['consultant_id'] == $consultant['member_id'] ){

                                      $decodedadmissiondx=json_decode($s['admissiondiagnosis']);
                      if ($decodedadmissiondx){
                      $is_tb_patient1=array_intersect($decodedadmissiondx,$tuber_list);
                    }
                    if (!empty($is_tb_patient1)){
                       $tuber++;
                    }     

                    $patient_count++;
                    if(is_null($s['newassign'])){
                      $old_n++;

                    } elseif ($s['newassign']=='1'){
                      $new_n++;
                    }

                    if($s['current_location'] == 'ICU'){
                      $icu_n++;
                      $active_ICU_count++;
                    } else{
                      $ward_n++;
                    }

                    if($s['current_location'] == 'ICU' OR $s['med_DISDATE'] OR $s['longterm'] OR  $is_tb_patient1){
                      
                    }else{
                      $activepatients++;
                      $all_activepatients++;
                    }
                  }
                }
                
                $consultant_count[$consultant['member_id']]['new']=$new_n;
                                                     $consultant_count[$consultant['member_id']]['old']=$old_n;
                                                     $consultant_count[$consultant['member_id']]['icu']=$icu_n;
                                                     $consultant_count[$consultant['member_id']]['ward']=$ward_n;
                                                     $consultant_count[$consultant['member_id']]['tb1']=$tuber;
                                                     $consultant_count[$consultant['member_id']]['activepatients']=$activepatients;
                                                     $consultant_count[$consultant['member_id']]['active']=$consultant['active'];
                                                     $consultant_count[$consultant['member_id']]['specialty_id']=$consultant['specialty_id'];
                                                     $consultant_count[$consultant['member_id']]['on_service']=$consultant['on_service'];
              }
              ?>
		
    <div class="row">


    <div id="mypresentersTable" class="col-md-12">

<!-- /.info-box -->
<div class="card">
  <div  class="card-header">
    <h3 class="card-title"><i class="fas fa-user-tie text-info"></i> Patient Count per Consultant</h3>
    <div id="addbtn" class='eachrow' style=' float: right; '>

    <h3 class="card-title"><i class="fas fa-user-tie text-info"></i> Total Count: <?php echo $patient_count; ?></h3>
      
</div>
  </div>
  <!-- /.card-header -->
  <div class="card-body">
    <div class="row table-responsive">

    <table class="col-md-12" style="text-align: center;" >
                <thead   style="text-align: center;font-weight: 700;">
                <tr>
                  <td class="col-md-4" >Consultant</td>
                  <td class="col-md-1">Old Pateints</td>
                  <td class="col-md-1">New Patients</td>
                  <td class="col-md-6" colspan="4">All Patients</td>
                </tr>
                <tr>
                  <td class="col-md-4" ></td>
                  <td class="col-md-1"></td>
                  <td class="col-md-1"></td>
                  <td class="col-md-2">Active Patients</td>
                  <td class="col-md-2">Total Ward Patients</td>
                  <td class="col-md-1">ICU Patients</td>
                  <td class="col-md-1">TB Patients</td>

                </tr>
                </thead>
                             <?php
// var_dump($consultant_count);
// on service hospitalist consultant
                                    foreach ($consultant_count as $key => $cc){
                                      if ($cc['on_service'] =='1' && $cc['specialty_id'] =='1' && $cc['active'] =='1'){
                                    
                                     $total=   $cc['ward'];
                                        echo"  
                                       
                                        <tr class='eachrow'>
                                        <td>
                                        ";

                                        $formationSQL = "SELECT * FROM members WHERE member_id = '".$key."'";
                                        $result1 = $mysqli->query($formationSQL);
                                        $consultant_data = $result1 -> fetch_array(MYSQLI_ASSOC);
                                        // var_dump($consultant_count);

                                          echo"
                                          <p>Dr. ".$consultant_data['full_name']."</p>
                                        </td>";
                                        

                                       
                                        echo"
                                          
                                          <td style='  padding: 0px 1%;' class='eachcol' >
                                          <p>".$cc['old']."</p>
                                          </td>

                                          <td style='  padding: 0px 1%;' class='eachcol'>
                                          <p>".$cc['new']."</p>
                                          </td>
                                          <td style='  padding: 0px 1%;' class='eachcol'  scope='row' >
                                          
                                          <p>".$cc['activepatients']."</p>
                                          </td>
                                          <td style='  padding: 0px 1%;' class='eachcol'  scope='row' >
                                          
                                          <p>".$total."</p>
                                          </td>
                                          <td style='  padding: 0px 1%;' class='eachcol'  scope='row' >
                                          
                                          <p>".$cc['icu']."</p>
                                          </td>
                                           <td style='  padding: 0px 1%;' class='eachcol'  scope='row' >
                                          
                                          <p>".$cc['tb1']."</p>
                                          </td>
                                          </tr >
                                          ";
                                       
                                      }
                                      
                                            }

                                            // on service subs consultant
                                    foreach ($consultant_count as $key => $cc){
                                      if ($cc['on_service'] =='1' && $cc['specialty_id'] !=='1' && $cc['active'] =='1'){
                                    
                                     $total=   $cc['ward'];
                                        echo"  
                                       
                                        <tr class='eachrow'>
                                        <td>
                                        ";

                                        $formationSQL = "SELECT * FROM members WHERE member_id = '".$key."'";
                                        $result1 = $mysqli->query($formationSQL);
                                        $consultant_data = $result1 -> fetch_array(MYSQLI_ASSOC);
                                        // var_dump($consultant_count);

                                          echo"
                                          <p>Dr. ".$consultant_data['full_name']."</p>
                                        </td>";
                                        

                                       
                                        echo"
                                          
                                          <td style='  padding: 0px 1%;' class='eachcol' >
                                          <p>".$cc['old']."</p>
                                          </td>

                                          <td style='  padding: 0px 1%;' class='eachcol'>
                                          <p>".$cc['new']."</p>
                                          </td>

                                          <td style='  padding: 0px 1%;' class='eachcol'  scope='row' >
                                          
                                          <p>".$cc['activepatients']."</p>
                                          </td>
                                    
                                          <td style='  padding: 0px 1%;' class='eachcol'  scope='row' >
                                          
                                          <p>".$total."</p>
                                          </td>
                                          <td style='  padding: 0px 1%;' class='eachcol'  scope='row' >
                                          
                                          <p>".$cc['icu']."</p>
                                          </td>
                                        <td style='  padding: 0px 1%;' class='eachcol'  scope='row' >
                                          
                                          <p>".$cc['tb1']."</p>
                                          </td>
                                          </tr >
                                          ";
                                       
                                      }
                                    }
                                         // NOT on service consultant
                                         echo"  
                                         <tr class='eachrow'>
                                         <td colspan='7'>
                                        <p style='color: red;font-weight: bold;'> OFF Service: </p>
                                         </td>
                                         </tr>";
                                    foreach ($consultant_count as $key => $cc){
                                      if ($cc['on_service'] =='0' && $cc['active'] =='1'){
                                    
                                     $total=   $cc['ward'];
                                        echo"  
                                       
                                        
                                        <tr class='eachrow'>
                                        <td>
                                        ";

                                        $formationSQL = "SELECT * FROM members WHERE member_id = '".$key."'";
                                        $result1 = $mysqli->query($formationSQL);
                                        $consultant_data = $result1 -> fetch_array(MYSQLI_ASSOC);
                                        // var_dump($consultant_count);

                                          echo"
                                          <p>Dr. ".$consultant_data['full_name']."</p>
                                        </td>";
                                        

                                       
                                        echo"
                                          
                                          <td style='  padding: 0px 1%;' class='eachcol' >
                                          <p>".$cc['old']."</p>
                                          </td>

                                          <td style='  padding: 0px 1%;' class='eachcol'>
                                          <p>".$cc['new']."</p>
                                          </td>
                                          <td style='  padding: 0px 1%;' class='eachcol'  scope='row' >
                                          
                                          <p>".$cc['activepatients']."</p>
                                          </td>
                                          <td style='  padding: 0px 1%;' class='eachcol'  scope='row' >
                                          
                                          <p>".$total."</p>
                                          </td>
                                          <td style='  padding: 0px 1%;' class='eachcol'  scope='row' >
                                          
                                          <p>".$cc['icu']."</p>
                                          </td>
                                           <td style='  padding: 0px 1%;' class='eachcol'  scope='row' >
                                          
                                          <p>".$cc['tb1']."</p>
                                          </td>
                                          </tr >
                                          ";
                                       
                                      }
                                            }

                             ?> 
            </table>


        

      
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </div>
  <!-- /.card-body -->
</div>
<!-- /.card -->

</div>

</div> <!--row -->


<div class="row">

 <div id="accordion" class="col-md-12">

            <!-- /.info-box -->

            <?php

$admdate = array_column($activepicupatints, 'ADMDATE');

array_multisort($admdate, SORT_ASC , $activepicupatints);

foreach ($consultants as $consultant){
  
  /// counts and skip
  $any=0;

  foreach($activepicupatints as $s){
    if($s['consultant_id'] == $consultant['member_id'] ){
      $any++;
        }
    }

    if ($any == 0){
      continue;
    }
  /// counts and skip end

              ?>
            <div class="card">
              <div  class="card-header">
             
  
                <h3 class="card-title" onclick="showfunction('<?php echo $consultant['member_id']; ?>-1')" data-toggle="collapse" href="#<?php echo $consultant['member_id'];  ?>" role="button" aria-expanded="false" aria-controls="<?php echo $consultant['member_id'];  ?>" > <a  style="font-size: larger;font-weight: 700;" class="SeeMore2" id="<?php echo $consultant['member_id']; ?>-1" >
    +    </a><i class="fas fa-user-tie text-info"></i> Dr. <?php echo $consultant['full_name'];  ?> Patient List</h3>
          
                <div id="addbtn" class='eachrow' style=' float: right; '>
  
                <!-- <a  class='btn btn-success'  href='#admiting_modal' data-toggle='modal'  style='color: aliceblue; line-height: 2;padding: 0px 15px;'>Add New Patient</a> -->
                  </td>
        
          </div>
              </div>
              <!-- /.card-header -->


              <div class="card-body collapse" id="<?php echo $consultant['member_id'];  ?>">
                <div class="row">
         
          
                

                              <!-- <td class="col-md-1" >Bed #</td>
                              <td class="col-md-2">MRN</td>
                              <td class="col-md-2">Patient Name</td>
                              <td class="col-md-2">Admission Date</td>
                              <td class="col-md-1">LOS</td>
                              <td class="col-md-4">Diagnosis</td> -->
                                         <?php

                                        
                                                     foreach($activepicupatints as $s){
                                                      $formationSQL = "SELECT * FROM picupatients WHERE DISDATE + INTERVAL 3 DAY >='".$s['ADMDATE']."' AND ID <'".$s['ID']."' AND MRN='".$s['MRN']."' AND (trans_discharge = 'discharge from ICU' or trans_discharge='discharge from ward' or trans_discharge IS NULL) LIMIT 1";
                                                      $result1 = $mysqli->query($formationSQL);
                                                      $recentadmission = $result1 -> fetch_all(MYSQLI_ASSOC);
                                                      


                                                      if($s['consultant_id'] == $consultant['member_id'] ){

                                                      $decodedadmissiondx=json_decode($s['admissiondiagnosis']);
                                                      if( $decodedadmissiondx){
                                                        $tb_patient=array_intersect($decodedadmissiondx,$tb_list);
                                                      }else{
                                                        $tb_patient=array();
                                                      }


                                                      $today = date("Y-m-d");
                                                      $today1 = strtotime(date("Y-m-d"));
                                                      // $datediff = $today - strtotime($s['ADMDATE']);
                                                      // $LOS = $today->date_diff($s['ADMDATE'])->format("%a");
                                                      $timeDiff = abs($today1 - strtotime($s['ADMDATE']));

                                                      $LOS = $timeDiff/86400;  // 86400 seconds in one day

                                                      // $LOS= round($datediff / (60 * 60 * 24));
                                                      

                                                      
                                                    echo"  
                                                    <div class='col-sm-3'>
                                                
                                                    <div class='eachrow card'>
                                                    
                                                    
                                                    <div style='   display: inline; padding: 2%; text-align: center;' class='eachcol bed card-header'  scope='row' >";
                                                   
                                                    if ($s['newassign']=='1'){
                                                          echo"<p style='color: red;'><strong>New Patient</strong></p>";
                                                        
                                                        }
                                                        if ($recentadmission ){
                                                          echo"<div style='   background: #fd7e14'><strong> Readmission in 72 hours</strong></div>";
                                                        }
                                                        if ($tb_patient ){
                                                          echo"<div style='   background: #01ff70'><strong> TB Patient</strong></div>";
                                                        }
                                                        if ($s['delay'] !==Null){
                                                      echo"<div style='   background: gold'><strong> Discharged Still in</strong></div>";
                                                    }
                                                    if (!empty($s['longterm'])){
                                                      echo"<div style='      background: #87503e;color: white;'><strong> Long Term Patient</strong></div>";
                                                    }
                                                echo"
                                                      <p class='txtdata' name='bed' placeholder='Bed Number' style='text-align: center;'><strong>In ".$s['current_location']." Bed # ".$s['BED']." </strong></p>
                        
                                                     
                                                      </div>
                                                      
                                                      <div style=' margin: 0% 2%; 'class='eachcol mrn' >
                                                       <p class='txtdata' name='mrn'  style='text-align: center;'><strong> MRN: </strong>".$s['MRN']." </p>
                                                      </div>

                                                      <div style=' margin: 0% 2%; ' class='eachcol name'>
                                                      <label style='margin: 0px; text-align: center; '>Patient Name:</label>
                                                     
                                                      <p class='txtdata' name='mrn'  style='text-align: center;'>".$s['PNAME']." </p>
                                                      </div>";
                                                // echo"
                                                //       <div style=' margin: 0% 2%; '  class='eachcol admdate'  scope='row' >
                                                //       <label style='margin: 0px; text-align: center; '>Admission Date</label>
                                                //       <input type='text' onfocus='(this.type='date')' class='txtdata' name='admdate'  id='datepicker' value='".$s['ADMDATE']."' style='text-align: center;padding: 0px;' disabled>
                                                //       </div>";
                                                if($s['current_location'] == 'ICU'){
                                                  echo"   <div style='  margin: 2%; background: royalblue;color: white;' class='eachcol'    scope='row' >
                                                  <p style='text-align: center;margin-bottom: 0px;'>ICU patient</p>
                                                 
                                                          </div>
                                                          ";

                                                } else{
                                                      if ($LOS < $shortlos){
                                                        echo"   <div style=' margin: 0% 2%; background: #d4edda;' class='eachcol LOS'    scope='row' >";
                                                      } elseif   ($LOS > $longlos){
                                                        echo"   <div style=' margin: 0% 2%; background: #f8d7da;' class='eachcol LOS'   scope='row' >";
                                                      }elseif   ($LOS >= $shortlos){
                                                        echo"   <div style=' margin: 0% 2%; background: #fff3cd;' class='eachcol LOS'    scope='row' >";
                                                      }
                                                    

                                                      echo"
                                       
                                                      <p class='txtdata' name='mrn'  style='text-align: center;'><strong> Duration of Admission: </strong>".$LOS." </p>
                                                   
                                                      </div>
                                                      ";
                                                    }
                                                   
                                                 echo"
                                                 <div style=' margin: 0% 2%;' class='eachcol admissiondiagnosis'>
                                                 <label style='margin: 0px; text-align: center; '>Admission Diagnosis:</label>
                                                      <ul style='list-style-position: inside;margin: 1% 0% 5%;'>
                                                      ";
                                                
                                                      if (is_array($decodedadmissiondx)){
                                                        
                                                        foreach($decodedadmissiondx as $key => $value)
                                                  {
                                                    $formationSQL = "SELECT * FROM icd10 WHERE id='".$value."'";
                                                    $result1 = $mysqli->query($formationSQL);
                                                    $dxlist = $result1 -> fetch_array(MYSQLI_ASSOC);
                                                     
                                                
                                                      echo '<li>'.  $dxlist['name']. '</li>';
                                                  }}
                                                
                                                      echo"
                                                   </ul></div>
  
                                                  </div >
                                                  </div >   
                                                      ";
                                                        }
                                                     }



                                         ?> 
                     
        
                </div>
                <!-- /.row -->
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
            <?php } ?>
</div>
			
      
 </div> <!--row -->
	
 

</div><!--/. container-fluid -->

            

 </section>
    <!-- /.content -->
  

<!-- PAGE SCRIPTS -->

<!-- AdminLTE App -->
<script src="dist/js/adminlte.js"></script>

<!-- OPTIONAL SCRIPTS -->
<script src="dist/js/demo.js"></script>

<script>

// $('.SeeMore2').click(function(){
//     var $this = $(this);
//     $this.toggleClass('SeeMore2');
//     if($this.hasClass('SeeMore2')){
//         $this.text('+    ');         
//     } else {
//         $this.text('-    ');
//     }
// });

function showfunction(v) {
  var aaa = document.getElementById(v);
  // console.log(v);
    aaa.classList.toggle('SeeMore2');
    if(aaa.classList.contains('SeeMore2')){
      aaa.innerHTML='+    ';         
    } else {
      aaa.innerHTML='-    ';
    }


}
</script>
 




  <!-- Main Footer -->
  <footer class="main-footer" style="margin: 0px;">
    <strong>Desgined by <a href="https://www.healthpro.ai/main/">Healthpro.ai</a>.</strong>
    All rights reserved.
    <div class="float-right d-none d-sm-inline-block">
      <b>Version</b> 1.1
    </div>
  </footer>
  
</div>
<!-- ./wrapper -->

</body>
</html>

  