<?php 
session_start();

require_once "authCookieSessionValidate.php";

if(!$isLoggedIn) {
    header("Location: ./");
}
date_default_timezone_set('Asia/Riyadh');
$today=date("Y-m-d");


?>

  <!-- Navbar -->
<?php
require 'sidebar.php';
	require ('dbconnect.php');

  $userid=$user['member_id'];

if (in_array($user['position'],$access_PICU_endorsement)){
  $formationSQL = "SELECT * FROM members WHERE position ='3'";
  $result1 = $mysqli->query($formationSQL);
  $consultants = $result1 -> fetch_all(MYSQLI_ASSOC);
}else{
    $formationSQL = "SELECT * FROM members WHERE member_id = $userid";
		$result1 = $mysqli->query($formationSQL);
		$consultants = $result1 -> fetch_all(MYSQLI_ASSOC);

  }
  
// var_dump($user);
  if (!in_array($user['position'],$access_PICU_patients)){
    
    echo "
    <div class='content-wrapper'>
    
  
    <section class='content'>
    <div class='container-fluid'>  
    <div class='alert alert-danger' role='alert'> you dont have permission to access this page, Contact you manager if you need to.
    </div>
    </div>
    </section>
    </div>
    ";
    require 'footer.php';

    exit();
  }


  
if (isset($_POST['signoff_btn'])) {

    $consultid = $_POST['consultid'];
   
   /// if dscharge date = today... or yesterday
   
   
   $query = "UPDATE consultations SET signoff_date='".$today."' WHERE ID='".$consultid."'";
             if (!$mysqli -> query( $query)) {
               echo("Error description: " . $mysqli -> error);
             } else {
              
               echo "<script language='javascript'>\n";
               echo "window.location.href = 'dmc-new-consultation.php';";
               echo "</script>\n";
   
             }
     
   }


?>

     <style>
          .hidden{
                  display: none;
          }
          textarea {
    resize: none;
    overflow: hidden;
}

      </style>

        <script>



function auto_grow(element) {
    element.style.height = "5px";
    element.style.height = (element.scrollHeight)+"px";
}


function del(value){
  if(!confirm("Do you really want to delete this consultation?")) {
    return false;
  }
var rowname= "row";
rowname+=value;
row = document.getElementById(rowname);

    var id = value;
    data = {id: id};
    $.post('dmc-consultation-delete.php', data, function(data){
    // $(parent).html(data);
  });
  // location.reload();
  row.style.display = "none";
    }

function addconsult() {

// var rowname= "row";
// rowname+=value;
// row = document.getElementById(rowname);
//   var id = value;
bed_new=document.getElementById('bed_new').value;
mrn_new=document.getElementById('mrn_new').value;
pname_new=document.getElementById('pname_new').value;
age_new=document.getElementById('age_new').value;
consultdate_new=document.getElementById('consultdate_new').value;
consultfrom_new=document.getElementById('consultfrom_new').value;
current_location_new=document.getElementById('current_location_new').value;
entered_by=<?php echo $user['member_id']; ?>;
var checked = document.querySelectorAll('#indication_new :checked');
var indication_new = [...checked].map(option => option.value);
consultation_to_service=document.getElementById('consultation_to_service').value;
if (isNaN(consultation_to_service) == true){
consultant_new='N/A';
}else{
  consultant_new=document.getElementById('consultant_new').value;
}
var parent = document.getElementById('messsssage');
// alert(indication_new);
// get other
    if (indication_new.includes('0')){
      other_indication=document.getElementById('other_indication').value;
      // alert(other_indication);
  } else{
    other_indication="none";
    // alert(other_indication);
  }
if(bed_new==""){
            //do something
            // alert("name can not be null");
            parent.innerHTML="<p style='color:red'>   Please Fill all required informations</p>"
            return false;
            //this will not submit your form
        }
        else if(mrn_new==""){
            parent.innerHTML="<p style='color:red'>   Please Fill all required informations</p>"

            return false;
        }
        else if(pname_new==""){
            parent.innerHTML="<p style='color:red'>   Please Fill all required informations</p>"

          // alert(mrn_new);
            return false;
        }
        else if(age_new==""){
            parent.innerHTML="<p style='color:red'>   Please Fill all required informations</p>"

            return false;
        }
        else if(consultdate_new==""){
            parent.innerHTML="<p style='color:red'>   Please Fill all required informations</p>"

            return false;
        }
        else if(consultfrom_new==""){
            parent.innerHTML="<p style='color:red'>   Please Fill all required informations</p>"

            return false;
        }
        else if(indication_new==""){
            parent.innerHTML="<p style='color:red'>   Please Fill all required informations</p>"

            return false;
        }
        else if(current_location_new==""){
            parent.innerHTML="<p style='color:red'>   Please Fill Patient Location</p>"

            return false;
        }
        else if(consultation_to_service==""){
            parent.innerHTML="<p style='color:red'>   Please Fill Consultation To Service</p>"

            return false;
        }
        else if(consultant_new=="" && isNaN(consultation_to_service)){
          
            parent.innerHTML="<p style='color:red'>   Please Fill Consultation To</p>"

            return false;
        
        }
        else if(other_indication==""){
            parent.innerHTML="<p style='color:red'>   Please Fill Other Indications</p>"

            return false;
        }
        else{
          data = {bed_new: bed_new, mrn_new: mrn_new, pname_new: pname_new, current_location_new:current_location_new,consultant_new:consultant_new,
            age_new:age_new, consultdate_new:consultdate_new,consultfrom_new:consultfrom_new,entered_by:entered_by,
             indication_new:indication_new,other_indication:other_indication, consultation_to_service:consultation_to_service};
            
            $.post('dmc-consultation-add.php', data, function(data){
$(parent).html(data);
return false
// location.reload();
});
// alert(data);
//This will submit your form.
        }
// alert(patientId);
//   row.style.display = "none";
  }


	</script>
      
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
  
	<?php
   		
		$formationSQL = "SELECT * FROM consultations WHERE signoff_date IS NULL";
		$result1 = $mysqli->query($formationSQL);
		$activeconsultations = $result1 -> fetch_all(MYSQLI_ASSOC);

	?>



    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Active Consultations</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="dashboard.php">Home</a></li>
              <li class="breadcrumb-item active">Active Consultations</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">  
      

<div class="row">

 <div id="mypresentersTable" class="col-md-12">

            <!-- /.info-box -->
            
           <?php
           $sub=null;
           $total=0;
           $subtotal=0;

           if (in_array($user['position'],$access_PICU_endorsement)){

              foreach($activeconsultations as $s){
               if(is_null($s["signoff_date"]) == 1 ){
                $total++;
               
            }
          }
             
          }else{

            foreach ($consultants as $consultant){
              foreach($activeconsultations as $s){
               if(is_null($s["signoff_date"]) == 1 ){
                $total++;
                 if($s['consultant_id'] == $consultant['member_id'] ){
                   $subtotal++;
                   $sub= $subtotal . " out of ";
              }
            }
          }
             }
            }
          // var_dump($activeconsultations);
           ?>
            <div class="card">
              <div  class="card-header">
                <h3 class="card-title"><i class="fas fa-user-tie text-info"></i> Active Consultations List: <?php echo $sub. $total; ?> Consultation</h3>
                <div id="addbtn" class='eachrow' style=' float: right; '>
                  <?php
                   if (in_array($user['position'],$access_PICU_patients)){

                echo "<a  class='btn btn-success'  href='#new_consultation_modal' data-toggle='modal'  style='color: aliceblue; line-height: 2;padding: 0px 15px;'>New Consultation</a>
               ";
                  }
                  ?>
          </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <div class="row">
<!--          
                          <table class="col-md-12" >
                            <thead   style="text-align: center;font-weight: 700;">
                            <tr>
                              <td class='col-md-1' >Bed #</td>
                              <td class="col-md-2">MRN</td>
                              <td class="col-md-2">Patient Name</td>
                              <td class="col-md-2">Age</td>
                              <td class="col-md-2">Gender</td>
                              <td class="col-md-2">Nationality</td>
                              <td class="col-md-2">Admission Date</td>
                              <td class="col-md-2">Admission From</td>
                              <td class="col-md-3">Diagnosis</td>
                            </tr>
                            </thead> -->
                                         <?php

                                         $otherconsults = array();
                                         $otherconsults['member_id']=0;
                                         array_push($consultants,$otherconsults);
                                         foreach ($consultants as $consultant){
                                                     foreach($activeconsultations as $s){
                                                      $total++;
                                                      if(is_null($s["signoff_date"]) == 1 ){
                                                     
                                                        if($s['consultant_id'] == $consultant['member_id'] ){

                                                      $decodedindications=json_decode($s['indication']);
                                                  
                                          
                                                    
                                                    echo"  
                                                    <div class='col-sm-3'>
                                                    <div class='eachrow card'  style='text-align: center;' id='row".$s['id']."'>
                                                    <div style='  margin: 2%; display: none; ' class='eachcol id'  scope='row' >
                                                   
                                                    <input class='txtdata' type='hidden' name='id' id='id' value='".$s['id']."' style='text-align: center;width: 85%;' >
                                                   
                                                    </div>
                                                    <div style='  margin: 2%; display: inline; ' class='eachcol bed card-header'  scope='row' >
                                                    ";
                                                    if (in_array($user['position'],$access_PICU_control)){
                                                    echo "<a  type='button' class='btn-tool'  style='display: contents;'   onclick='del(".$s['id'].")'><i class='fa fa-trash'></i>  </a>";
                                                    }


                                                echo"
                                                    <label style=' text-align: center;margin-bottom: 0px;'>Consultation Location</label>
                                                    <input class='txtdata' name='current_location' placeholder='Current Location' value='".$s['current_location']."' style='text-align: center;' disabled >

                                                    </select>

                                               
                                                    <label style='text-align: center;margin-bottom: 0px;'>Bed</label>  
                                                    <input class='txtdata' name='bed' placeholder='Bed Number' value='".$s['BED']."' style='text-align: center;' disabled>
                                                      
                                                     
                                                      </div>
                                                      
                                                      <div style='  margin: 2%; display: inline; ' class='eachcol mrn' >
                                                      <label style='text-align: center;margin-bottom: 0px;'>MRN</label>
                                                        <input class='txtdata' name='mrn' value='".$s['MRN']."' style='text-align: center;' disabled>
                                                      </div>

                                                      <div style='  margin: 2%; display: inline; ' class='eachcol name'>
                                                      <label style='text-align: center;margin-bottom: 0px;'>Patient Name</label>
                                                      <input class='txtdata'  name='name' value='".$s['PNAME']."' style='text-align: center;' disabled>
                                                      </div>
                                                
                                                      <div style='  margin: 2%; display: inline; ' class='eachcol age'>
                                                      <label style='text-align: center;margin-bottom: 0px;'>Age</label>
                                                      <input class='txtdata' name='age' value='".$s['age']."' style='text-align: center;' disabled>
                                                      </div>
                                                      
                                                      <div style='  margin: 2%; display: inline; ' class='eachcol consultationdate'>
                                                      <label style='text-align: center;margin-bottom: 0px;'>Consulted On</label>
                                                      <input class='txtdata' name='age' value='".$s['consultation_date']."' style='text-align: center;' disabled>

                                                  </div>
                                                  <div style='  margin: 2%; display: inline; ' class='eachcol consultationfrom'>
                                                  <label style='text-align: center;margin-bottom: 0px;'>Consultation From</label>
                                                  <input class='txtdata' name='age' value='".$s['consultation_from']."' style='text-align: center;' disabled>

                                                    </div>

                                                    <div style='  margin: 2%; display: inline; ' class='eachcol consultationtoservice'>
                                                    <label style='text-align: center;margin-bottom: 0px;'>Consultation To Service</label>";

                                                    if (is_numeric($s['consultation_to_service'])){
                                                      $formationSQL = "SELECT * FROM speciality WHERE id='".$s['consultation_to_service']."'";
                                                      $result1 = $mysqli->query($formationSQL);
                                                      $spppp = $result1 -> fetch_array(MYSQLI_ASSOC);
                                                    echo "<input class='txtdata' name='age' value='".$spppp['specilaity']."' style='text-align: center;' disabled>";
                                                  } else{
                                                    echo "<input class='txtdata' name='age' value='".$s['consultation_to_service']."' style='text-align: center;' disabled>";
                                                  }

                                                    echo"
                                                      </div>

                                                    <div style='  margin: 2%; display: inline; ' class='eachcol consultationto'>
                                                  <label style='text-align: center;margin-bottom: 0px;'>Consulted Doctor</label>";

                                                  $formationSQL = "SELECT * FROM members WHERE member_id='".$s['consultant_id']."'";
                                                    $result1 = $mysqli->query($formationSQL);
                                                    $primary = $result1 -> fetch_array(MYSQLI_ASSOC);
                                                   echo"
                                                  <input class='txtdata' name='consultationto' value='".$primary['full_name']."' style='text-align: center;' disabled>

                                                 </div>
                                                 <div style='  margin: 2%; display: inline; ' class='eachcol indications'>
                                                 <label style='text-align: center;margin-bottom: 0px;'>Indications</label>
                                                 <ul style='list-style-position: inside;'>";
                                                
                                                      if (is_array($decodedindications)){
                                                        
                                                        foreach($decodedindications as $key => $value)
                                                  {
                                                    $formationSQL = "SELECT * FROM consultation_reason WHERE id='".$value."'";
                                                    $result1 = $mysqli->query($formationSQL);
                                                    $indlist = $result1 -> fetch_array(MYSQLI_ASSOC);
                                                     
                                                
                                                      echo '<li>'.  $indlist['consultation_reason']. '</li>';
                                                  }}
                                                
                                                      echo"
                                                      </ul>
                                                
                                                      </div>
                                                      
                                                      <div style='  margin: 2%; display: inline; ' class='eachcol other_indications'>
                                                      <label style='text-align: center;margin-bottom: 0px;'>Other Indications</label>
                                                      <p style='text-align: center;'>".$s['other_ind']."</p>

                                                     
                                                           </div>
                                                           
                                                           ";

                                                      
                                                      if ($user['member_id'] == $s['consultant_id'] OR $user['manage_patient'] == '1'){
                                                        echo"
                                                        <form method='post' name='signoff' action='dmc-new-consultation.php'>
                                               <input type='hidden' name='consultid' value='".$s['id']."'>
                                                        <button type='submit' value='submit' class='btn btn-danger' style='color: aliceblue;line-height: 2;margin-top: 3%;padding: 0px 10%;width: 100%;'  id= 'signoff_btn' name= 'signoff_btn'>Sign Off</button>
                                                        </form>
                                                        ";
                                                        echo "<a class='btn btn-info' href='#details_modal' data-book-id='".$s['id']."' data-toggle='modal'  style='color: aliceblue;line-height: 2;margin-top: 3%;padding: 0px 10%;width: 100%;'>Modify</a>";

                                                        }
                                                    //   if ($user['position']== '3'){

                                                    //     echo "
                                                    //     <form autocomplete='off' id='assignme' method='POST' action='dmc-new-admissions.php'>
                                                    //     <input class='txtdata' type='hidden' name='patientid' value='".$s['ID']."' style='text-align: center;' required>
                                                    //     <input class='txtdata' type='hidden' name='userid' value='".$user['member_id']."' style='text-align: center;' required>
                                                    //     <button  class='btn btn-success' type='submit' name='assign_me_btn'  style='color: aliceblue; line-height: 2;padding: 0px 15px;' form='assignme' value='Submit'>Assign to me</button>
                                                    //     </form>";
                                                    //       }

                                                    //       if ($user['assign_access']== '1'){

                                                    //         echo "
                                                    //         <a class='btn btn-danger'  href='#assignprimary_modal' data-toggle='modal'  data-book-id='".$s['ID']."'  style='color: aliceblue;line-height: 2;margin-top: 3%;padding: 0px 10%;width: 100%;'  id= 'assignprimary'>Assign To Primary</a>";
                                                    //           }
                                                      echo "</div >
                                                      </div>
                                                      ";
                                                        } 
                                                      }
                                                     }
                                                    }
                                         ?> 
                        </table>


        
                    
    
                  
                  <!-- /.col -->
                </div>
                <!-- /.row -->
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
         
</div>
			

           
 </div> <!--row -->

 

<!-- PAGE SCRIPTS -->

<!-- AdminLTE App -->
<script src="dist/js/adminlte.js"></script>

<!-- OPTIONAL SCRIPTS -->
<script src="dist/js/demo.js"></script>


<?php
	



?>
</div><!--/. container-fluid -->


    

    </section>
    <!-- /.content -->
  
<!-- Modal patient details-->

<div class="modal" id="details_modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
     
          <h4 class="modal-title">Consultation Details</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
    <!-- data retrived from JS -->
      <div class="modal-body">
      <div id="cdetailsdiv"></div>
      </div>
     
    
  </div>
</div>
</div>    

<!-- Modal for admitting patient -->

<div class="modal" id="new_consultation_modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
     
          <h4 class="modal-title">Add New Consultation</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <p>Kindly Complete the Required Information</p>



        <form autocomplete="off"  method="POST">
        <label>Consultation Location</label>
        <select class='' id='current_location_new' style='width: 100%; padding: 4px;text-align: center;' required>
      <option value='Ward'>Ward</option>
      <option value='ICU'>ICU</option>
      <option value='ER'>ER</option>
    </select>
        <label>Bed Number</label>
        <input class='' id='bed_new' value='' style='text-align: center;' required>
        <label>MRNs</label>
        <input class='' id='mrn_new' value='' style='text-align: center;' required>
        <label>Patient Name</label>
        <input class=''  id='pname_new' value='' style='text-align: center;' required>
        <label>Age</label>
        <input class='' id='age_new' style='text-align: center;' required>
        
      

        <label>Consultation date</label>
        <input value='' class='' id ="consultdate_new"  data-date-format="DD-MM-YYYY" type="text"  name='consultdate_new' style="text-align: center;padding: 0px;" required>
    
    <label>Consultation Indication</label>
    <select class='txtdata1 select2' style='width: 100%;'  oninput='auto_grow(this)'  multiple='multiple' id='indication_new' required>
<?php
$formationSQL = "SELECT * FROM consultation_reason";
$result1 = $mysqli->query($formationSQL);
$indlist1 = $result1 -> fetch_all(MYSQLI_ASSOC);
      foreach($indlist1 as $i)
      {
        
         
    
          echo '<option value="' . $i['id'] . '">'.  $i['consultation_reason']. '</option>';
      }
?>

</select>


<div id="other" style="display: none; margin-top:2%;">
<label>Write Indications</label>
<input class='' id='other_indication' style='text-align: center;'>
    </div>


<label>Consultation From</label>
    <select class='select2' style='width: 100%;'   id='consultfrom_new' required>
    <option disabled selected>Select</option>

<?php
$formationSQL = "SELECT * FROM speciality";
$result1 = $mysqli->query($formationSQL);
$speciality = $result1 -> fetch_all(MYSQLI_ASSOC);
      foreach($speciality as $sp)
      {
        echo "<option value='".$sp['specilaity']."'>".$sp['specilaity']."</option>";
    }
    
      $formationSQL = "SELECT * FROM other_specialities";
$result1 = $mysqli->query($formationSQL);
$otherspeciality = $result1 -> fetch_all(MYSQLI_ASSOC);
      foreach($otherspeciality as $osp)
      {
          echo "<option value='".$osp['specilaity']."'>".$osp['specilaity']."</option>";

      }
    
?>


</select>

<label>Consultation To Service</label>
<select class='select2 txtdata11' style="width: 100%;" id='consultation_to_service' name='consultation_to_service' style='text-align: center;' required>
<option selected disabled> Select</option>



<?php
$formationSQL = "SELECT * FROM speciality";
$result1 = $mysqli->query($formationSQL);
$speciality = $result1 -> fetch_all(MYSQLI_ASSOC);
      foreach($speciality as $sp)
      {
        echo "<option value='".$sp['id']."'>".$sp['specilaity']."</option>";
    }
    
      $formationSQL = "SELECT * FROM other_specialities";
$result1 = $mysqli->query($formationSQL);
$otherspeciality = $result1 -> fetch_all(MYSQLI_ASSOC);
      foreach($otherspeciality as $osp)
      {
          echo "<option value='".$osp['specilaity']."'>".$osp['specilaity']."</option>";

      }
    
?>

</select>


<div id="dropdown" style="display: none; margin-top:2%;">
<label>Consultation To</label>
<select class='select2_modify txtdata'  id='consultant_new' name='consultant_new' style='text-align: center;' required> </select>

</div>

<div id='messsssage'></div>
      </div>
    
      <div class="modal-footer">
      
<?php
     echo" <button type='button' class='btn btn-success'  onclick='addconsult()'>Add Consultation</button>";
  ?>
      <button type="button" class="btn btn-default" style=" color: black; " data-dismiss="modal">Close</button>
      </div>

          </form>
     

    </div>
  </div>
</div>


  </div>  


  <!-- /.content-wrapper -->


  <script type="text/javascript">
      $(document).ready(function() {
        $('.select2').select2({
      placeholder: 'Select',
      
    } );

      });

    </script>

<script>

// document.getElementById('addpatient').onclick = function(){
//   var parent = $(this).parent('.eachrow');
//   var attribChanged = $(this).attr('name');
//   data = {attribChanged: attribChanged};
//   $.post('dmc-patients-add.php', data, function(data){
//   $(parent).html(data);
//   location.reload();
// });
// }



$("textarea").each(function(textarea) {
    $(this).height( $(this)[0].scrollHeight );
});


  </script>


<script>


$(function() {
  $('input[name="consultdate_new"]').daterangepicker({
    singleDatePicker: true,
    timePicker: false,
    timePicker24Hour: false,
    showButtonPanel: false,
    autoUpdateInput: false,
    autoApply: true,
    showDropdowns: true,
    minYear: 2010,
    maxYear: parseInt(moment().format('YYYY'),10),
    locale: {
            format: 'YYYY-MM-DD'
        }
  }, ).on("apply.daterangepicker", function (e, picker) {
        picker.element.val(picker.startDate.format(picker.locale.format));
    });
});



$(document).ready(function($) {
  
  $('.txtdata1').on('change', function(){
    var indication_new = document.getElementById("indication_new").selectedOptions;
    var values = Array.from(indication_new).map(({ value }) => value);
    // var specialty_inner1 = document.getElementById("specialty_transfer");
    // var specialty_inner=specialty_inner1.options[specialty_inner1.selectedIndex].text;
    // var dropdownDIV = document.getElementById('dropdown');
    if (values.includes('0')){
    document.getElementById("other").style.display ='block';
  }
// alert (values);
});
});


$(document).ready(function($) {
  
  $('.txtdata11').on('change', function(){
    var consultation_to_service = document.getElementById("consultation_to_service").value;
    // var specialty_inner1 = document.getElementById("specialty_transfer");
    // var specialty_inner=specialty_inner1.options[specialty_inner1.selectedIndex].text;
    // var dropdownDIV = document.getElementById('dropdown');
    document.getElementById("dropdown").style.display ="block";
data = {consultation_to_service: consultation_to_service};
$.post('dmc-consultation-specialty-dropdown.php', data, function(data){
$("#dropdown").html(data);

});
// $(this).parent('.eachcol').css("backgroundColor", "#90EE90");

});
});


$('#details_modal').on('show.bs.modal', function(e) {
 
 //  var bookId = $(e.relatedTarget).data('book-id');
  var bookId = $(e.relatedTarget).data('book-id');
 //  $(e.currentTarget).find('input[name="patientId"]').val(bookId);
 
 
  data = {bookId: bookId};
  $.post('dmc-consultation-details.php', data, function(data){
  $('#cdetailsdiv').html(data);
      
     });
 });

</script>
<?php

require 'footer.php';

?>

