<?php 
session_start();
// date_default_timezone_set('Asia/Riyadh');
$today=date("Y-m-d");

require ('../dbconnect.php');
$time=$_REQUEST['timing2'];
$date=$_REQUEST['date2'];
// $time = "quarterly";
?>
<script src="https://cdn.jsdelivr.net/npm/chart.js"></script>

<?php
$label=array();
$admissions=array();
$discharges=array();
$toicu=array();
$icumortalitya=array();
$mortalitya=array();
$newconsults=array();
$signedoff=array();
$beddays=array();
$LOS=array();
$weekend=array();
$weekend_p=array();
$LSP=array();
$total_patients=array();
$LSP_P=array();
$m_LOS=array();
$icuaverageLOSa=array();
$readmission=array();

if ($time == "daily"){
    $title ='Daily Overview';
 
// $date=new DateTime();
$date1 = date("Y-m-t", strtotime($date));
$ydate1=date("Y",strtotime($date1));
$mdate1=date("m",strtotime($date1));
$ds=cal_days_in_month(CAL_GREGORIAN,$mdate1,$ydate1);
// echo $date;
// echo $date1;
$n=0;
while($n < $ds){
  $month = date('F', strtotime($date1));
  $day =date ('d', strtotime($date1));

  $formationSQL = "SELECT * FROM picupatients WHERE ADMDATE = '".$date1."' AND (current_location != 'ICU' or current_location is null)";
  $result1 = $mysqli->query($formationSQL);
  $admittedpcount = mysqli_num_rows($result1);

  $formationSQL = "SELECT * FROM picupatients WHERE DISDATE = '".$date1."' AND (current_location != 'ICU' or current_location is null)";
  $result1 = $mysqli->query($formationSQL);
  $dischargedpcount = mysqli_num_rows($result1);
  
  $formationSQL = "SELECT * FROM consultations WHERE consultation_date = '".$date1."'";
  $result1 = $mysqli->query($formationSQL);
  $newconsultscount = mysqli_num_rows($result1);

  $formationSQL = "SELECT * FROM consultations WHERE signoff_date = '".$date1."'";
  $result1 = $mysqli->query($formationSQL);
  $signedoffcount = mysqli_num_rows($result1);

    ///// Trans to ICU
    $formationSQL = "SELECT DISDATE FROM picupatients WHERE  DISDATE = '".$date1."' AND DISTO = 'Intensive Care (ICU)'";
    $result1 = $mysqli->query($formationSQL);
    $transtoicu = mysqli_num_rows($result1);

            ///// Mortality in ICU
            $formationSQL = "SELECT DISDATE FROM picupatients WHERE DISDATE = '".$date1."'  AND current_location = 'ICU' AND MORTALITY = 'Dead'";
            $result1 = $mysqli->query($formationSQL);
            $icumortality = mysqli_num_rows($result1);
            
            ///// Mortality
            $formationSQL = "SELECT DISDATE FROM picupatients WHERE DISDATE = '".$date1."'  AND (current_location != 'ICU' or current_location is null) AND MORTALITY = 'Dead'";
            $result1 = $mysqli->query($formationSQL);
            $mortality = mysqli_num_rows($result1);
              //////////////

              
/////////////////////
    // readmissions
//////////////////////////
$formationSQL = "SELECT ID, MRN, ADMDATE FROM picupatients WHERE ADMDATE = '".$date1."' AND YEAR(ADMDATE) = '".$ydate1."'";
$result1 = $mysqli->query($formationSQL);
$admitted_patients = $result1 -> fetch_all(MYSQLI_ASSOC);

$readmission_count=0;
// echo $date1 ."</br>";
foreach ($admitted_patients as $s){
$formationSQL = "SELECT * FROM picupatients WHERE DISDATE + INTERVAL 3 DAY >='".$s['ADMDATE']."' AND ID < '".$s['ID']."'  AND MRN='".$s['MRN']."' AND (trans_discharge = 'discharge from ICU' or trans_discharge='discharge from ward' or trans_discharge IS NULL) LIMIT 1";
$result1 = $mysqli->query($formationSQL);
$recentadmission = mysqli_num_rows($result1);
$readmission_count=$readmission_count+$recentadmission;
// var_dump($recentadmission);
}



  $datelabel= $day ;
  array_push($label,$datelabel);
  array_push($admissions,$admittedpcount);
  array_push($discharges,$dischargedpcount);
  array_push($toicu,$transtoicu);
  array_push($newconsults,$newconsultscount);
  array_push($signedoff,$signedoffcount);
  array_push($readmission,$readmission_count);
  array_push($icumortalitya,$icumortality);
  array_push($mortalitya,$mortality);

$n++;
$date1 = date("Y-m-d", strtotime("-1 day", strtotime($date1)));

}

$label=array_reverse($label);
$admissions=array_reverse($admissions);
$discharges=array_reverse($discharges);
$toicu=array_reverse($toicu);
$newconsults=array_reverse($newconsults);
$signedoff=array_reverse($signedoff);
$readmission=array_reverse($readmission);
$mortalitya=array_reverse($mortalitya);
$icumortalitya=array_reverse($icumortalitya);
?>
<div class="table-responsive text-nowrap">
<table class="table table-striped">
<thead>
  <tr>
    <th scope="col">KPI for <?php echo $month ." ". $ydate1; ?></th>
<?php
foreach ($label as $l){
  echo "<th>" . $l . "</th>" ;
}
?>
</tr>
</thead>
<tbody>
  <tr>
    <th>Admissions</th>
    <?php
foreach ($admissions as $a){
  echo "<td>" . $a . "</td>" ;
}
?>
  </tr>
  <tr>
    <th>Discharges</th>
    <?php
foreach ($discharges as $d){
  echo "<td>" . $d . "</td>" ;
}
?>
  </tr>
  <tr>
    <th>Transfer to ICU</th>
    <?php
foreach ($toicu as $t){
  echo "<td>" . $t . "</td>" ;
}
?>
  </tr>
  <tr>
    <th>Consultations</th>
    <?php
foreach ($newconsults as $n){
  echo "<td>" . $n . "</td>" ;
}
?>
  </tr>
  <tr>
    <th>Sign Offs</th>
    <?php
foreach ($signedoff as $s){
  echo "<td>" . $s . "</td>" ;
}
?>
  </tr>
  <tr>
    <th>72 hrs Readmissions</th>
    <?php
foreach ($readmission as $r){
  echo "<td>" . $r . "</td>" ;
}
?>
  </tr>
  <tr>
    <th>ICU Mortality</th>
    <?php
foreach ($icumortalitya as $icm){
  echo "<td>" . $icm . "</td>" ;
}
?>
  </tr>
  <tr>
    <th>Out ICU Mortality</th>
    <?php
foreach ($mortalitya as $m){
  echo "<td>" . $m . "</td>" ;
}
?>
  </tr>
</tbody>
</table>
</div>

<?php




} elseif ($time == "monthly"){





 $title ='Monthly Overview';

 $date1 = date("Y-12-1", strtotime($date));

    
    $n=0;
    while($n < 12){

      $ydate1=date("Y",strtotime($date1));
      $mdate1=date("m",strtotime($date1));
      $last_day_ofmonth=date("Y-m-t", strtotime($date1));
      $first_day_ofmonth=date("Y-m-01", strtotime($date1));

      $dateObj   = DateTime::createFromFormat('!m', $mdate1);
      $monthName = $dateObj->format('F'); // March

      $formationSQL = "SELECT * FROM picupatients WHERE MONTH(ADMDATE) = '".$mdate1."' AND YEAR(ADMDATE) = '".$ydate1."' AND (current_location != 'ICU' or current_location is null)";
      $result1 = $mysqli->query($formationSQL);
      $admittedpcount = mysqli_num_rows($result1);

      $formationSQL = "SELECT * FROM picupatients WHERE MONTH(DISDATE) = '".$mdate1."' AND YEAR(DISDATE) = '".$ydate1."' AND (current_location != 'ICU' or current_location is null)";
      $result1 = $mysqli->query($formationSQL);
      $dischargedpcount = mysqli_num_rows($result1);
      
      $formationSQL = "SELECT * FROM consultations WHERE MONTH(consultation_date) = '".$mdate1."' AND YEAR(consultation_date) = '".$ydate1."'  ";
      $result1 = $mysqli->query($formationSQL);
      $newconsultscount = mysqli_num_rows($result1);

      $formationSQL = "SELECT * FROM consultations WHERE MONTH(signoff_date) = '".$mdate1."' AND YEAR(signoff_date) = '".$ydate1."'  ";
      $result1 = $mysqli->query($formationSQL);
      $signedoffcount = mysqli_num_rows($result1);
    
    ///// Trans to ICU
    $formationSQL = "SELECT DISDATE FROM picupatients WHERE MONTH(DISDATE) = '".$mdate1."' AND YEAR(DISDATE) = '".$ydate1."' AND DISTO = 'Intensive Care (ICU)'";
    $result1 = $mysqli->query($formationSQL);
    $transtoicu = mysqli_num_rows($result1);

    ///// Mortality in ICU
    $formationSQL = "SELECT DISDATE FROM picupatients WHERE MONTH(DISDATE) = '".$mdate1."' AND YEAR(DISDATE) = '".$ydate1."' AND current_location = 'ICU' AND MORTALITY = 'Dead'";
    $result1 = $mysqli->query($formationSQL);
    $icumortality = mysqli_num_rows($result1);
    
    ///// Mortality
    $formationSQL = "SELECT DISDATE FROM picupatients WHERE MONTH(DISDATE) = '".$mdate1."' AND YEAR(DISDATE) = '".$ydate1."' AND (current_location != 'ICU' or current_location is null) AND MORTALITY = 'Dead'";
    $result1 = $mysqli->query($formationSQL);
    $mortality = mysqli_num_rows($result1);
      //////////////

      //////////////
      // Medical Los
      /////////////
  
      $formationSQL = "SELECT ADMDATE, med_DISDATE FROM picupatients WHERE DISDATE IS NOT NULL AND MONTH(DISDATE) = '".$mdate1."' AND YEAR(ADMDATE) = '".$ydate1."' AND (current_location != 'ICU' or current_location is null)";
      $result1 = $mysqli->query($formationSQL);
      $datesss = $result1 -> fetch_all(MYSQLI_ASSOC);
      
      // echo $mdate1 . "</br>";
    
      $los=array();
      
      foreach ($datesss as $d){
        $timeDiff = abs(strtotime($d['ADMDATE']) - strtotime($d['med_DISDATE']));
      
        array_push($los,$timeDiff/86400);
        
      }
      // var_dump($los);
      // $a = array_filter($los);
      if(count($los) > 0) {
        $m_average = array_sum($los)/count($los);
      } else {
        $m_average = 0;
      }
     
       //////////////
      // physical Los
      /////////////
  
      $formationSQL = "SELECT ADMDATE, DISDATE FROM picupatients WHERE DISDATE IS NOT NULL AND MONTH(DISDATE) = '".$mdate1."' AND YEAR(ADMDATE) = '".$ydate1."' AND (current_location != 'ICU' or current_location is null)";
      $result1 = $mysqli->query($formationSQL);
      $datesss = $result1 -> fetch_all(MYSQLI_ASSOC);
      
      // echo $mdate1 . "</br>";
    
      $los=array();
      
      foreach ($datesss as $d){
        $timeDiff = abs(strtotime($d['ADMDATE']) - strtotime($d['DISDATE']));
      
        array_push($los,$timeDiff/86400);
        
      }
      // var_dump($los);
      // $a = array_filter($los);
      if(count($los) > 0) {
        $average = array_sum($los)/count($los);
      } else {
        $average = 0;
      }

       //////////////
      // ICU physical Los
      /////////////
  
      $formationSQL = "SELECT ADMDATE, DISDATE FROM picupatients WHERE DISDATE IS NOT NULL AND MONTH(DISDATE) = '".$mdate1."' AND YEAR(ADMDATE) = '".$ydate1."' AND current_location ='ICU'";
      $result1 = $mysqli->query($formationSQL);
      $icudatesss = $result1 -> fetch_all(MYSQLI_ASSOC);
      
      // echo $mdate1 . "</br>";
    
      $los=array();
      
      foreach ($icudatesss as $d){
        $timeDiff = abs(strtotime($d['ADMDATE']) - strtotime($d['DISDATE']));
      
        array_push($los,$timeDiff/86400);
        
      }
      // var_dump($los);
      // $a = array_filter($los);
      if(count($los) > 0) {
        $icuaverageLOS = array_sum($los)/count($los);
      } else {
        $icuaverageLOS = 0;
      }

////////////////////////////////////
//long stays > 30 days
////////////////////////////////////
// $formationSQL = "SELECT * FROM picupatients WHERE ADMDATE < '".$first_day_ofmonth."' AND DISDATE >= '".$last_day_ofmonth."' AND (current_location != 'ICU' or current_location is null)";
if (strtotime($last_day_ofmonth) <= strtotime($today)) {
$formationSQL = "SELECT * FROM picupatients WHERE ((DISDATE IS NOT NULL AND ADMDATE < '".$first_day_ofmonth."' AND DISDATE >= '".$last_day_ofmonth."') OR (DISDATE IS NULL AND ADMDATE + INTERVAL 30 DAY < '".$last_day_ofmonth."')) AND (current_location != 'ICU' or current_location is null)";
$result1 = $mysqli->query($formationSQL);
$LSP_count = mysqli_num_rows($result1);
// echo $last_day_ofmonth."</br>";
// var_dump($patient);

// total number of patients at that month
$formationSQL = "SELECT * FROM picupatients WHERE (
  (DISDATE IS NULL AND ADMDATE <= '".$last_day_ofmonth."') 
 OR (DISDATE IS NOT NULL AND DISDATE >= '".$first_day_ofmonth."' AND DISDATE <= '".$last_day_ofmonth."') 
 OR (DISDATE IS NOT NULL AND ADMDATE < '".$first_day_ofmonth."' AND DISDATE > '".$last_day_ofmonth."') 
 OR   (ADMDATE >= '".$first_day_ofmonth."' AND ADMDATE <= '".$last_day_ofmonth."')
 ) AND (current_location != 'ICU' or current_location is null)";
 $result1 = $mysqli->query($formationSQL);
$total_count = mysqli_num_rows($result1);

}else{
/////////////// last day of the month not yet came to calculate
$LSP_count='Pending';
$total_count='Pending';
}

       
////////////////////////////////////////////////////////////////
// Number of bed days
////////////////////////////////////////////////////////////////
// $formationSQL = "SELECT * FROM picupatients WHERE ADMDATE = '".$date1."' AND (DISDATE = '".$date1."' OR DISDATE IS NULL)";


$ds=cal_days_in_month(CAL_GREGORIAN,$mdate1,$ydate1);
$monthly_beddayscount=0;
$allweekend_discharge=0;
$date_day= $date1;
// echo $date_day;

    for ($x = 1; $x <= $ds; $x++) {
        // Where not in ICU and where not discharged at the same day of admission
        // echo $date_day ."</br>";
        if (strtotime($date_day) <= strtotime($today)) {
        $formationSQL = "SELECT * FROM picupatients WHERE ADMDATE <= '".$date_day."' AND (DISDATE >= '".$date_day."' OR DISDATE IS NULL) AND NOT DISDATE <=> ADMDATE  AND (current_location != 'ICU' or current_location is null)";
        $result1 = $mysqli->query($formationSQL);
        $dayscount = mysqli_num_rows($result1);
        $monthly_beddayscount=$monthly_beddayscount+$dayscount;


      //weekend discharges
      if (date('w', strtotime($date_day)) == 6 || date('w', strtotime($date_day)) == 5){
          // echo $date_day . "</br>";
      $formationSQL = "SELECT * FROM picupatients WHERE DISDATE = '".$date_day."' AND (current_location != 'ICU' or current_location is null)";
      $result1 = $mysqli->query($formationSQL);
      $weekend_discharge = mysqli_num_rows($result1);
      $allweekend_discharge=$allweekend_discharge+$weekend_discharge;
        }
     


      }
        $date_day= date('Y-m-d', strtotime($date_day . ' +1 day'));
    }
/////////////////////
    ///// readmissions
//////////////////////////
// echo $mdate1 ."</br>";
$formationSQL = "SELECT * FROM picupatients WHERE MONTH(ADMDATE) = '".$mdate1."' AND YEAR(ADMDATE) = '".$ydate1."'";
$result1 = $mysqli->query($formationSQL);
$admitted_patients = $result1 -> fetch_all(MYSQLI_ASSOC);

$readmission_count=0;

foreach ($admitted_patients as $s){
  $formationSQL = "SELECT * FROM picupatients WHERE DISDATE + INTERVAL 3 DAY >='".$s['ADMDATE']."' AND ID <'".$s['ID']."'  AND MRN='".$s['MRN']."' AND (trans_discharge = 'discharge from ICU' or trans_discharge='discharge from ward' or trans_discharge IS NULL) LIMIT 1";
  $result1 = $mysqli->query($formationSQL);
$recentadmission = mysqli_num_rows($result1);
// $recentadmission1 = $result1 -> fetch_array(MYSQLI_ASSOC);
// var_dump($recentadmission1);
$readmission_count=$readmission_count+$recentadmission;
}

/////////////////
      array_push($label,$monthName);
      array_push($admissions,$admittedpcount);
      array_push($discharges,$dischargedpcount);
      array_push($toicu,$transtoicu);
      array_push($icumortalitya,$icumortality);
      array_push($mortalitya,$mortality);
      array_push($newconsults,$newconsultscount);
      array_push($signedoff,$signedoffcount);
      array_push($beddays,$monthly_beddayscount);
      array_push($LSP,$LSP_count);
      array_push($total_patients,$total_count);
      array_push($readmission,$readmission_count);


      if($average>0){
      array_push($LOS,(number_format(($average), 2, '.', '')." Days"));
      } else {
        array_push($LOS,0);
              }

      
      if($m_average>0){
      array_push($m_LOS,(number_format(($m_average), 2, '.', '')." Days"));
      } else {
        array_push($m_LOS,0);
              }

              if($icuaverageLOS>0){
                array_push($icuaverageLOSa,(number_format(($icuaverageLOS), 2, '.', '')." Days"));
                } else {
                  array_push($icuaverageLOSa,0);
                        }

      if($dischargedpcount>0){
        
        array_push($weekend,(number_format($allweekend_discharge, 0, '.', '').""));
      array_push($weekend_p,(number_format((($allweekend_discharge/$dischargedpcount)*100), 2, '.', '')."%"));
      
    }else{
      array_push($weekend_p,"N/A");
      array_push($weekend,"N/A");
    }
    
    if ($total_count>0){

      $num=($LSP_count/$total_count)*100;
      array_push($LSP_P,(number_format($num, 2, '.', '')."%"));
    }elseif(strcmp($total_count, 'Pending') == 0){
      array_push($LSP_P,"Pending");
    }else{
      array_push($LSP_P,"0%");
    }
    
    $n++;
    
    // $date->modify("-1 month");
    $time = strtotime($date1);
    $date1 = date("Y-m-d", strtotime("-1 month", $time));
    }
    

    
$label=array_reverse($label);
$admissions=array_reverse($admissions);
$discharges=array_reverse($discharges);
$toicu=array_reverse($toicu);
$mortalitya=array_reverse($mortalitya);
$icumortalitya=array_reverse($icumortalitya);
$newconsults=array_reverse($newconsults);
$signedoff=array_reverse($signedoff);
$beddays=array_reverse($beddays);
$LOS=array_reverse($LOS);
$m_LOS=array_reverse($m_LOS);
$weekend=array_reverse($weekend);
$weekend_p=array_reverse($weekend_p);
$icuaverageLOSa=array_reverse($icuaverageLOSa);
$LSP=array_reverse($LSP);
$total_patients=array_reverse($total_patients);
$LSP_P=array_reverse($LSP_P);
$readmission=array_reverse($readmission);


    ?>
<div class="table-responsive text-nowrap">
<table class="table table-striped">
<thead>
  <tr>
    <th scope="col">KPI for <?php echo $ydate1; ?></th>
<?php
foreach ($label as $l){
  echo "<th>" . $l . "</th>" ;
}
?>
</tr>
</thead>
<tbody>
  <tr>
    <th>Admissions</th>
    <?php
foreach ($admissions as $a){
  echo "<td>" . $a . "</td>" ;
}
?>
  </tr>
  <tr>
    <th>Discharges</th>
    <?php
foreach ($discharges as $d){
  echo "<td>" . $d . "</td>" ;
}
?>
  </tr>
  <tr>
    <th>Transfer to ICU</th>
    <?php
foreach ($toicu as $t){
  echo "<td>" . $t . "</td>" ;
}
?>
  </tr>
  <tr>
    <th>Consultations</th>
    <?php
foreach ($newconsults as $n){
  echo "<td>" . $n . "</td>" ;
}
?>
  </tr>
  <tr>
    <th>Sign Offs</th>
    <?php
foreach ($signedoff as $s){
  echo "<td>" . $s . "</td>" ;
}
?>
  </tr>
  <tr>
    <th>Bed Days</th>
    <?php
foreach ($beddays as $b){
  echo "<td>" . $b . "</td>" ;
}
?>
  </tr>
  <tr>
    <th>Clinical LOS</th>
    <?php
foreach ($m_LOS as $l){
  echo "<td>" . $l . "</td>" ;
}
?>
  </tr>
  <tr>
    <th>Physical LOS</th>
    <?php
foreach ($LOS as $l){
  echo "<td>" . $l . "</td>" ;
}
?>
  </tr>
  <tr>
    <th>ICU LOS</th>
    <?php
foreach ($icuaverageLOSa as $l){
  echo "<td>" . $l . "</td>" ;
}
?>
  </tr>
  <tr>
    <th>Weekend Discharge #</th>
    <?php
foreach ($weekend as $w){
  echo "<td>" . $w . "</td>" ;
}
?>
  </tr>
  <tr>
    <th>Weekend Discharge %</th>
    <?php
foreach ($weekend_p as $w){
  echo "<td>" . $w . "</td>" ;
}
?>
  </tr>
  <tr>
    <th>Total Patients</th>
    <?php
foreach ($total_patients as $tp){
  echo "<td>" . $tp . "</td>" ;
}
?>
  </tr>
  <tr>
    <th>Long Stay</th>
    <?php
foreach ($LSP as $L){
  echo "<td>" . $L . "</td>" ;
}
?>
  </tr>
  <tr>
    <th>Long Stay %</th>
    <?php
foreach ($LSP_P as $Lp){
  echo "<td>" . $Lp . "</td>" ;
}
?>
  </tr>
  <tr>
    <th>72 hrs Readmissions</th>
    <?php
foreach ($readmission as $r){
  echo "<td>" . $r . "</td>" ;
}
?>
  </tr>
  <tr>
    <th>ICU Mortality</th>
    <?php
foreach ($icumortalitya as $icm){
  echo "<td>" . $icm . "</td>" ;
}
?>
  </tr>
  <tr>
    <th>Out ICU Mortality</th>
    <?php
foreach ($mortalitya as $m){
  echo "<td>" . $m . "</td>" ;
}
?>
  </tr>
</tbody>
</table>
</div>
<?php
    
}  elseif ($time == "quarterly"){
    $title ='Quarterly Overview';

       $n=0;
       $quarter=4;
       $allweekend_discharge=0;

       
       $date1 = date("Y-12-1", strtotime($date));
       $long_stay_date = date("Y-12-1", strtotime($date));
       $date_day1 =  date("Y-10-1", strtotime($date));
       $first_day1 =  date("Y-12-1", strtotime($date));

       while($n < 4){

        
      
        $ydate1=date("Y",strtotime($date1));
         $formationSQL = "SELECT * FROM picupatients WHERE QUARTER(ADMDATE) = '".$quarter."' AND YEAR(ADMDATE) = '".$ydate1."' AND (current_location != 'ICU' or current_location is null)";
         $result1 = $mysqli->query($formationSQL);
         $admittedpcount = mysqli_num_rows($result1);
       
         $formationSQL = "SELECT * FROM picupatients WHERE QUARTER(DISDATE) = '".$quarter."'  AND YEAR(DISDATE) = '".$ydate1."' AND (current_location != 'ICU' or current_location is null)";
         $result1 = $mysqli->query($formationSQL);
         $dischargedpcount = mysqli_num_rows($result1);
         
         $formationSQL = "SELECT * FROM consultations WHERE QUARTER(consultation_date) = '".$quarter."' AND YEAR(consultation_date) = '".$ydate1."'";
         $result1 = $mysqli->query($formationSQL);
         $newconsultscount = mysqli_num_rows($result1);
       
         $formationSQL = "SELECT * FROM consultations WHERE QUARTER(signoff_date) = '".$quarter."' AND YEAR(signoff_date) = '".$ydate1."'";
         $result1 = $mysqli->query($formationSQL);
         $signedoffcount = mysqli_num_rows($result1);
       
         ///// Trans to ICU
    $formationSQL = "SELECT DISDATE FROM picupatients WHERE QUARTER(DISDATE) = '".$quarter."' AND YEAR(DISDATE) = '".$ydate1."' AND DISTO = 'Intensive Care (ICU)'";
    $result1 = $mysqli->query($formationSQL);
    $transtoicu = mysqli_num_rows($result1);

        ///// Mortality in ICU
        $formationSQL = "SELECT DISDATE FROM picupatients WHERE QUARTER(DISDATE) = '".$quarter."' AND YEAR(DISDATE) = '".$ydate1."' AND current_location = 'ICU' AND MORTALITY = 'Dead'";
        $result1 = $mysqli->query($formationSQL);
        $icumortality = mysqli_num_rows($result1);
        
        ///// Mortality
        $formationSQL = "SELECT DISDATE FROM picupatients WHERE QUARTER(DISDATE) = '".$quarter."' AND YEAR(DISDATE) = '".$ydate1."' AND (current_location != 'ICU' or current_location is null) AND MORTALITY = 'Dead'";
        $result1 = $mysqli->query($formationSQL);
        $mortality = mysqli_num_rows($result1);
          //////////////

         $m_in_quarter = $quarter * 3;

               
      //////////////
      //Los
      /////////////
      $q_m_average=0;
      $q_average=0;
      $q_LSP_count=0;
      $q_total_count=0;
      $q_readmission_count=0;
      $m_in_quarter1=$m_in_quarter;
      for ($x = 1; $x <= 3; $x++) {


//////////////
// Medical Los
/////////////

$formationSQL = "SELECT ADMDATE, med_DISDATE FROM picupatients WHERE DISDATE IS NOT NULL AND MONTH(DISDATE) = '".$m_in_quarter1."' AND YEAR(ADMDATE) = '".$ydate1."' AND (current_location != 'ICU' or current_location is null)";
$result1 = $mysqli->query($formationSQL);
$datesss = $result1 -> fetch_all(MYSQLI_ASSOC);


$los=array();

foreach ($datesss as $d){
  $timeDiff = abs(strtotime($d['ADMDATE']) - strtotime($d['med_DISDATE']));

  array_push($los,$timeDiff/86400);
  
}
// var_dump($los);
// $a = array_filter($los);
if(count($los) > 0) {
  $m_average = array_sum($los)/count($los);
} else {
  $m_average = 0;
}
$q_m_average=$q_m_average+$m_average;

 //////////////
// physical Los
/////////////

$formationSQL = "SELECT ADMDATE, DISDATE FROM picupatients WHERE DISDATE IS NOT NULL AND MONTH(DISDATE) = '".$m_in_quarter1."' AND YEAR(ADMDATE) = '".$ydate1."' AND (current_location != 'ICU' or current_location is null)";
$result1 = $mysqli->query($formationSQL);
$datesss = $result1 -> fetch_all(MYSQLI_ASSOC);


$los=array();

foreach ($datesss as $d){
  $timeDiff = abs(strtotime($d['ADMDATE']) - strtotime($d['DISDATE']));

  array_push($los,$timeDiff/86400);
  
}
// var_dump($los);
// $a = array_filter($los);
if(count($los) > 0) {
  $average = array_sum($los)/count($los);
} else {
  $average = 0;
}
$q_average=$q_average+$average;


/////////////////////
    ///// readmissions
//////////////////////////
// echo $mdate1 ."</br>";
$formationSQL = "SELECT * FROM picupatients WHERE MONTH(ADMDATE) = '".$m_in_quarter1."' AND YEAR(ADMDATE) = '".$ydate1."'";
$result1 = $mysqli->query($formationSQL);
$admitted_patients = $result1 -> fetch_all(MYSQLI_ASSOC);

$readmission_count=0;

foreach ($admitted_patients as $s){
  $formationSQL = "SELECT * FROM picupatients WHERE DISDATE + INTERVAL 3 DAY >='".$s['ADMDATE']."' AND ID <'".$s['ID']."'  AND MRN='".$s['MRN']."' AND (trans_discharge = 'discharge from ICU' or trans_discharge='discharge from ward' or trans_discharge IS NULL) LIMIT 1";
  $result1 = $mysqli->query($formationSQL);
$recentadmission = mysqli_num_rows($result1);
// $recentadmission1 = $result1 -> fetch_array(MYSQLI_ASSOC);
// var_dump($recentadmission1);
$readmission_count=$readmission_count+$recentadmission;
}

$q_readmission_count=$q_readmission_count+$readmission_count;

$m_in_quarter1=$m_in_quarter1-1;




////////////////////////////////////
//long stays > 30 days
////////////////////////////////////


$first_day_ofmonth = $first_day1;
$last_day_ofmonth= date("Y-m-t", strtotime($first_day_ofmonth));
// echo $last_day_ofmonth ."</br>";
// echo strtotime($last_day_ofmonth) . "</br>";
// echo strtotime($today). "</br>";
if (strtotime($last_day_ofmonth) <= strtotime($today)) {
  $formationSQL = "SELECT * FROM picupatients WHERE ((DISDATE IS NOT NULL AND ADMDATE < '".$first_day_ofmonth."' AND DISDATE >= '".$last_day_ofmonth."') OR (DISDATE IS NULL AND ADMDATE + INTERVAL 30 DAY < '".$last_day_ofmonth."')) AND (current_location != 'ICU' or current_location is null)";
  $result1 = $mysqli->query($formationSQL);
  $LSP_count = mysqli_num_rows($result1);
  // echo $last_day_ofmonth."</br>";
  // var_dump($patient);
  
  // total number of patients at that month
  $formationSQL = "SELECT * FROM picupatients WHERE (
    (DISDATE IS NULL AND ADMDATE <= '".$last_day_ofmonth."') 
   OR (DISDATE IS NOT NULL AND DISDATE >= '".$first_day_ofmonth."' AND DISDATE <= '".$last_day_ofmonth."') 
   OR (DISDATE IS NOT NULL AND ADMDATE < '".$first_day_ofmonth."' AND DISDATE > '".$last_day_ofmonth."') 
   OR   (ADMDATE >= '".$first_day_ofmonth."' AND ADMDATE <= '".$last_day_ofmonth."')
   ) AND (current_location != 'ICU' or current_location is null)";
   $result1 = $mysqli->query($formationSQL);
  $total_count = mysqli_num_rows($result1);
  
  $time2 = strtotime($first_day1);
  $first_day1 = date("Y-m-d", strtotime("-1 month", $time2));

$q_LSP_count=$q_LSP_count+$LSP_count;
$q_total_count=$q_total_count+$total_count;
  }else{
    $q_LSP_count=$q_LSP_count+0;
$q_total_count=$q_total_count+0;
$time2 = strtotime($first_day1);
$first_day1 = date("Y-m-d", strtotime("-1 month", $time2));
  }



}


    ////////////////////////////////////////////////////////////////
    //average of the 3 months calcualted by sum and devided by
    ////////////////////////////////////////////////////////////////
    $q_average=$q_average/3;
    $q_m_average=$q_m_average/3;




////////////////////////////////////////////////////////////////
// Number of bed days
////////////////////////////////////////////////////////////////
// $formationSQL = "SELECT * FROM picupatients WHERE ADMDATE = '".$date1."' AND (DISDATE = '".$date1."' OR DISDATE IS NULL)";

// number of day in quarter
$d_in_quarter=0;
    for ($x = 1; $x <= 3; $x++) {

        $ds=cal_days_in_month(CAL_GREGORIAN,$m_in_quarter,$ydate1);
        $m_in_quarter=$m_in_quarter-1;
        $d_in_quarter=$d_in_quarter+$ds;
    }
// echo $d_in_quarter."</br>";
$quarter_beddayscount=0;

// echo $date_day;
$date_day= $date_day1;
    for ($x = 1; $x <= $d_in_quarter; $x++) {
        // Where not in ICU and where not discharged at the same day of admission
        // echo $date_day ."</br>";
        if (strtotime($date_day) <= strtotime($today)) {
        $formationSQL = "SELECT * FROM picupatients WHERE ADMDATE <= '".$date_day."' AND (DISDATE >= '".$date_day."' OR DISDATE IS NULL) AND NOT DISDATE <=> ADMDATE  AND (current_location != 'ICU' or current_location is null)";
        $result1 = $mysqli->query($formationSQL);
        $dayscount = mysqli_num_rows($result1);
        $quarter_beddayscount=$quarter_beddayscount+$dayscount;
        // echo $date_day . "</br>";
      }
      // echo $date_day . "</br>";
      
      //weekend discharges
      if (date('w', strtotime($date_day)) == 6 || date('w', strtotime($date_day)) == 5){
        // echo $date_day . "</br>";
    $formationSQL = "SELECT * FROM picupatients WHERE DISDATE = '".$date_day."' AND (current_location != 'ICU' or current_location is null)";
    $result1 = $mysqli->query($formationSQL);
    $weekend_discharge = mysqli_num_rows($result1);
    $allweekend_discharge=$allweekend_discharge+$weekend_discharge;
      }
   

        $date_day= date('Y-m-d', strtotime($date_day . ' +1 day'));

    }
     


      array_push($admissions,$admittedpcount);
      array_push($discharges,$dischargedpcount);
      array_push($toicu,$transtoicu);
      array_push($newconsults,$newconsultscount);
      array_push($signedoff,$signedoffcount);
      array_push($beddays,$quarter_beddayscount);
      array_push($LSP,$q_LSP_count);
      array_push($total_patients,$q_total_count);
      array_push($readmission,$q_readmission_count);
      array_push($icumortalitya,$icumortality);
      array_push($mortalitya,$mortality);

      if($q_average>0){
        array_push($LOS,(number_format(($q_average), 2, '.', '')." Days"));
        } else {
          array_push($LOS,0);
                }

      if($q_m_average>0){
        array_push($m_LOS,(number_format(($q_m_average), 2, '.', '')." Days"));
        } else {
          array_push($m_LOS,0);
                }

      if($dischargedpcount>0){
        
        array_push($weekend,(number_format($allweekend_discharge, 0, '.', '').""));
      array_push($weekend_p,(number_format((($allweekend_discharge/$dischargedpcount)*100), 2, '.', '')."%"));
      
    }else{
      array_push($weekend_p,"N/A");
      array_push($weekend,"N/A");
    }
      
    if ($q_total_count>0){

      $num=($q_LSP_count/$q_total_count)*100;
      array_push($LSP_P,(number_format($num, 2, '.', '')."%"));
    }elseif(strcmp($q_total_count, 'Pending') == 0){
      array_push($LSP_P,"Pending");
    }else{
      array_push($LSP_P,"0%");
    }


    $n++;
    $quarter=$quarter-1;
    
    $time = strtotime($date_day1);
    $date_day1 = date("Y-m-d", strtotime("-3 month", $time));
   
    
    }
    
    $label=['Forth Quarter','Third Quarter', 'Second Quarter', 'First Quarter']  ;
    
$label=array_reverse($label);
$admissions=array_reverse($admissions);
$discharges=array_reverse($discharges);
$toicu=array_reverse($toicu);
$newconsults=array_reverse($newconsults);
$signedoff=array_reverse($signedoff);
$beddays=array_reverse($beddays);
$LOS=array_reverse($LOS);
$m_LOS=array_reverse($m_LOS);
$weekend=array_reverse($weekend);
$weekend_p=array_reverse($weekend_p);
$LSP=array_reverse($LSP);
$total_patients=array_reverse($total_patients);
$LSP_P=array_reverse($LSP_P);
$readmission=array_reverse($readmission);
$mortalitya=array_reverse($mortalitya);
$icumortalitya=array_reverse($icumortalitya);

    ?>
<div class="table-responsive text-nowrap">
<table class="table table-striped">
<thead>
  <tr>
    <th scope="col">KPI for <?php echo $ydate1; ?></th>
<?php
foreach ($label as $l){
  echo "<th>" . $l . "</th>" ;
}
?>
</tr>
</thead>
<tbody>
  <tr>
    <th>Admissions</th>
    <?php
foreach ($admissions as $a){
  echo "<td>" . $a . "</td>" ;
}
?>
  </tr>
  <tr>
    <th>Discharges</th>
    <?php
foreach ($discharges as $d){
  echo "<td>" . $d . "</td>" ;
}
?>
  </tr>
  <tr>
    <th>Transfer to ICU</th>
    <?php
foreach ($toicu as $t){
  echo "<td>" . $t . "</td>" ;
}
?>
  </tr>
  <tr>
    <th>Consultations</th>
    <?php
foreach ($newconsults as $n){
  echo "<td>" . $n . "</td>" ;
}
?>
  </tr>
  <tr>
    <th>Sign Offs</th>
    <?php
foreach ($signedoff as $s){
  echo "<td>" . $s . "</td>" ;
}
?>
  </tr>
  <tr>
    <th>Bed Days</th>
    <?php
foreach ($beddays as $b){
  echo "<td>" . $b . "</td>" ;
}
?>
  </tr>
  <tr>
    <th>Clinical LOS</th>
    <?php
foreach ($m_LOS as $l){
  echo "<td>" . $l . "</td>" ;
}
?>
  </tr>
  <tr>
    <th>Physical LOS</th>
    <?php
foreach ($LOS as $l){
  echo "<td>" . $l . "</td>" ;
}
?>
  </tr>
  <tr>
    <th>Weekend Discharge #</th>
    <?php
foreach ($weekend as $w){
  echo "<td>" . $w . "</td>" ;
}
?>
  </tr>
  <tr>
    <th>Weekend Discharge %</th>
    <?php
foreach ($weekend_p as $w){
  echo "<td>" . $w . "</td>" ;
}
?>
  </tr>
  <tr>
    <th>Total Patients</th>
    <?php
foreach ($total_patients as $tp){
  echo "<td>" . $tp . "</td>" ;
}
?>
  </tr>
  <tr>
    <th>Long Stay</th>
    <?php
foreach ($LSP as $L){
  echo "<td>" . $L . "</td>" ;
}
?>
  </tr>
  <tr>
    <th>Long Stay %</th>
    <?php
foreach ($LSP_P as $Lp){
  echo "<td>" . $Lp . "</td>" ;
}
?>
  </tr>
  <tr>
    <th>72 hrs Readmissions</th>
    <?php
foreach ($readmission as $r){
  echo "<td>" . $r . "</td>" ;
}
?>
  </tr>
  <tr>
    <th>ICU Mortality</th>
    <?php
foreach ($icumortalitya as $icm){
  echo "<td>" . $icm . "</td>" ;
}
?>
  </tr>
  <tr>
    <th>Out ICU Mortality</th>
    <?php
foreach ($mortalitya as $m){
  echo "<td>" . $m . "</td>" ;
}
?>
  </tr>
</tbody>
</table>
</div>
<?php
    
} 
?>


