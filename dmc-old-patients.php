<?php 
session_start();

require_once "authCookieSessionValidate.php";

if(!$isLoggedIn) {
    header("Location: ./");
}
date_default_timezone_set('Asia/Riyadh');
$today=date("Y-m-d");

?>

  <!-- Navbar -->
<?php
require 'sidebar.php';

if (isset($_POST['confirm_pt_btn'])) {

    $formationSQL = "SELECT * FROM picupatients_temp";
		$result1 = $mysqli->query($formationSQL);
		$temppicupatints = $result1 -> fetch_all(MYSQLI_ASSOC);

        foreach($temppicupatints as $t) {
            $tempid=$t['ID'];
    $sql ="INSERT INTO picupatients (MRN, PNAME, ADMDATE, DISDATE, ADMFROM, DISTO, MORTALITY, admissiondiagnosis
    , nationality, gender, age, consultant_id, trans_discharge, current_location, med_DISDATE, delay, longterm)
    SELECT MRN, PNAME, ADMDATE, DISDATE, ADMFROM, DISTO, MORTALITY, admissiondiagnosis
    , nationality, gender, age, consultant_id, trans_discharge, current_location, med_DISDATE, delay, longterm FROM picupatients_temp WHERE ID='".$tempid."'";
    
if ($mysqli->query($sql) === TRUE) {
    $message= "Record transfered successfully";
    // $last_id = mysqli_insert_id($mysqli);
  } else {
   $message= "Error transfering record: " . $mysqli->error;
  }

 $sql = "DELETE FROM picupatients_temp WHERE ID='".$tempid."'";

  
                if ($mysqli->query($sql) === TRUE) {
                  $message= "Record delete successfully";
                
                    echo "<script language='javascript'>\n";
                    echo "window.location.href = 'dmc-old-patients.php';";
                    echo "</script>\n";
                    
                 
                } else {
                 $message= "Error deleting record: " . $mysqli->error;
                }


    
}

}

if (isset($_POST['add_pt_btn'])) {
    // receive all input values from the form
    $mrn = $_POST['mrn'];
    $pname = $_POST['pname'];
    $admdate = $_POST['admdate'];
    $disdate = $_POST['disdate'];
    $admfrom = $_POST['admfrom'];
    $disto = $_POST['disto'];
    $mortality = $_POST['mortality'];
    $admissiondiagnosis = json_encode($_POST['admissiondiagnosis']);
    $nationality = $_POST['nationality'];
    $gender = $_POST['gender'];
    $consultant_id = $_POST['primary'];
    $age = $_POST['age'];
    $trans_discharge = $_POST['trans_discharge'];
    $current_location = $_POST['current_location'];
    $med_disdate = $_POST['med_disdate'];
    $delay = $_POST['delay'];
    $longterm = $_POST['longterm'];

  $sql = "INSERT INTO picupatients_temp SET  MRN='".$mrn."', PNAME='".$pname."' ,ADMDATE=" . ($admdate==NULL ? "NULL" : "'".$admdate."'") . "
  ,DISDATE=" . ($disdate==NULL ? "NULL" : "'".$disdate."'") . ", ADMFROM='".$admfrom."',DISTO='".$disto."' 
  ,MORTALITY='".$mortality."', admissiondiagnosis='".$admissiondiagnosis."'
  ,nationality='".$nationality."', gender='".$gender."', age='".$age."',consultant_id='".$consultant_id."'
  , trans_discharge='".$trans_discharge."'  , current_location='".$current_location."'
 ,med_DISDATE='".$med_disdate."',delay='".$delay."',longterm='".$longterm."'"
  ;

  

//  $sql = "INSERT INTO picupatients (BED) VALUES ('$bed')";


if ($mysqli->query($sql) === TRUE) {
    $message= "Record added successfully";
  
      echo "<script language='javascript'>\n";
      echo "window.location.href = 'dmc-old-patients.php';";
      echo "</script>\n";
      
  
  } else {
   $message= "Error adding record: " . $mysqli->error;
  }

  // echo  $message;
echo "<a>".$message."</a>";


}

  if (!in_array($user['position'],$access_PICU_control)){
    
    echo "
    <div class='content-wrapper'>
    
  
    <section class='content'>
    <div class='container-fluid'>  
    <div class='alert alert-danger' role='alert'> you dont have permission to access this page, Contact you manager if you need to.
    </div>
    </div>
    </section>
    </div>
    ";
    require 'footer.php';

    exit();
  }
?>

     <style>
          .hidden{
                  display: none;
          }
          textarea {
    resize: none;
    overflow: hidden;
}

      </style>

        <script>


function auto_grow(element) {
    element.style.height = "5px";
    element.style.height = (element.scrollHeight)+"px";
}

	</script>
      
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
  
	<?php
   		
		$formationSQL = "SELECT * FROM picupatients_temp";
		$result1 = $mysqli->query($formationSQL);
		$temppicupatints = $result1 -> fetch_all(MYSQLI_ASSOC);

    $formationSQL = "SELECT * FROM countries";
		$result1 = $mysqli->query($formationSQL);
		$countries = $result1 -> fetch_all(MYSQLI_ASSOC);
$userid=$user['member_id'];


  // TB list

  $formationSQL = "SELECT dx_id FROM tb_list";
  $result1 = $mysqli->query($formationSQL);
  $tb_list1 = $result1 -> fetch_all(MYSQLI_ASSOC);
  $tb_list=array();
  foreach($tb_list1 as $tb){
    $tb_list[]=$tb['dx_id'];
  }

  //settings
  $query = "select * from settings";
  $result1 = $mysqli->query($query);
  $settings = $result1 -> fetch_array(MYSQLI_ASSOC);

  $shortlos=$settings['short_los'];
  $longlos=$settings['long_los'];
	?>


    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Add old patients</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="dashboard.php">Home</a></li>
              <li class="breadcrumb-item active">Add old patients</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">  
           

<div class="row">

 <div id="mypresentersTable" class="col-md-12">

            <!-- /.info-box -->



            <div class="card">
              <div  class="card-header">
              <h3 class="card-title">Old Patient List</h3>
                     
  
                          <div id="addbtn" class='eachrow' style='  float: right; text-align: center;display: flex;'>
                  <?php
                  if ($user['add_new_patient']== '1'){

                echo "<a  class='btn btn-success'  href='#admiting_modal' data-toggle='modal'  style='color: aliceblue; line-height: 2;padding: 0px 15px;margin-bottom: 2%;'>New Admission</a>
               ";

                    echo "
                    <form method='post' action='dmc-old-patients.php'>
                    <button type='submit' value='submit' name='confirm_pt_btn' class='btn btn-danger' style='color: aliceblue; line-height: 2;padding: 0px 15px;margin-bottom: 2%;'>Confirm Patients</button>
                    </form>";
                      }
                  ?>
                  
        
          </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <div class="row">
         
                       
                                         <?php

                                        
                                                     foreach($temppicupatints as $s){



                         


                                                        $formationSQL = "SELECT * FROM picupatients WHERE DISDATE + INTERVAL 3 DAY > NOW() AND trans_discharge IS NULL AND MRN='".$s['MRN']."' LIMIT 1";
                                                        $result1 = $mysqli->query($formationSQL);
                                                        $recentadmission = $result1 -> fetch_all(MYSQLI_ASSOC);
                                                        // var_dump($recentadmission);




                                                      $decodedadmissiondx=json_decode($s['admissiondiagnosis']);
                                                      if ($decodedadmissiondx){
                                                      $tb_patient=array_intersect($decodedadmissiondx,$tb_list);
                                                    }else{
                                                      $tb_patient=array();
                                                    }

                                                  
                                                      $today = date("Y-m-d");
                                                      $today1 = strtotime(date("Y-m-d"));
                                                      // $datediff = $today - strtotime($s['ADMDATE']);
                                                      // $LOS = $today->date_diff($s['ADMDATE'])->format("%a");

                                                      if ($s['med_DISDATE']){
                                                        $timeDiff = abs(strtotime($s['med_DISDATE']) - strtotime($s['ADMDATE']));

                                                        $LOS = $timeDiff/86400;  // 86400 seconds in one day
                                                      }else{
                                                          
                                                      $timeDiff = abs($today1 - strtotime($s['ADMDATE']));

                                                      $LOS = $timeDiff/86400;  // 86400 seconds in one day

                                                    }
                                                      // $LOS= round($datediff / (60 * 60 * 24));
                                                      

                                                      
                                                    echo"  
                                                    <div class='col-sm-4'>
                                                    <div class='eachrow card'  id='row".$s['ID']."'>
                                                    
                                                    <div style='   margin: 1%; text-align: center;display: inline; ' class='eachcol bed card-header'  scope='row' >";
                                                   
                                                    // var_dump($decodedadmissiondx);  
                                                    // var_dump($tb_list);
                                                    
                                                    // var_dump($result);
                                                        if ($s['newassign']=='1' AND $s['assigned_on']==$today){
                                                          echo"<p style='color: red;display: contents;'><strong>New</strong></p>";
                                                          
                                                           }
                                                          
                                                          if ($tb_patient ){
                                                            echo"<div style='   background: #01ff70'><strong> TB Patient</strong></div>";
                                                          }
                                                          if ($s['delay'] !==Null){
                                                        echo"<div style='   background: gold'><strong> Delayed Discharge</strong></div>";
                                                      }
                                                      if (!empty($s['longterm'])){
                                                        echo"<div style='    background: #87503e;color: white;'><strong> Long Term Patient</strong></div>";
                                                      }
                                                      // 1> if discharge still in

                                              if($s['med_DISDATE'] && $s['DISDATE'] == NULL && $s['trans_discharge'] == NULL) {
                                                echo"
                                                <label style='text-align: center;margin-bottom: 0px;'>Discharge Still in</label>";
                                                      
                                                      ///2> if discharge is complete
                                              } elseif ($s['med_DISDATE'] && $s['DISDATE'] && $s['trans_discharge'] == 'discharge from ward'){
                                                echo"
                                                <label style='text-align: center;margin-bottom: 0px;'>Discharged</label>";
                                            
                                              // 4> transfer to other service within internal medicine department 
                                        }  elseif ($s['med_DISDATE'] && $s['DISDATE'] && $s['trans_discharge'] == 'transfer to other speciality'){
                                            echo"
                                            <label style='text-align: center;margin-bottom: 0px;'>Transferred Intradepartment</label>";
                                         // 5> transfer to other service outside internal medicine department 
                                    }  elseif ($s['med_DISDATE'] && $s['DISDATE'] && $s['trans_discharge'] == 'other transfer'){
                                        if ($s['DISTO'] == 'Intensive Care (ICU)'){
                                            echo"
                                            <label style='text-align: center;margin-bottom: 0px;'>Transferred to ICU</label>";
                                        }else{
                                        echo"
                                        <label style='text-align: center;margin-bottom: 0px;'>Transferred out department</label>";
                                        }
                                    
                                    // 6> Discharged from ICU
                                } elseif ($s['med_DISDATE'] && $s['DISDATE'] && $s['trans_discharge'] == 'discharge from ICU'){
                                    echo" <label style='text-align: center;margin-bottom: 0px;'>Discharged from ICU</label>";
                                    
                                // 7> Discharged from ICU
                            } elseif ($s['med_DISDATE'] && $s['DISDATE'] && $s['trans_discharge'] == 'Transfer from ICU'){
                                echo" <label style='text-align: center;margin-bottom: 0px;'>Transferred back from ICU</label>";
                                
                            }else{

                                                      echo"
                                                <label style='text-align: center;margin-bottom: 0px;'>Admitted In ".$s['current_location']." Bed #</label>

                                                      <input disabled class='txtdata' name='bed' placeholder='Bed Number' value='".$s['BED']."' style='text-align: center;' >
                                                      <input disabled class='txtdata' type='hidden' name='id' id='id' value='".$s['ID']."' style='text-align: center;width: 85%;' >";
                                                      }

                                                      echo"
                                                        </div>
                                                      
                                                      <div style=' margin: 1%; ' class='eachcol mrn' >
                                                      <table style='width: 100%;'>
                                                      <tr>
                                                      <td style='width: 50%; border-right: solid 0.5px;'>
                                                      <label style='text-align: center;margin-bottom: 0px;'>MRN</label>
                                                      <p style='text-align: center;'>".$s['MRN']."</p>
                                                      </td>
                                                      <td style='width: 50%;'>
                                                      <label style='text-align: center;margin-bottom: 0px;'>Age</label>
                                                      <p style='text-align: center;'>".$s['age']."</p>
                                                    </td>
                                                      </tr>
                                                      </table>
                                                      </div>

                                                      <div style=' margin: 1%; ' class='eachcol name'>
                                                      <table style='width: 100%;'>
                                                      <tr>
                                                      <td style='width: 50%; border-right: solid 0.5px;'>
                                                      <label style='text-align: center;margin-bottom: 0px;'>Patient Name</label>
                                                      <p style='text-align: center;'>".$s['PNAME']."</p>
                                                      </td>
                                                      <td style='width: 50%;'>
                                                      <label style='text-align: center;margin-bottom: 0px;'>Gender</label>
                                                      <p style='text-align: center;'>".$s['gender']."</p>
                                                    </td>
                                                      </tr>
                                                      </table>
                                                      </div>
                                                
                                                
                                                 <div style=' margin: 1%; ' class='eachcol admfrom'  scope='row' >
                                                      <table style='width: 100%;'>
                                                      <tr>
                                                      <td style='width: 50%; border-right: solid 0.5px;'>
                                                      <label style='text-align: center;margin-bottom: 0px;'>Admission From</label>
                                                      <p style='text-align: center;margin-bottom: 0px;'>".$s['ADMFROM']."</p>
                                                      </td>
                                                      <td style='width: 50%;'>
                                                      <label style='text-align: center;margin-bottom: 0px;'>Admitted By</label>";
                                                      $mem_id= $s['admitted_by'];
                                                      $formationSQL = "SELECT * FROM members WHERE member_id='".$mem_id."'";
                                                       $result1 = $mysqli->query($formationSQL);
                                                       $doctor = $result1 -> fetch_array(MYSQLI_ASSOC);

                                                      echo"
                                                      <p style='text-align: center;margin-bottom: 0px;'>".$doctor['full_name']."</p>
                                                      </td>
                                                      </tr>
                                                      </table>
                                                      </div>

                                                      <div style=' margin: 1%; ' class='eachcol admdate'  scope='row' >
                                                      <label style='text-align: center;margin-bottom: 0px;'>Admission Date</label>
                                                      <p style='text-align: center;'>".$s['ADMDATE']."</p>
                                                      </div>
                                                      <div style=' margin: 1%; ' class='eachcol admdate'  scope='row' >
                                                      <label style='text-align: center;margin-bottom: 0px;'>Nationality</label>";

                                                      $nation = $s['nationality'];
                                                      
                                                      echo"
                                                      <p style='text-align: center;'>".$nation."</p>
                                                      </div>";

                                                      if($s['current_location'] == 'ICU'){
                                                        echo"   <div style='  margin: 1%; background: royalblue;color: white;' class='eachcol'    scope='row' >
                                                        <p style='text-align: center;margin-bottom: 0px;'>ICU patient</p>
                                                       
                                                                </div>
                                                                ";

                                                      } else{

                                                      
                                                                if ($LOS < $shortlos){
                                                                  echo"   <div style=' margin: 1%; background: #d4edda;' class='eachcol admdate'    scope='row' >";
                                                                } elseif   ($LOS > $longlos){
                                                                  echo"   <div style=' margin: 1%; background: #f8d7da;' class='eachcol admdate'   scope='row' >";
                                                                }elseif   ($LOS >= $shortlos){
                                                                  echo"  <div style=' margin: 1%; background: #fff3cd;' class='eachcol admdate'    scope='row' >";
                                                                }

                                                                echo"
                                                                <label style='text-align: center;margin-bottom: 0px;'>Duration of Admission: ".$LOS." Days</label>
                                                            
                                                                </div>
                                                                ";
                                                              }
                                                              echo"
                                                 <div style=' margin: 1%; ' class='eachcol admissiondiagnosis'>
                                                 <label style='text-align: center;margin-bottom: 0px;'>Diagnosis</label>
                                                 <ul style='list-style-position: inside;margin: 1% 0% 1%;'>
                                                 ";
                                           
                                                 if (is_array($decodedadmissiondx)){
                                                   
                                                   foreach($decodedadmissiondx as $key => $value)
                                             {
                                               $formationSQL = "SELECT * FROM icd10 WHERE id='".$value."'";
                                               $result1 = $mysqli->query($formationSQL);
                                               $dxlist = $result1 -> fetch_array(MYSQLI_ASSOC);
                                                
                                           
                                                 echo '<li>'.  $dxlist['name']. '</li>';
                                             }}
                                           
                                                 echo"
                                              </ul></div>
                                              <div style=' margin: 1%; '>
                                             
                                              <label style='text-align: center;margin-bottom: 0px;'>Primary Consultant</label>";
                                              $con_id= $s['consultant_id'];
                                              $formationSQL = "SELECT * FROM members WHERE member_id='".$con_id."'";
                                               $result1 = $mysqli->query($formationSQL);
                                               $doctor1 = $result1 -> fetch_array(MYSQLI_ASSOC);
                                                if ($doctor1){
                                              echo"
                                              <p style='text-align: center;margin-bottom: 0px;'>".$doctor1['full_name']."</p>";
                                            }else{
                                                echo"  <p style='text-align: center;margin-bottom: 0px;'>Not Assigned Yet</p>";
                                            }
                                              
                                            echo" </div>

                                            ";

                                              // discharge and transfer part
                                              // 1> if discharge still in
                                              if($s['med_DISDATE'] && $s['DISDATE'] == NULL && $s['trans_discharge'] == NULL) {
                                                echo"
                                                <div style=' margin: 1%; border-top: solid 0.5px;'>
                                             
                                                <table style='width: 100%;'>
                                                <tr>
                                                <td style='width: 50%; border-right: solid 0.5px;'>
                                                <label style='text-align: center;margin-bottom: 0px;'>Discharge Status</label>
                                                <p style='text-align: center;margin-bottom: 0px;'>".$s['MORTALITY']."</p>
                                                </td>
                                                <td style='width: 50%;'>
                                                <label style='text-align: center;margin-bottom: 0px;'>Discharged By</label>";
                                                $mem_id2= $s['trans_discharge_by'];
                                                $formationSQL = "SELECT * FROM members WHERE member_id='".$mem_id2."'";
                                                 $result1 = $mysqli->query($formationSQL);
                                                 $doctor2 = $result1 -> fetch_array(MYSQLI_ASSOC);
  
                                                echo"
                                                <p style='text-align: center;margin-bottom: 0px;'>".$doctor2['full_name']."</p>
                                                </td>
                                                </tr>
                                                </table>
                                                </div>
                                                <div style=' margin: 1%;'>
                                                <table style='width: 100%;'>
                                                <tr>
                                                <td style='width: 50%; border-right: solid 0.5px;'>
                                                <label style='text-align: center;margin-bottom: 0px;'>Discharge Date</label>
                                                <p style='text-align: center;margin-bottom: 0px;'>".$s['med_DISDATE']."</p>
                                                </td>
                                                <td style='width: 50%;'>
                                                <label style='text-align: center;margin-bottom: 0px; color:red;'>Delay Due To</label>
                                                
                                                <p style='text-align: center;margin-bottom: 0px;'>".$s['delay']."</p>
                                                </td>
                                                </tr>
                                                </table>
                                                <label style='text-align: center;margin-bottom: 0px; color:red;'>File Not Closed Yet</label>
                                                </div>";
                                                ///2> if discharge is complete
                                              } elseif ($s['med_DISDATE'] && $s['DISDATE'] && $s['trans_discharge'] == 'discharge from ward'){
                                                echo"
                                                <div style=' margin: 1%; border-top: solid 0.5px;'>
                                             
                                                <table style='width: 100%;'>
                                                <tr>
                                                <td style='width: 50%; border-right: solid 0.5px;'>
                                                <label style='text-align: center;margin-bottom: 0px;'>Discharge Status</label>
                                                <p style='text-align: center;margin-bottom: 0px;'>".$s['MORTALITY']."</p>
                                                </td>
                                                <td style='width: 50%;'>
                                                <label style='text-align: center;margin-bottom: 0px;'>Discharged By</label>";
                                                $mem_id2= $s['trans_discharge_by'];
                                                $formationSQL = "SELECT * FROM members WHERE member_id='".$mem_id2."'";
                                                 $result1 = $mysqli->query($formationSQL);
                                                 $doctor2 = $result1 -> fetch_array(MYSQLI_ASSOC);
  
                                                echo"
                                                <p style='text-align: center;margin-bottom: 0px;'>".$doctor2['full_name']."</p>
                                                </td>
                                                </tr>
                                                </table>
                                                </div>
                                              <div style=' margin: 1%;'>
                                             
                                              <table style='width: 100%;'>
                                              <tr>
                                              <td style='width: 50%; border-right: solid 0.5px;'>
                                              <label style='text-align: center;margin-bottom: 0px;'>Discharge Date</label>
                                              <p style='text-align: center;margin-bottom: 0px;'>".$s['med_DISDATE']."</p>
                                              </td>
                                              <td style='width: 50%;'>
                                              <label style='text-align: center;margin-bottom: 0px;'>Discharged To</label>
                                              <p style='text-align: center;margin-bottom: 0px;'>".$s['DISTO']."</p>
                                              </td>
                                              </tr>
                                              </table>
                                              </div>";

                                                    // 3> if discharge is complete and there is delay
                                              if ($s['delay']){
                                                  echo"
                                              <div style=' margin: 1%;'>
                                             
                                              <table style='width: 100%;'>
                                              <tr>
                                              <td style='width: 50%; border-right: solid 0.5px;'>
                                              <label style='text-align: center;margin-bottom: 0px; color:red;'>File Closed At</label>
                                              <p style='text-align: center;margin-bottom: 0px;'>".$s['DISDATE']."</p>
                                              </td>
                                              <td style='width: 50%;'>
                                              <label style='text-align: center;margin-bottom: 0px; color:red;'>Delay Due To</label>
                                              
                                              <p style='text-align: center;margin-bottom: 0px;'>".$s['delay']."</p>
                                              </td>
                                              </tr>
                                              </table>
                                              </div>";
                                            }
                                            // 4> transfer to other service within internal medicine department 
                                        }  elseif ($s['med_DISDATE'] && $s['DISDATE'] && $s['trans_discharge'] == 'transfer to other speciality'){
                                            echo"
                                            <div style=' margin: 1%; border-top: solid 0.5px;'>
                                            <label style='text-align: center;margin-bottom: 0px;'>Transfer to Other Specilaity</label>
                                            <table style='width: 100%;'>
                                            <tr>
                                            <td style='width: 50%; border-right: solid 0.5px;'>
                                            <label style='text-align: center;margin-bottom: 0px;'>Transfer Status</label>
                                            <p style='text-align: center;margin-bottom: 0px;'>".$s['MORTALITY']."</p>
                                            </td>
                                            <td style='width: 50%;'>
                                            <label style='text-align: center;margin-bottom: 0px;'>Transfer By</label>";
                                            $mem_id2= $s['trans_discharge_by'];
                                            $formationSQL = "SELECT * FROM members WHERE member_id='".$mem_id2."'";
                                             $result1 = $mysqli->query($formationSQL);
                                             $doctor2 = $result1 -> fetch_array(MYSQLI_ASSOC);

                                            echo"
                                            <p style='text-align: center;margin-bottom: 0px;'>".$doctor2['full_name']."</p>
                                            </td>
                                            </tr>
                                            </table>
                                            </div>
                                          <div style=' margin: 1%;'>
                                         
                                          <table style='width: 100%;'>
                                          <tr>
                                          <td style='width: 50%; border-right: solid 0.5px;'>
                                          <label style='text-align: center;margin-bottom: 0px;'>Tramsfer At</label>
                                          <p style='text-align: center;margin-bottom: 0px;'>".$s['med_DISDATE']."</p>
                                          </td>
                                          <td style='width: 50%;'>
                                          <label style='text-align: center;margin-bottom: 0px;'>Transfer To</label>
                                          <p style='text-align: center;margin-bottom: 0px;'>".$s['DISTO']."</p>
                                          </td>
                                          </tr>
                                          </table>
                                          </div>";
                                          // 5> transfer to other service outside internal medicine department 
                                        } elseif ($s['med_DISDATE'] && $s['DISDATE'] && $s['trans_discharge'] == 'other transfer'){
                                            echo"
                                            <div style=' margin: 1%; border-top: solid 0.5px;'>";
                                            if ($s['DISTO'] == 'Intensive Care (ICU)'){
                                                echo"
                                                <label style='text-align: center;margin-bottom: 0px;'>Transferred to ICU</label>";
                                            }else{
                                            echo"
                                            <label style='text-align: center;margin-bottom: 0px;'>Transfer to Other Specilaity</label>";
                                            }
                                            echo"
                                            
                                            <table style='width: 100%;'>
                                            <tr>
                                            <td style='width: 50%; border-right: solid 0.5px;'>
                                            <label style='text-align: center;margin-bottom: 0px;'>Transfer Status</label>
                                            <p style='text-align: center;margin-bottom: 0px;'>".$s['MORTALITY']."</p>
                                            </td>
                                            <td style='width: 50%;'>
                                            <label style='text-align: center;margin-bottom: 0px;'>Transfer By</label>";
                                            $mem_id2= $s['trans_discharge_by'];
                                            $formationSQL = "SELECT * FROM members WHERE member_id='".$mem_id2."'";
                                             $result1 = $mysqli->query($formationSQL);
                                             $doctor2 = $result1 -> fetch_array(MYSQLI_ASSOC);

                                            echo"
                                            <p style='text-align: center;margin-bottom: 0px;'>".$doctor2['full_name']."</p>
                                            </td>
                                            </tr>
                                            </table>
                                            </div>
                                          <div style=' margin: 1%;'>
                                         
                                          <table style='width: 100%;'>
                                          <tr>
                                          <td style='width: 50%; border-right: solid 0.5px;'>
                                          <label style='text-align: center;margin-bottom: 0px;'>Tramsfer At</label>
                                          <p style='text-align: center;margin-bottom: 0px;'>".$s['med_DISDATE']."</p>
                                          </td>
                                          <td style='width: 50%;'>
                                          <label style='text-align: center;margin-bottom: 0px;'>Transfer To</label>
                                          <p style='text-align: center;margin-bottom: 0px;'>".$s['DISTO']."</p>
                                          </td>
                                          </tr>
                                          </table>
                                          </div>";
                                        // 6> Discharged from ICU
                                        } elseif ($s['med_DISDATE'] && $s['DISDATE'] && $s['trans_discharge'] == 'discharge from ICU'){
                                            echo"
                                            <div style=' margin: 1%; border-top: solid 0.5px;'>
                                            
                                            <label style='text-align: center;margin-bottom: 0px;'>Discharged from ICU</label>
                                            
                                            
                                            <table style='width: 100%;'>
                                            <tr>
                                            <td style='width: 50%; border-right: solid 0.5px;'>
                                            <label style='text-align: center;margin-bottom: 0px;'>Discharge Status</label>
                                            <p style='text-align: center;margin-bottom: 0px;'>".$s['MORTALITY']."</p>
                                            </td>
                                            <td style='width: 50%;'>
                                            <label style='text-align: center;margin-bottom: 0px;'>Discharged By</label>";
                                            $mem_id2= $s['trans_discharge_by'];
                                            $formationSQL = "SELECT * FROM members WHERE member_id='".$mem_id2."'";
                                             $result1 = $mysqli->query($formationSQL);
                                             $doctor2 = $result1 -> fetch_array(MYSQLI_ASSOC);

                                            echo"
                                            <p style='text-align: center;margin-bottom: 0px;'>".$doctor2['full_name']."</p>
                                            </td>
                                            </tr>
                                            </table>
                                            </div>
                                          <div style=' margin: 1%;'>
                                         
                                          <table style='width: 100%;'>
                                          <tr>
                                          <td style='width: 50%; border-right: solid 0.5px;'>
                                          <label style='text-align: center;margin-bottom: 0px;'>Discharged At</label>
                                          <p style='text-align: center;margin-bottom: 0px;'>".$s['med_DISDATE']."</p>
                                          </td>
                                          <td style='width: 50%;'>
                                          <label style='text-align: center;margin-bottom: 0px;'>Discharged To</label>
                                          <p style='text-align: center;margin-bottom: 0px;'>".$s['DISTO']."</p>
                                          </td>
                                          </tr>
                                          </table>
                                          </div>";
                                        // 7> Discharged from ICU
                            } elseif ($s['med_DISDATE'] && $s['DISDATE'] && $s['trans_discharge'] == 'Transfer from ICU'){
                                echo"
                                <div style=' margin: 1%; border-top: solid 0.5px;'>
                                
                                <label style='text-align: center;margin-bottom: 0px;'>Transferred Back from ICU</label>
                                
                                
                                <table style='width: 100%;'>
                                <tr>
                                <td style='width: 50%; border-right: solid 0.5px;'>
                                <label style='text-align: center;margin-bottom: 0px;'>Transfer Status</label>
                                <p style='text-align: center;margin-bottom: 0px;'>".$s['MORTALITY']."</p>
                                </td>
                                <td style='width: 50%;'>
                                <label style='text-align: center;margin-bottom: 0px;'>Transfer By</label>";
                                $mem_id2= $s['trans_discharge_by'];
                                $formationSQL = "SELECT * FROM members WHERE member_id='".$mem_id2."'";
                                 $result1 = $mysqli->query($formationSQL);
                                 $doctor2 = $result1 -> fetch_array(MYSQLI_ASSOC);

                                echo"
                                <p style='text-align: center;margin-bottom: 0px;'>".$doctor2['full_name']."</p>
                                </td>
                                </tr>
                                </table>
                                </div>
                              <div style=' margin: 1%;'>
                             
                              <table style='width: 100%;'>
                              <tr>
                              <td style='width: 50%; border-right: solid 0.5px;'>
                              <label style='text-align: center;margin-bottom: 0px;'>Transfer At</label>
                              <p style='text-align: center;margin-bottom: 0px;'>".$s['med_DISDATE']."</p>
                              </td>
                              <td style='width: 50%;'>
                              <label style='text-align: center;margin-bottom: 0px;'>Transfer To</label>
                              <p style='text-align: center;margin-bottom: 0px;'>".$s['DISTO']."</p>
                              </td>
                              </tr>
                              </table>
                              ";
                                echo"
                              </div>";
                            
                }
                // if ($user['modify_patient'] == '1'){
                //   echo "<a class='btn btn-info' href='#details_modal' data-book-id='".$s['ID']."' data-toggle='modal'  style='color: aliceblue;line-height: 2;margin-top: 3%;padding: 0px 10%;width: 100%;'>Modify</a>";
                //   };
                                            echo"
                                                   </div >
                                                   </div >
                                                      ";
                                                 
                                                     }


                                                   
                                         ?> 
             

        
                    
    
                  
                  <!-- /.col -->
                </div>
                <!-- /.row -->
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
 
</div>
			
      
 </div> <!--row -->
			

</div><!--/. container-fluid -->

            

 </section>
    <!-- /.content -->


    <!-- Modal patient details-->

<div class="modal" id="details_modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
     
          <h4 class="modal-title">Patient Details</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
    <!-- data retrived from JS -->
      <div class="modal-body">
      <div id="pdetailsdiv"></div>
      </div>
     
    
  </div>
</div>
</div>    

<!-- PAGE SCRIPTS -->

<!-- AdminLTE App -->
<script src="dist/js/adminlte.js"></script>

<!-- OPTIONAL SCRIPTS -->
<script src="dist/js/demo.js"></script>


<!-- Modal patient details-->

<div class="modal" id="admiting_modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
     
          <h4 class="modal-title">Add Patient</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
    <!-- data retrived from JS -->
      <div class="modal-body">


      <form method="post" autocomplete="off" action="dmc-old-patients.php">
<label>MRNs</label>
<input class='txtdata' name='mrn' style='text-align: center;' required>
<label>Patient Name</label>
<input class='txtdata' name='pname'  style='text-align: center;' required>
<label>Age</label>
<input class='txtdata' name='age' style='text-align: center;' required>
<label>Gender</label>
<select class='txtdata' name='gender' style='text-align: center;' required>
<option value='Male'>Male</option>
<option value='Female'>Female</option>
</select>
<label>Nationality</label>
<select class='select2 txtdata' name='nationality' style='text-align: center;' required>
 <?php  

foreach($countries as $country)
    echo"
    <option value='".$country['name']."'>".$country['name']."</option>";
  ?>

</select>

<label>Admittion date</label>
<input  class='txtdata' id="admdate"  data-date-format="DD-MM-YYYY" type="text"  name='admdate' style="text-align: center;padding: 0px;" required>


<label>Admitted from</label>
<select class='txtdata' name="admfrom" style="width: 100%;text-align: center;padding: 4px;" required>
<option value='ER'>ER</option>
<option value='ICU'>ICU</option>
<option value='OPD'>OPD</option>
<option value='OR'>OR</option>
<option value='Referral'>Referral</option>
</select>

<label>Primary Consultant</label>
<select class='select2 txtdata' name="primary" style="width: 100%;text-align: center;padding: 4px;" required>

<?php
$formationSQL = "SELECT * FROM members";
$result1 = $mysqli->query($formationSQL);
$consultant = $result1 -> fetch_all(MYSQLI_ASSOC);

foreach ($consultant as $con){
  echo "<option value='".$con['member_id']."'>".$con['full_name']."</option>";
}
?>


</select>

<label>Diagnosis</label>
<select class='txtdata ddxname form-control' style='width: 100%;'  oninput='auto_grow(this)'  multiple='multiple' name='admissiondiagnosis[]' required>
</select>

<label>Current location</label>
<select class='txtdata' name='current_location' style="width: 100%;text-align: center;padding: 4px;" required>
<?php
                        
                            $formationSQL =  "SELECT DISTINCT(current_location) FROM picupatients";
                            $result1 = $mysqli->query($formationSQL);
                            $current_location = $result1 -> fetch_all(MYSQLI_ASSOC);
                            
                            foreach ($current_location as $a){
                                echo  "<option value='".$a['current_location']."'>".$a['current_location']."</option>";
                            }
                        ?>
</select>

<label>Long term?</label>
<select class='txtdata' name='longterm' style="width: 100%;text-align: center;padding: 4px;">
<?php
                        
                            $formationSQL =  "SELECT DISTINCT(longterm) FROM picupatients";
                            $result1 = $mysqli->query($formationSQL);
                            $longterm = $result1 -> fetch_all(MYSQLI_ASSOC);
                            
                            foreach ($longterm as $a){
                                echo  "<option value='".$a['longterm']."'>".$a['longterm']."</option>";
                            }
                        ?>
</select>

<label>Trans Discharge status</label>
<select class='txtdata' name='trans_discharge' style="width: 100%;text-align: center;padding: 4px;" required>
<?php
                        
                            $formationSQL =  "SELECT DISTINCT(trans_discharge) FROM picupatients";
                            $result1 = $mysqli->query($formationSQL);
                            $trans_discharge = $result1 -> fetch_all(MYSQLI_ASSOC);
                            
                            foreach ($trans_discharge as $a){
                                echo  "<option value='".$a['trans_discharge']."'>".$a['trans_discharge']."</option>";
                            }
                        ?>
</select>

<label>Discharge date</label>
<input  class='txtdata' id ="admdate"  data-date-format="DD-MM-YYYY" type="text"  name='med_disdate' style="text-align: center;padding: 0px;" required>
<label>Close file date</label>
<input  class='txtdata' id ="admdate"  data-date-format="DD-MM-YYYY" type="text"  name='disdate' style="text-align: center;padding: 0px;" required>
<label>Delay</label>
<select class='txtdata' name='delay' style="width: 100%;text-align: center;padding: 4px;">
<?php
                        
                            $formationSQL =  "SELECT DISTINCT(delay) FROM picupatients";
                            $result1 = $mysqli->query($formationSQL);
                            $delay = $result1 -> fetch_all(MYSQLI_ASSOC);
                            
                            foreach ($delay as $a){
                                echo  "<option value='".$a['delay']."'>".$a['delay']."</option>";
                            }
                        ?>
</select>

<label>Discharge to</label>

<select class='txtdata' id ="disto" name='disto' style="width: 100%;text-align: center;padding: 4px;" required>
<?php
                        
                            $formationSQL =  "SELECT DISTINCT(DISTO) FROM picupatients";
                            $result1 = $mysqli->query($formationSQL);
                            $disto = $result1 -> fetch_all(MYSQLI_ASSOC);
                            
                            foreach ($disto as $a){
                                echo  "<option value='".$a['DISTO']."'>".$a['DISTO']."</option>";
                            }
                        ?>
</select>

<label>Mortality</label>
<select class='txtdata' name='mortality' style="width: 100%;text-align: center;padding: 4px;" required>
<option value='Alive'>Alive</option>
<option value='Dead'>Dead</option>
</select>

</div>

<div class="modal-footer" style="text-align: center;display: block;">
<button type='submit' value='submit' class='btn btn-success' name='add_pt_btn'>Add Patient</button>

<button type="button" class="btn btn-default" style=" color: black; " data-dismiss="modal">Close</button>

</div>

</form>


      </div>
     
    
  </div>
</div>
</div>    


  </div>
  <!-- /.content-wrapper -->


  <script type="text/javascript">
      $(document).ready(function() {
        $('.select2').select2({
      placeholder: 'Select',
      
    } );
        $('.ddxname').select2({
            placeholder: 'Select',
            minimumInputLength: 4,
            ajax: {
                url: 'fetchicd10.php',
                dataType: 'json',
                delay: 250,
                data: function (data) {
                    return {
                        searchTerm: data.term // search term
                    };
                },
                processResults: function (response) {
                    return {
                        results:response
                    };
                },
                cache: true
            }
        });
      });

    </script>
 

<script>

$('#details_modal').on('show.bs.modal', function(e) {
 
//  var bookId = $(e.relatedTarget).data('book-id');
 var bookId = $(e.relatedTarget).data('book-id');
//  $(e.currentTarget).find('input[name="patientId"]').val(bookId);


 data = {bookId: bookId};
 $.post('dmc-old-patients-details.php', data, function(data){
 $('#pdetailsdiv').html(data);
     
    });
});


$("textarea").each(function(textarea) {
    $(this).height( $(this)[0].scrollHeight );
});


  </script>


<script>
$(function() {
  $('input[name="admdate"]').daterangepicker({
    singleDatePicker: true,
    timePicker: false,
    timePicker24Hour: false,
    showButtonPanel: false,
    autoUpdateInput: false,
    autoApply: true,
    showDropdowns: true,
    minYear: 2010,
    maxYear: parseInt(moment().format('YYYY'),10),
    locale: {
            format: 'YYYY-MM-DD'
        }
  }, ).on("apply.daterangepicker", function (e, picker) {
        picker.element.val(picker.startDate.format(picker.locale.format));
    });
});

$(function() {
  $('input[name="disdate"]').daterangepicker({
    singleDatePicker: true,
    timePicker: false,
    timePicker24Hour: false,
    showButtonPanel: false,
    autoUpdateInput: false,
    autoApply: true,
    showDropdowns: true,
    minYear: 2010,
    maxYear: parseInt(moment().format('YYYY'),10),
    locale: {
            format: 'YYYY-MM-DD'
        }
  }, ).on("apply.daterangepicker", function (e, picker) {
        picker.element.val(picker.startDate.format(picker.locale.format));
    });
});

$(function() {
  $('input[name="med_disdate"]').daterangepicker({
    singleDatePicker: true,
    timePicker: false,
    timePicker24Hour: false,
    showButtonPanel: false,
    autoUpdateInput: false,
    autoApply: true,
    showDropdowns: true,
    minYear: 2010,
    maxYear: parseInt(moment().format('YYYY'),10),
    locale: {
            format: 'YYYY-MM-DD'
        }
  }, ).on("apply.daterangepicker", function (e, picker) {
        picker.element.val(picker.startDate.format(picker.locale.format));
    });
});

</script>

<script>




</script>
<?php

require 'footer.php';

?>

<script>



 function showfunction(v) {
  var aaa = document.getElementById(v);
  // console.log(v);
    aaa.classList.toggle('SeeMore2');
    if(aaa.classList.contains('SeeMore2')){
      aaa.innerHTML='+    ';         
    } else {
      aaa.innerHTML='-    ';
    }


}
 
</script>


