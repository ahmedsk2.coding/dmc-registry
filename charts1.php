<?php 
session_start();
date_default_timezone_set('Asia/Riyadh');
$today=date("Y-m-d");

require ('dbconnect.php');
$kpi=$_REQUEST['kpi'];
$s_date=$_REQUEST['date'];
$interval=$_REQUEST['interval'];
?>
<script src="https://cdn.jsdelivr.net/npm/chart.js"></script>

<?php

$formationSQL = "SELECT * FROM members WHERE position = '3' AND active = 1";
$result1 = $mysqli->query($formationSQL);
$consultants = $result1 -> fetch_all(MYSQLI_ASSOC);

     ?>
   
    
<div class="row">
      <div id="mypresentersTable" class="col-md-12">

      <div class="card">
  <div  class="card-header">
  <h3 class="card-title"><i class="nav-icon fas fa-tachometer-alt text-info"></i> <?php echo ucwords($interval) ?> Overview:</h3><i style=" float: right; " download="download.png" id="btn-Preview-Image" type="button" value="Download" class="fas fa-download"></i>
  </div>
  <!-- /.card-header -->
  <div class="card-body">
  <div class="row" style="background: white" id="divpicture">



<?php
        $label=array();
        $admissions=array();
        $discharges=array();
        $newconsults=array();
        $signedoff=array();
        $chartdata=array();
        $chart_label='';
switch ($kpi){
    case 'los':
      $chart_label="Average lenght of Stay";
        echo "
        <div class='col-sm-12' style='position: relative; height:40vh'>
        <canvas id='myChartlos'></canvas>";
            switch ($interval){
            case 'daily':
            case 'monthly':
                                
                    $month = date("m",strtotime($s_date));
                    $mdate1_name=date("F",strtotime($s_date));
                    $ydate1=date("Y",strtotime($s_date));
                    $chart_title="Average Length of Stay for " . $mdate1_name . " " . $ydate1; 
                  
                    foreach($consultants as $c){

                    $formationSQL = "SELECT ADMDATE, DISDATE FROM picupatients WHERE MONTH(DISDATE) = '".$month."' AND YEAR(DISDATE) = '".$ydate1."' AND consultant_id='".$c['member_id']."' AND (current_location != 'ICU' or current_location is null)";
                    $result1 = $mysqli->query($formationSQL);
                    $dates = $result1 -> fetch_all(MYSQLI_ASSOC);

                
                    $los=array();

                    foreach ($dates as $date){
                        $timeDiff = abs(strtotime($date['ADMDATE']) - strtotime($date['DISDATE']));

                        array_push($los,$timeDiff/86400);

                    }
                    // var_dump($los);
                    // $a = array_filter($los);
                    if(count($los)) {
                        $average = array_sum($los)/count($los);
                    }else {
                    $average = 0;
                    }

                    array_push($label,$c['full_name']);
                    array_push($chartdata,$average);
                  
                  }
                break;

            case "quarterly":
                $month = date("m",strtotime($s_date));
                $ydate1=date("Y",strtotime($s_date));

                if ($month >=1 && $month <=3){
                $quarter=1;
                $chart_title="Average Length of Stay for first quarter"  .$ydate1;
                }elseif($month >=4 && $month <=6){
                    $quarter=2;
                    $chart_title="Average Length of Stay for second quarter "  .$ydate1;
                }elseif($month >=7 && $month <=9){
                    $quarter=3;
                    $chart_title="Average Length of Stay for third quarter "  .$ydate1;
                }elseif($month >=10 && $month <=12){
                    $quarter=4;
                    $chart_title="Average Length of Stay for forth quarter " .$ydate1;
                }
                // by type of discharge
                foreach($consultants as $c){

                    $formationSQL = "SELECT ADMDATE, DISDATE FROM picupatients WHERE QUARTER(DISDATE) = '".$quarter."' AND YEAR(DISDATE) = '".$ydate1."' AND consultant_id='".$c['member_id']."' AND (current_location != 'ICU' or current_location is null)";
                    $result1 = $mysqli->query($formationSQL);
                    $dates = $result1 -> fetch_all(MYSQLI_ASSOC);

                
                    $los=array();

                    foreach ($dates as $date){
                        $timeDiff = abs(strtotime($date['ADMDATE']) - strtotime($date['DISDATE']));

                        array_push($los,$timeDiff/86400);

                    }
                    // var_dump($los);
                    // $a = array_filter($los);
                    if(count($los)) {
                        $average = array_sum($los)/count($los);
                    }else {
                    $average = 0;
                    }

                    array_push($label,$c['full_name']);
                    array_push($chartdata,$average);
                  
                  }
               
            break;
            }
        break;

        case 'readmission':
        
          echo "
          <div class='col-sm-12' style='position: relative; height:40vh'>
          <canvas id='myChartlos'></canvas>";
          $chart_label="72 hours readmission count";
              switch ($interval){
              case 'daily':
              case 'monthly':
                                  
                      $month = date("m",strtotime($s_date));
                      $mdate1_name=date("F",strtotime($s_date));
                      $ydate1=date("Y",strtotime($s_date));
                      $chart_title="Readmissions for " . $mdate1_name . " " . $ydate1; 
                    
                      foreach($consultants as $c){
                          
                        /////////////////////
                            // readmissions
                        //////////////////////////
                        $formationSQL = "SELECT consultant_id,ID, MRN, ADMDATE FROM picupatients WHERE MONTH(ADMDATE) = '".$month."' AND YEAR(ADMDATE) = '".$ydate1."'";
                        $result1 = $mysqli->query($formationSQL);
                        $admitted_patients = $result1 -> fetch_all(MYSQLI_ASSOC);

                        $readmission_count=0;
                        // echo $date1 ."</br>";
                        foreach ($admitted_patients as $s){
                        $formationSQL = "SELECT * FROM picupatients WHERE DISDATE + INTERVAL 3 DAY >='".$s['ADMDATE']."' AND ID < '".$s['ID']."'  AND MRN='".$s['MRN']."' AND (trans_discharge = 'discharge from ICU' or trans_discharge='discharge from ward' or trans_discharge IS NULL) LIMIT 1";
                        $result1 = $mysqli->query($formationSQL);
                        $recentadmission = $result1 -> fetch_array(MYSQLI_ASSOC);
                        
                       if ($c['member_id'] == $recentadmission['consultant_id']){
                        $readmission_count=$readmission_count+1;
                        // var_dump($recentadmission);
                      }
                        }
  
                      array_push($label,$c['full_name']);
                      array_push($chartdata,$readmission_count);
                    
                    }
                  break;
  
              case "quarterly":
                  $month = date("m",strtotime($s_date));
                  $ydate1=date("Y",strtotime($s_date));
                  

                  if ($month >=1 && $month <=3){
                  $quarter=1;
                  $chart_title="72 hours Readmissions for first quarter"  .$ydate1;
                  }elseif($month >=4 && $month <=6){
                      $quarter=2;
                      $chart_title="72 hours Readmissions for second quarter "  .$ydate1;
                  }elseif($month >=7 && $month <=9){
                      $quarter=3;
                      $chart_title="72 hours Readmissions for third quarter "  .$ydate1;
                  }elseif($month >=10 && $month <=12){
                      $quarter=4;
                      $chart_title="72 hours Readmissions for forth quarter " .$ydate1;
                  }
                  foreach($consultants as $c){
                          
                    /////////////////////
                        // readmissions
                    //////////////////////////
                    $formationSQL = "SELECT consultant_id,ID, MRN, ADMDATE FROM picupatients WHERE QUARTER(ADMDATE) = '".$quarter."' AND YEAR(ADMDATE) = '".$ydate1."'";
                    $result1 = $mysqli->query($formationSQL);
                    $admitted_patients = $result1 -> fetch_all(MYSQLI_ASSOC);

                    $readmission_count=0;
                    // echo $date1 ."</br>";
                    foreach ($admitted_patients as $s){
                    $formationSQL = "SELECT * FROM picupatients WHERE DISDATE + INTERVAL 3 DAY >='".$s['ADMDATE']."' AND ID < '".$s['ID']."'  AND MRN='".$s['MRN']."' AND (trans_discharge = 'discharge from ICU' or trans_discharge='discharge from ward' or trans_discharge IS NULL) LIMIT 1";
                    $result1 = $mysqli->query($formationSQL);
                    $recentadmission = $result1 -> fetch_array(MYSQLI_ASSOC);
                    
                   if ($c['member_id'] == $recentadmission['consultant_id']){
                    $readmission_count=$readmission_count+1;
                    // var_dump($recentadmission);
                  }
                    }

                  array_push($label,$c['full_name']);
                  array_push($chartdata,$readmission_count);
                
                }
                 
              break;
              }
          break;
    case 'admission':
        echo "
        <div class='col-sm-12' style='position: relative; height:40vh'>
        <canvas id='myChartadmission'></canvas>"; 
// echo "ahmed";
        
        $month = date("m",strtotime($s_date));
        $mdate1_name=date("F",strtotime($s_date));
        $ydate1=date("Y",strtotime($s_date));

            switch ($interval){
            case 'daily':
                $chart_title="Admissions / Consultations for "  .$s_date;
                // var_dump($consultants);
                foreach($consultants as $c){
                    $formationSQL = "SELECT * FROM picupatients WHERE ADMDATE = '".$s_date."'  AND consultant_id='".$c['member_id']."' AND (current_location != 'ICU' or current_location is null)";
                    $result1 = $mysqli->query($formationSQL);
                    $admittedpcount = mysqli_num_rows($result1);

                    $formationSQL = "SELECT * FROM picupatients WHERE DISDATE = '".$s_date."' AND consultant_id='".$c['member_id']."' AND (current_location != 'ICU' or current_location is null)";
                    $result1 = $mysqli->query($formationSQL);
                    $dischargedpcount = mysqli_num_rows($result1);
                    
                    $formationSQL = "SELECT * FROM consultations WHERE consultation_date = '".$s_date."'  AND consultant_id='".$c['member_id']."' ";
                    $result1 = $mysqli->query($formationSQL);
                    $newconsultscount = mysqli_num_rows($result1);
            
                    $formationSQL = "SELECT * FROM consultations WHERE signoff_date = '".$s_date."'  AND consultant_id='".$c['member_id']."' ";
                    $result1 = $mysqli->query($formationSQL);
                    $signedoffcount = mysqli_num_rows($result1);
            
                    array_push($label,$c['full_name']);
                    array_push($admissions,$admittedpcount);
                    array_push($discharges,$dischargedpcount);
                    array_push($newconsults,$newconsultscount);
                    array_push($signedoff,$signedoffcount);
                }

                break;
            case 'monthly':
                                 
                    $chart_title="Admissions / Consultations for " . $mdate1_name . " " . $ydate1; 
                    foreach($consultants as $c){
                        $formationSQL = "SELECT * FROM picupatients WHERE MONTH(ADMDATE) = '".$month."' AND YEAR(ADMDATE) = '".$ydate1."' AND consultant_id='".$c['member_id']."' AND (current_location != 'ICU' or current_location is null)";
                        $result1 = $mysqli->query($formationSQL);
                        $admittedpcount = mysqli_num_rows($result1);
                        $formationSQL = "SELECT * FROM picupatients WHERE MONTH(DISDATE) = '".$month."' AND YEAR(DISDATE) = '".$ydate1."' AND consultant_id='".$c['member_id']."' AND (current_location != 'ICU' or current_location is null)";
                        $result1 = $mysqli->query($formationSQL);
                        $dischargedpcount = mysqli_num_rows($result1);
                        
                        $formationSQL = "SELECT * FROM consultations WHERE MONTH(consultation_date) = '".$month."' AND YEAR(consultation_date) = '".$ydate1."'  AND consultant_id='".$c['member_id']."' ";
                        $result1 = $mysqli->query($formationSQL);
                        $newconsultscount = mysqli_num_rows($result1);
                
                        $formationSQL = "SELECT * FROM consultations WHERE MONTH(signoff_date) = '".$month."' AND YEAR(signoff_date) = '".$ydate1."'  AND consultant_id='".$c['member_id']."' ";
                        $result1 = $mysqli->query($formationSQL);
                        $signedoffcount = mysqli_num_rows($result1);
                
                        array_push($label,$c['full_name']);
                        array_push($admissions,$admittedpcount);
                        array_push($discharges,$dischargedpcount);
                        array_push($newconsults,$newconsultscount);
                        array_push($signedoff,$signedoffcount);
                    }
             

                break;

            case "quarterly":
               
                if ($month >=1 && $month <=3){
                $quarter=1;
                $chart_title="Admissions / Consultations for first quarter"  .$ydate1;
                }elseif($month >=4 && $month <=6){
                    $quarter=2;
                    $chart_title="Admissions / Consultations for second quarter "  .$ydate1;
                }elseif($month >=7 && $month <=9){
                    $quarter=3;
                    $chart_title="Admissions / Consultations for third quarter "  .$ydate1;
                }elseif($month >=10 && $month <=12){
                    $quarter=4;
                    $chart_title="Admissions / Consultations for forth quarter " .$ydate1;
                }
      
              
                    foreach($consultants as $c){
                        $formationSQL = "SELECT * FROM picupatients WHERE QUARTER(ADMDATE) = '".$quarter."' AND YEAR(ADMDATE) = '".$ydate1."' AND consultant_id='".$c['member_id']."' AND (current_location != 'ICU' or current_location is null)";
                        $result1 = $mysqli->query($formationSQL);
                        $admittedpcount = mysqli_num_rows($result1);
                        $formationSQL = "SELECT * FROM picupatients WHERE QUARTER(DISDATE) = '".$quarter."' AND YEAR(DISDATE) = '".$ydate1."' AND consultant_id='".$c['member_id']."' AND (current_location != 'ICU' or current_location is null)";
                        $result1 = $mysqli->query($formationSQL);
                        $dischargedpcount = mysqli_num_rows($result1);
                        
                        $formationSQL = "SELECT * FROM consultations WHERE QUARTER(consultation_date) = '".$quarter."' AND YEAR(consultation_date) = '".$ydate1."'  AND consultant_id='".$c['member_id']."' ";
                        $result1 = $mysqli->query($formationSQL);
                        $newconsultscount = mysqli_num_rows($result1);
                
                        $formationSQL = "SELECT * FROM consultations WHERE QUARTER(signoff_date) = '".$quarter."' AND YEAR(signoff_date) = '".$ydate1."'  AND consultant_id='".$c['member_id']."' ";
                        $result1 = $mysqli->query($formationSQL);
                        $signedoffcount = mysqli_num_rows($result1);
                
                        array_push($label,$c['full_name']);
                        array_push($admissions,$admittedpcount);
                        array_push($discharges,$dischargedpcount);
                        array_push($newconsults,$newconsultscount);
                        array_push($signedoff,$signedoffcount);
                    }
            break;
            }
    break;
    }
?>

</div><!-- end colomn  -->


<script>
        $(document).ready(function() {


            var element = $("#divpicture"); // global variable
            var getCanvas; // global variable
            var newData;

            $("#btn-Preview-Image").on('click', function() {
                html2canvas(element, {
                    onrendered: function(canvas) {
                        getCanvas = canvas;
                        var imgageData = getCanvas.toDataURL("image/png");
                        var a = document.createElement("a");
                        a.href = imgageData; //Image Base64 Goes here
                        a.download = "Overview.png"; //File name Here
                        a.click(); //Downloaded file
                    }
                });
            });


        });
    </script>

</div><!-- end row -->
</div><!-- end colomn  -->
</div><!-- end row -->
  </div>
  <!-- /.card-body -->
</div>
<!-- /.card -->


<script>
  
  var label = <?php echo json_encode($label); ?>;
  var chartdata = <?php echo json_encode($chartdata); ?>;

  // var consultant = [];
  // var admissions = [];

  // for(var i=0; i<jArray.length; i++){
  //   consultant.push("Dr. " + jArray[i].full_name);
  //   admissions.push(jArray[i].admittions);
  //   // alert(jArray[i].full_name);
  //   }
    // alert(JSON.stringify(label));
  const mlabels = label;

  const mdata = {
    labels: mlabels,
    datasets: [{
      label: '<?php echo $chart_label; ?>',
      backgroundColor: 'rgb(41, 134, 204, 0.9)',
      borderColor: 'rgb(41, 134, 204, 0.9)',
      data: chartdata,
      fill: true,
      stack: 'Stack 0',
    }]
  };
  const colors = ['red', 'blue', 'yellow', 'green'];
  const weekend =[];
  const mconfig = {
    type: 'bar',
    
    data: mdata,
    options: {
      maintainAspectRatio: false,
    plugins: {
      filler: {
        propagate: false,
      },
      title: {
        display: true,
        text: '<?php echo $chart_title; ?>'
      }
    },
    responsive: true,
    interaction: {
      intersect: false,
    },
    scales: {
        y: {
            beginAtZero: true,
            stacked: true,
        },
      x: {
        stacked: true,
        ticks: {
          callback: function(value, index, ticks) {
                    var day =new Date(this.getLabelForValue(value));
                    // alert (day.getDay());
                    if (day.getDay() == 6 || day.getDay() == 5){
                                  // alert("this one");
                                  weekend.push(value);
                                }

                    return this.getLabelForValue(value);

                    },
          color: (c) => {
            if (weekend.includes(c.index)){
            return colors[0]
            }
          }
        }
      },
        }
  },
  };


  const myChartlos = new Chart(
    document.getElementById('myChartlos'),
    mconfig
  );
 
</script>


<script>
  
  var label = <?php echo json_encode($label); ?>;
  var admissions = <?php echo json_encode($admissions); ?>;
  var discharges = <?php echo json_encode($discharges); ?>;
  var newconsults = <?php echo json_encode($newconsults); ?>;
  var signedoff = <?php echo json_encode($signedoff); ?>;

  // var consultant = [];
  // var admissions = [];

  // for(var i=0; i<jArray.length; i++){
  //   consultant.push("Dr. " + jArray[i].full_name);
  //   admissions.push(jArray[i].admittions);
  //   // alert(jArray[i].full_name);
  //   }
    // alert(JSON.stringify(label));
  const labels = label;

  const data = {
    labels: labels,
    datasets: [{
      label: 'Admissions',
      backgroundColor: 'rgb(41, 134, 204, 0.9)',
      borderColor: 'rgb(41, 134, 204, 0.9)',
      data: admissions,
      fill: true,
      stack: 'Stack 0',
    },
    {
      label: 'Discharges',
      backgroundColor: 'rgb(204, 41, 134, 0.9)',
      borderColor: 'rgb(204, 41, 134, 0.9)',
      data: discharges,
      fill: true,
      stack: 'Stack 0',
    },
    {
      label: 'New Consultations',
      backgroundColor: 'rgb(75, 192, 192, 0.9)',
      borderColor: 'rgb(75, 192, 192, 0.9)',
      data: newconsults,
      fill: true,
      stack: 'Stack 1',
    },
    {
      label: 'Signed Off Consultations',
      backgroundColor: 'rgb(255, 205, 86, 0.9)',
      borderColor: 'rgb(255, 205, 86, 0.9)',
      data: signedoff,
      fill: true,
      stack: 'Stack 1',
    }]
  };
  const colors = ['red', 'blue', 'yellow', 'green'];
  const weekend =[];
  const config = {
    type: 'bar',
    
    data: data,
    options: {
      maintainAspectRatio: false,
    plugins: {
      filler: {
        propagate: false,
      },
      title: {
        display: true,
        text: '<?php echo $chart_title; ?>'
      }
    },
    responsive: true,
    interaction: {
      intersect: false,
    },
    scales: {
        y: {
            beginAtZero: true,
            stacked: true,
        },
      x: {
        stacked: true,
        ticks: {
          callback: function(value, index, ticks) {
                    var day =new Date(this.getLabelForValue(value));
                    // alert (day.getDay());
                    if (day.getDay() == 6 || day.getDay() == 5){
                                  // alert("this one");
                                  weekend.push(value);
                                }

                    return this.getLabelForValue(value);

                    },
          color: (c) => {
            if (weekend.includes(c.index)){
            return colors[0]
            }
          }
        }
      },
        }
  },
  };


  const myChartadmission = new Chart(
    document.getElementById('myChartadmission'),
    config
  );
 
</script>
