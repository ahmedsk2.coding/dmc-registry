<?php require "dbconnect.php"; 

$username = "";
$email    = "";

// $_POST['position']="";
$errors = array(); 

if (isset($_POST['reg_user'])) {
    // receive all input values from the form
    $username = mysqli_real_escape_string($mysqli, $_POST['username']);
    $full_name = mysqli_real_escape_string($mysqli, $_POST['full_name']);
    $email = mysqli_real_escape_string($mysqli, $_POST['email']);
    $password_1 = mysqli_real_escape_string($mysqli, $_POST['password_1']);
    $password_2 = mysqli_real_escape_string($mysqli, $_POST['password_2']);
    $position = mysqli_real_escape_string($mysqli, $_POST['position']);

    // form validation: ensure that the form is correctly filled ...
    // by adding (array_push()) corresponding error unto $errors array
    if (empty($username)) { array_push($errors, "Username is required"); }
    if (empty($full_name)) { array_push($errors, "Your full name is required"); }
    if (empty($email)) { array_push($errors, "Email is required"); }
    if (empty($password_1)) { array_push($errors, "Password is required"); }
    if (empty($position)) { array_push($errors, "position is required"); }
    if ($password_1 != $password_2) {
      array_push($errors, "The two passwords do not match");
    }
  
    // first check the database to make sure 
    // a user does not already exist with the same username and/or email
    $user_check_query = "SELECT * FROM members WHERE member_name='".$username."' OR member_email='".$email."' LIMIT 1";
    $result = mysqli_query($mysqli, $user_check_query);
    $user = mysqli_fetch_assoc($result);
    
    if ($user) { // if user exists
      if ($user['member_name'] === $username) {
        array_push($errors, "Username already exists");
      }
  
      if ($user['member_email'] === $email) {
        array_push($errors, "email already exists");
      }
      
    }
  
    // Finally, register user if there are no errors in the form
    if (count($errors) == 0) {
      $today=date("Y-m-d");
        $password =  password_hash($password_1, PASSWORD_DEFAULT);//encrypt the password before saving in the database
  
        $query = "INSERT INTO members (member_name, full_name, member_email, member_password, position, pass_exp_date) 
                  VALUES('".$username."', '".$full_name."','".$email."', '".$password."', '".$position."','".$today."')";
        mysqli_query($mysqli, $query);
        $_SESSION['username'] = $username;
        $_SESSION['success'] = "You are now logged in";
        header('location: index.php?s=success');
    }
  }
  
 
  
  ?>
<!DOCTYPE html>
<html>
<head>
  <title>Registration New Account</title>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">


  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/adminlte.min.css">

  <!-- Password strength meter -->
 

  <!--<script language="javascript" src="vendor\pwdMeter-master\jquery.pwdMeter.js"></script> -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
 

  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

</head>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
	
    <a href="https://www.healthpro.ai/main/">  <img src="dist/img/logo.png" width="100%"></a>
  </div>

 
  <div class="card">
    <div class="card-body login-card-body">
      <p class="login-box-msg">Registeration</p>

<form method="post" autocomplete="off" action="register.php">

<div class="input-group mb-3">
    <div class="col-4" style=" display: table-cell;">
  	    <label>Username</label>
    </div>
    <div class="col-8">
  	  <input type="text" name="username" value="<?php if (isset($_POST['username'])){echo htmlspecialchars($_POST['username']);} ?>" placeholder="Login Name" >
    </div>
</div>
<div class="input-group mb-3">
    <div class="col-4" style=" display: table-cell;">
  	    <label>Full Name</label>
    </div>
    <div class="col-8">
  	  <input type="text" name="full_name" value="<?php if (isset($_POST['full_name'])){echo htmlspecialchars($_POST['full_name']);} ?>" placeholder="Display Name" >
    </div>
</div>
<div class="input-group mb-3">
    <div class="col-4"  style=" display: table-cell;">
  	  <label>Position</label>
    </div>
    <div class="col-11">
  	  <select  type="position" name="position" style=" width: 100%; ">
        <option selected value="">Select</option>
        <?php
        $formationSQL = "SELECT * FROM position";
        $result1 = $mysqli->query($formationSQL);
        $positions = $result1 -> fetch_all(MYSQLI_ASSOC);
        
        

        foreach($positions as $p)
        {

            echo '<option value="' . $p['id'] . '">'.  $p['position']. '</option>';
        }
        ?>
       

    </select>
    </div>
</div>
<div class="input-group mb-3">
    <div class="col-4"  style=" display: table-cell;">
  	  <label>Email</label>
    </div>
    <div class="col-8">
  	  <input type="email" name="email" value="<?php if (isset($_POST['email'])){echo htmlspecialchars($_POST['email']);} ?>" placeholder="E-Mail">
    </div>
</div>
<div class="input-group mb-3">

    <div class="form-group" style=" display: table; margin-bottom: 0px;width: 100%; ">
    <div class="col-4"  style=" display: table-cell;">
      <label for="password">Password</label>
      </div>
      <div class="col-8">
      <input type="password" autocomplete="false" id="password" name="password_1" placeholder="Password">
      </div>


      <div class="progress" style="margin-bottom: 0px;  margin-top: 2%;">
        <div class="progress-bar"></div>
      </div>
    </div>
    
    
</div>
<div class="input-group mb-3">
    <div class="col-4"  style=" display: table-cell;">
        <label>Confirm password</label>
    </div>
    <div class="col-8">
        <input autocomplete="false" type="password" name="password_2" placeholder="Confirm password">
  	</div>
</div>
<div class="input-group mb-3" style='color: red;'>
<?php include('errors.php'); ?>
  	</div>

      <div class="input-group mb-3">
  	  <button type="submit" id='registerbtn' class="btn btn-primary btn-block" name="reg_user" disabled>Register</button>
  	</div>
  	<p>
  		Already a member? <a href=".\">Sign in</a>
  	</p>
  </form>
  </div>
    <!-- /.login-card-body -->
  </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/zxcvbn/4.2.0/zxcvbn.js" type="text/javascript"></script>
  <script type="text/javascript">
    $(document).ready(function () { 
      });
  </script>
<script>
  const button = document.getElementById('registerbtn'); 
    $(function() {
  $.fn.bootstrapPasswordMeter = function(options) {
    var settings = $.extend({
      minPasswordLength: 3,
      level0ClassName: 'progress-bar-danger',
      level0Description: 'Weak',
      level1ClassName: 'progress-bar-danger',
      level1Description: 'Not great',
      level2ClassName: 'progress-bar-warning',
      level2Description: 'Better',
      level3ClassName: 'progress-bar-success',
      level3Description: 'Strong',
      level4ClassName: 'progress-bar-success',
      level4Description: 'Very strong',
      parentContainerClass: '.form-group'
    }, options || {});

    $(this).on("keyup", function() {
      var progressBar = $(this).closest(settings.parentContainerClass).find('.progress-bar');
      var progressBarWidth = 0;
      var progressBarDescription = '';
      if ($(this).val().length >= settings.minPasswordLength) {
        var zxcvbnObj = zxcvbn($(this).val());
        progressBar.removeClass(settings.level0ClassName)
          .removeClass(settings.level1ClassName)
          .removeClass(settings.level2ClassName)
          .removeClass(settings.level3ClassName)
          .removeClass(settings.level4ClassName);
        switch (zxcvbnObj.score) {
          case 0:
            progressBarWidth = 25;
            progressBar.addClass(settings.level0ClassName);
            progressBarDescription = settings.level0Description;
            button.disabled = true;
            button.innerText = 'You need stronger password';
            break;
          case 1:
            progressBarWidth = 25;
            progressBar.addClass(settings.level1ClassName);
            progressBarDescription = settings.level1Description;
            button.disabled = true;
            button.innerText = 'You need stronger password';
            break;
          case 2:
            progressBarWidth = 50;
            progressBar.addClass(settings.level2ClassName);
            progressBarDescription = settings.level2Description;
            button.disabled = true;
            button.innerText = 'You need stronger password';
            break;
          case 3:
            progressBarWidth = 75;
            progressBar.addClass(settings.level3ClassName);
            progressBarDescription = settings.level3Description;
             button.innerText = 'Register';
            button.disabled = false;
            break;
           
          case 4:
            progressBarWidth = 100;
            progressBar.addClass(settings.level4ClassName);
            progressBarDescription = settings.level4Description;
            button.disabled = false;
            button.innerText = 'Register';
            break;
        }
      } else {
        progressBarWidth = 0;
        progressBarDescription = '';
        button.innerText = 'You need stronger password';
        button.disabled = true;
      }
      progressBar.css('width', progressBarWidth + '%');
      progressBar.text(progressBarDescription);
    });
  };
  $('#password').bootstrapPasswordMeter({minPasswordLength:3});
});
  </script>
  
</body>
</html>