<?php 
session_start();

require_once "authCookieSessionValidate.php";

if(!$isLoggedIn) {
    header("Location: ./");
}

?>

  <!-- Navbar -->
<?php
require 'sidebar.php';
	require ('dbconnect.php');

    if (!in_array($user['position'],$access_PICU_control)){
    
        echo "
        <div class='content-wrapper'>
        
      
        <section class='content'>
        <div class='container-fluid'>  
        <div class='alert alert-danger' role='alert'> you dont have permission to access this page, Contact you manager if you need to.
        </div>
        </div>
        </section>
        </div>
        ";
        require 'footer.php';
    
        exit();
      }
?>

     <style>
          .hidden{
                  display: none;
          }
          textarea {
    resize: none;
    overflow: hidden;
}

      </style>

        <script>
 
    function sortTable(n) {
  var table,
    rows,
    switching,
    i,
    x,
    y,
    shouldSwitch,
    dir,
    switchcount = 0;
  table = document.getElementById("myTable");
  switching = true;
  //Set the sorting direction to ascending:
  dir = "asc";
  /*Make a loop that will continue until
  no switching has been done:*/
  while (switching) {
    //start by saying: no switching is done:
    switching = false;
    rows = table.getElementsByTagName("TR");
    /*Loop through all table rows (except the
    first, which contains table headers):*/
    for (i = 1; i < rows.length - 1; i++) { //Change i=0 if you have the header th a separate table.
      //start by saying there should be no switching:
      shouldSwitch = false;
      /*Get the two elements you want to compare,
      one from current row and one from the next:*/
      x = rows[i].getElementsByTagName("TD")[n];
      y = rows[i + 1].getElementsByTagName("TD")[n];
      /*check if the two rows should switch place,
      based on the direction, asc or desc:*/
      if (dir == "asc") {
        if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
          //if so, mark as a switch and break the loop:
          shouldSwitch = true;
          break;
        }
      } else if (dir == "desc") {
        if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
          //if so, mark as a switch and break the loop:
          shouldSwitch = true;
          break;
        }
      }
    }
    if (shouldSwitch) {
      /*If a switch has been marked, make the switch
      and mark that a switch has been done:*/
      rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
      switching = true;
      //Each time a switch is done, increase this count by 1:
      switchcount++;
    } else {
      /*If no switching has been done AND the direction is "asc",
      set the direction to "desc" and run the while loop again.*/
      if (switchcount == 0 && dir == "asc") {
        dir = "desc";
        switching = true;
      }
    }
  }
}


function del(value){
    if(!confirm("Do you really want to delete this user?")) {
    return false;
  }
        var rowname= "row";
        rowname+=value;
        row = document.getElementById(rowname);
        var id = value;
        data = {id: id};
        $.post('dmc-users-delete.php', data, function(data){
        // $(parent).html(data);
        });
        row.style.display = "none";
        
    }
	</script>
      
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
  
	<?php
    $formationSQL = "SELECT * FROM members";
    $result1 = $mysqli->query($formationSQL);
    $members = $result1 -> fetch_all(MYSQLI_ASSOC);

    $formationSQL = "SELECT * FROM position";
    $result1 = $mysqli->query($formationSQL);
    $position = $result1 -> fetch_all(MYSQLI_ASSOC);

    $formationSQL = "SELECT * FROM speciality";
    $result1 = $mysqli->query($formationSQL);
    $speciality = $result1 -> fetch_all(MYSQLI_ASSOC);
	?>



    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Control Dash</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="dashboard.php">Home</a></li>
              <li class="breadcrumb-item active">Control Dash</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">  
      <div class="row">

<div id="limits" class="col-md-6">

           <!-- /.info-box -->

           <div class="card">
             <div  class="card-header">
               <h3 class="card-title"><i class="fas fa-procedures text-info"></i> Limits</h3>
             </div>
             <!-- /.card-header -->
             <div class="card-body">
               <div class="row">
                 <div class="col-md-12">
                 <form method="POST" id="limits">
                 <?php
                                    
                                    $query = "select * from settings";
                                    $result1 = $mysqli->query($query);
                                    $settings = $result1 -> fetch_array(MYSQLI_ASSOC);
                                    // var_dump($settings);
                                    ?>
                        <div class="row">
                            <div class="col-6">
                                <div class="input-group">
                                    <label class="label">Minimum Patients for Hospitalist</label>
                                    <input class="form-control" type="text" name="min_hospitalist" value="<?php echo $settings['min_hospitalist']; ?>" required>
                                    <label class="label">Maximum Patients for Hospitalist</label>
                                    <input class="form-control" type="text" name="max_hospitalist"  value="<?php echo $settings['max_hospitalist']; ?>" required>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="input-group">
                                <label class="label">Minimum Patients for Subspeciality</label>
                                    <input class="form-control" type="text" name="min_sub"  value="<?php echo $settings['min_subs']; ?>" required>
                                    <label class="label">Maximum Patients for Subspeciality</label>
                                    <input class="form-control" type="text" name="max_sub"  value="<?php echo $settings['max_subs']; ?>" required>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="input-group">
                                <label class="label">Short Duration of Admission Less Than</label>
                                    <input class="form-control" type="text" name="short_los"  value="<?php echo $settings['short_los']; ?>" required>
                                    <label class="label">Long Duration of Admission More Than</label>
                                    <input class="form-control" type="text" name="long_los"  value="<?php echo $settings['long_los']; ?>" required>
                                </div>
                            </div>
                            
                        </div>

                        <div class="p-t-15">
                            <button class="btn btn-info" type="submit" name="submitlimits" value="submit">Update</button>
                        </div>
                    </form>
					<?php

					

  if(isset($_REQUEST['submitlimits']))
    {
	
	 if (empty($_POST['min_hospitalist'] || $_POST['max_hospitalist'] || $_POST['min_sub'] || $_POST['max_sub'] || $_POST['short_los'] || $_POST['long_los'] )
	){ 
 // Setting error message
 echo '<p style="color: red;"><strong>Please, Fill all the form</strong></p>';
		 
 } else { 
    
    
	  
                    	  $query =  "UPDATE settings SET long_los='".$_POST['long_los']."', max_hospitalist='".$_POST['max_hospitalist']."', max_subs ='".$_POST['max_sub']."',
                         min_hospitalist= '".$_POST['min_hospitalist']."', min_subs='".$_POST['min_sub']."', short_los='".$_POST['short_los']."' WHERE id = '0'";	 
                    
                    	 if ($mysqli->query($query) === TRUE) {
                     echo '<p style="color:green;"><span>Updated successfully..!!</span></p>';
                     } else {
                     echo '<p style="color:red;"><span>Update Failed..!!</span></p>';
                    	 echo("Error description: " . mysqli_error($mysqli));
                     }    
	
             		
    }
}

?>
                 </div>
                 <!-- /.col -->
                 
                 <!-- /.col -->
               </div>
               <!-- /.row -->
             </div>
             <!-- /.card-body -->
            
             <!-- /.footer -->
           </div>
           <!-- /.card -->
</div>
           
<div id="specialty" class="col-md-3">

           <!-- /.info-box -->

           <div class="card">
             <div  class="card-header">
               <h3 class="card-title"><i class="fas fa-diagnoses text-info"></i> Add Specialty</h3>
             </div>
             <!-- /.card-header -->
             <div class="card-body">
               <div class="row">
                 <div class="col-md-12">
                 <form method="POST" id="addspecialty">
                        <div class="row">
                            <div class="col">
                            <div class="input-group">
                                    <label class="label">Specialty Added</label>
                                    <?php
                                    
                                    $query = "select * from speciality";
                                    $speciality = $mysqli->query($query);
                                    
                                   echo" <ul style='list-style-position: inside;'>
                                    ";
                                      
                                      foreach($speciality as $ss)
                                {
                              
                                    echo '<li>'.  $ss['specilaity']. '</li>';
                                }
           
                                    
                                    ?>
                                     </ul>
                                    
                                </div>
                                <div class="input-group">
                                    <label class="label">Specialty Name</label>
                                    <input class="form-control" type="text" name="specialty" required>
                                </div>
                            </div>
                         
                            <button class="btn btn-info" style="width: 100%;padding: 0 10%;" type="submit" name="submitspecialty" value="submit">Add Specialty</button>
                      
                        </div>
                        

                       
                    </form>
					<?php

					

  if(isset($_REQUEST['submitspecialty']))
    {
			$name = "";
	
	 if (empty($_POST['specialty'])
	){ 
 // Setting error message
 echo '<p style="color: red;"><strong>Please, Fill all the form</strong></p>';
		 
 } else { 
     	
		$name = $_POST['specialty'];
        $checkquery = "select * from speciality where specilaity='".$name."'";
	    $checkresult = $mysqli->query($checkquery);
	    
            	    if(mysqli_num_rows($checkresult)>0)
             {
              echo '<p style="color:red;"><span>Specilaity is Already in the list..!!</span></p>';
             }

     else {
      
                    	  $query =  "INSERT INTO speciality (specilaity) VALUES ('".$name."')";	 
                    
                    	 if ($mysqli->query($query) === TRUE) {
                     echo '<p style="color:green;"><span>Submitted successfully..!!</span></p>';
                     echo "<script language='javascript'>\n";
                     echo "window.location.href = 'control.php';";
                     echo "</script>\n";
                     
                     } else {
                     echo '<p style="color:red;"><span>Submission Failed..!!</span></p>';
                    // 	 echo("Error description: " . mysqli_error($mysqli));
                     }    
	
             }		
    }
}

?>
 
                 </div>
                 <!-- /.col -->
                 
                 <!-- /.col -->
               </div>
               <!-- /.row -->
             </div>
             <!-- /.card-body -->
            
             <!-- /.footer -->
           </div>
           <!-- /.card -->
</div>
         
         
<div id="other_specialities" class="col-md-3">

           <!-- /.info-box -->

           <div class="card">
             <div  class="card-header">
               <h3 class="card-title"><i class="fas fa-diagnoses text-info"></i> Add Allied Specialty</h3>
             </div>
             <!-- /.card-header -->
             <div class="card-body">
               <div class="row">
                 <div class="col-md-12">
                 <form method="POST" id="addspecialty">
                        <div class="row">
                            <div class="col">
                            <div class="input-group">
                                    <label class="label">Allied Specialty Added</label>
                                    <?php
                                    
                                    $query = "select * from other_specialities";
                                    $other_specialities = $mysqli->query($query);
                                    
                                   echo" <ul style='list-style-position: inside;'>
                                    ";
                                      
                                      foreach($other_specialities as $ss)
                                {
                              
                                    echo '<li>'.  $ss['specilaity']. '</li>';
                                }
           
                                    
                                    ?>
                                     </ul>
                                    
                                </div>
                                <div class="input-group">
                                    <label class="label">Allied Specialty Name</label>
                                    <input class="form-control" type="text" name="other_specialities" required>
                                </div>
                            </div>
                                                       <button class="btn btn-info" style="width: 100%;padding: 0 10%;" type="submit" name="submitother_specialities" value="submit">Add Allied Specialty</button>
 
                        </div>
                        

                       
                        
                    </form>
					<?php

					

  if(isset($_REQUEST['submitother_specialities']))
    {
			$name = "";
	
	 if (empty($_POST['other_specialities'])
	){ 
 // Setting error message
 echo '<p style="color: red;"><strong>Please, Fill all the form</strong></p>';
		 
 } else { 
     	
		$name = $_POST['other_specialities'];
        $checkquery = "select * from other_specialities where specilaity='".$name."'";
	    $checkresult = $mysqli->query($checkquery);
	    
            	    if(mysqli_num_rows($checkresult)>0)
             {
              echo '<p style="color:red;"><span>Specilaity is Already in the list..!!</span></p>';
             }

     else {
      
                    	  $query =  "INSERT INTO other_specialities (specilaity) VALUES ('".$name."')";	 
                    
                    	 if ($mysqli->query($query) === TRUE) {
                     echo '<p style="color:green;"><span>Submitted successfully..!!</span></p>';
                     echo "<script language='javascript'>\n";
                     echo "window.location.href = 'control.php';";
                     echo "</script>\n";
                     } else {
                     echo '<p style="color:red;"><span>Submission Failed..!!</span></p>';
                    // 	 echo("Error description: " . mysqli_error($mysqli));
                     }    
	
             }		
    }
}

?>
 
                 </div>
                 <!-- /.col -->
                 
                 <!-- /.col -->
               </div>
               <!-- /.row -->
             </div>
             <!-- /.card-body -->
            
             <!-- /.footer -->
           </div>
           <!-- /.card -->
</div>
          
</div> <!--row -->



<div class="row">
           
<div id="consultation" class="col-md-3">

           <!-- /.info-box -->

           <div class="card">
             <div  class="card-header">
               <h3 class="card-title"><i class="fas fa-diagnoses text-info"></i> Add Consultation Indication</h3>
             </div>
             <!-- /.card-header -->
             <div class="card-body">
               <div class="row">
                 <div class="col-md-12">
                 <form method="POST" id="addconsultreason">
                        <div class="row">
                            <div class="col">
                            <div class="input-group">
                                    <label class="label">Indications Added</label>
                                    <?php
                                    
                                    $query = "select * from consultation_reason";
                                    $indications = $mysqli->query($query);
                                    
                                   echo" <ul style='list-style-position: inside;'>
                                    ";
                                      
                                      foreach($indications as $ind)
                                {
                              
                                    echo '<li>'.  $ind['consultation_reason']. '</li>';
                                }
           
                                    
                                    ?>
                                     </ul>
                                    
                                </div>
                                <div class="input-group">
                                    <label class="label">Indication</label>
                                    <input class="form-control" type="text" name="indication_name" required>
                                </div>
                            </div>
                      <button class="btn btn-info" style="width: 100%;padding: 0 10%;" type="submit" name="submitindication" value="submit">Add Indication</button>

                        </div>
                        

                      
                  
                    </form>
					<?php

					

  if(isset($_REQUEST['submitindication']))
    {
			$name = "";
	
	 if (empty($_POST['indication_name'])
	){ 
 // Setting error message
 echo '<p style="color: red;"><strong>Please, Fill all the form</strong></p>';
		 
 } else { 
     	
		$name = $_POST['indication_name'];
        $checkquery = "select * from consultation_reason where consultation_reason='".$name."'";
	    $checkresult = $mysqli->query($checkquery);
	    
            	    if(mysqli_num_rows($checkresult)>0)
             {
              echo '<p style="color:red;"><span>Indication is Already in the list..!!</span></p>';
             }

     else {
      
                    	  $query =  "INSERT INTO consultation_reason (consultation_reason) VALUES ('".$name."')";	 
                    
                    	 if ($mysqli->query($query) === TRUE) {
                     echo '<p style="color:green;"><span>Submitted successfully..!!</span></p>';

                     echo "<script language='javascript'>\n";
                     echo "window.location.href = 'control.php';";
                     echo "</script>\n";

                     } else {
                     echo '<p style="color:red;"><span>Submission Failed..!!</span></p>';
                    // 	 echo("Error description: " . mysqli_error($mysqli));
                     }    
	
             }		
    }
}

?>
 
                 </div>
                 <!-- /.col -->
                 
                 <!-- /.col -->
               </div>
               <!-- /.row -->
             </div>
             <!-- /.card-body -->
            
             <!-- /.footer -->
           </div>
           <!-- /.card -->
</div>
         
         
          
</div> <!--row -->




<div class="row">

 <div id="mypresentersTable" class="col-md-12">

            <!-- /.info-box -->

            <div class="card">
              <div  class="card-header">
                <h3 class="card-title"><i class="fas fa-user-tie text-info"></i> Manage Active Users</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <div class="row">
                  <div class="col-md-12">
                    <div class="chart-responsive">
<?php 

   ?>
                    
                       <!-- <input type="text" class="mytablesearchInput" id="mypresenterssearchInput" onkeyup="mypresentersTable()" placeholder="Search for speaker names.." title="Type in a speaker name"> -->
                                        <div  class="table-responsive"> 
                                        <input class="form-control" id="search" type="text" placeholder="Search..">

                        <table id="myTable" class="table table-sm " cellspacing="0" width="100%">
                <thead>
                  <tr>
                
                    <th onclick="sortTable(0)" class="th-sm" scope="col" >Full Name <i class="fa fa-sort" aria-hidden="true"></i></th>
                    <th  class="th-sm" scope="col" >Login Name</th>
                    <th class="th-sm" scope="col" >Email</th>
                    <th class="th-sm" scope="col" >Position</th>
                    <th class="th-sm" scope="col" >Specilaity</th>
                    <th   class="th-sm" scope="col" >Status</th>
                    <th  onclick="sortTable(5)" class="th-sm" scope="col" >On Service <i class="fa fa-sort" aria-hidden="true"></i></th>
                    <th class="th-sm" scope="col" >Assign patients</th>
                    <th class="th-sm" scope="col" >Add patients</th>
                    <th class="th-sm" scope="col" >Manage patients</th>
                    <th class="th-sm" scope="col" >Modify patients</th>
                  </tr>
                </thead>
                <tbody id="searchtable">
              

                  <?php

                                                                         
usort($members, function($a, $b) {
  return $a['active'] <=> $b['active'];
});

// $decodedP=array();                       
            foreach($members as $s){
              if ($s['active']=='1'){
       echo " <tr class='eachrow' id='row".$s['member_id']."'>
     
      <td class='eachcol full_name'  scope='row' ><input class='txtdata'  name='full_name' id='full_name' value='".$s['full_name']."' style='text-align: center;' >
      <input class='txtdata' type='hidden' name='member_id' id='member_id' value='".$s['member_id']."' style='text-align: center;' >";

      ///// next lines are used for search JS
      echo "<p style='display:none;'>".$s['full_name']."</p>";
      foreach($position as $value)
      {
          if ($s['position'] == $value['id']){      
    
         echo  "<p style='display:none;'>".$value['position']."</p>";
    
        }
      }
      foreach($speciality as $sp)
      {
          if ($s['specialty_id'] == $sp['id']){      

       
          echo  "<p style='display:none;'>".$sp['specilaity']."</p>";
          }
        }


      /////////// End of search lines
      echo"
      
      </td>
      
      <td class='eachcol member_name'  scope='row' ><p>".$s['member_name']."</p></td>

      <td class='eachcol member_email'  scope='row' ><p>".$s['member_email']."</p></td>

      <td class='eachcol position'><select class='txtdata position form-control' style='width: 100%;'  name='position'>
      ";
      foreach($position as $value)
  {
      if ($s['position'] == $value['id']){      

      $position_name= $value['position'];

    }elseif ($s['position'] ==0){
        $position_name= 'Administrator';
    }}
      echo '<option selected value="' . $s['position'] . '">'. $position_name .'</option>';
        foreach($position as $value)
  {


      echo '<option value="' . $value['id'] . '">'.  $value['position']. '</option>';
      
  }
  echo '<option  value="0">Administrator</option>';

      echo"
      </select></td>

      <td class='eachcol speciality'> ";
      
if ($s['position'] == '3'){
                  echo"
                  <select class='txtdata speciality form-control' style='width: 100%;'  name='speciality'>
                  ";

                  foreach($speciality as $sp)
              {
                  if ($s['specialty_id'] == $sp['id']){      

                  $speciality_name= $sp['specilaity'];

                  echo '<option selected value="' . $s['specialty_id'] . '">'. $speciality_name .'</option>';
                    
                  }
                  echo '<option value="' . $sp['id'] . '">'.  $sp['specilaity']. '</option>';
                  
              }
                  echo"
                  </select>";
    }

      echo"
      </td>

      <td class='eachcol activate'>";
   if ($s['active'] == 1){

    $formationSQL = "SELECT * FROM picupatients WHERE DISDATE IS NULL AND consultant_id='".$s['member_id']."' ";
		$result1 = $mysqli->query($formationSQL);
		$pcount = mysqli_num_rows($result1);

// echo "$pcount";
      
      
 
    
    if ($pcount !== 0 || $s['on_service'] == 1 || $s['assign_access'] == 1 || $s['add_new_patient'] == 1 || $s['manage_patient'] == 1 || $s['modify_patient'] == 1 ){
       echo "<input style='width: auto;' type='checkbox' name='activate' value='1' checked disabled>
      ";}else{
        echo"<input style='width: auto;' class='txtdata' type='checkbox' name='activate' value='1' checked>
        ";
      }
   }else{
     echo " <input style='width: auto;' class='txtdata'  type='checkbox' name='activate'>";
   }
   echo "<label for='activate' style=' display: contents; '> Active</label><br> 
   </td>

   <td class='eachcol service'>";
   if ($s['on_service'] == 1){
     echo"<input style='width: auto;' class='txtdata' type='checkbox' name='service' value='1' checked>
     ";
   }else{
     echo " <input style='width: auto;' class='txtdata'  type='checkbox' name='service'>";
   }
   echo "<label for='service' style=' display: contents; '> On Service</label><br> 
   </td>
   
   <td class='eachcol assignaccess'>";
   if ($s['assign_access'] == 1){
     echo"<input style='width: auto;' class='txtdata' type='checkbox' name='assignaccess' value='1' checked>
     ";
   }else{
     echo " <input style='width: auto;' class='txtdata'  type='checkbox' name='assignaccess'>";
   }
   echo "<label for='assignaccess' style=' display: contents; '> Allowed</label><br> 
   </td>

   <td class='eachcol addpatient'>";
   if ($s['add_new_patient'] == 1){
     echo"<input style='width: auto;' class='txtdata' type='checkbox' name='addpatient' value='1' checked>
     ";
   }else{
     echo " <input style='width: auto;' class='txtdata'  type='checkbox' name='addpatient'>";
   }
   echo "<label for='addpatient' style=' display: contents; '> Allowed</label><br> 
   </td>
   
   <td class='eachcol managepatient'>";
   if ($s['manage_patient'] == 1){
     echo"<input style='width: auto;' class='txtdata' type='checkbox' name='managepatient' value='1' checked>
     ";
   }else{
     echo " <input style='width: auto;' class='txtdata'  type='checkbox' name='managepatient'>";
   }
   echo "<label for='managepatient' style=' display: contents; '> Allowed</label><br> 
   </td>

   <td class='eachcol modifypatient'>";
   if ($s['modify_patient'] == 1){
     echo"<input style='width: auto;' class='txtdata' type='checkbox' name='modifypatient' value='1' checked>
     ";
   }else{
     echo " <input style='width: auto;' class='txtdata'  type='checkbox' name='modifypatient'>";
   }
   echo "<label for='modifypatient' style=' display: contents; '> Allowed</label><br> 
   </td>

         </tr >
      ";
      // <textarea style='width: 100%;  padding-left: 5px;padding-right: 5px;'; oninput='auto_grow(this)' class='txtdata' name='procedures' value='".$s['PROCEDURES']."' >".$s['PROCEDURES']."</textarea>
      // onclick='discharge(".$s['ID'].")'
    // var_dump($decodedP) ;
  }
    }
            // 

            ?>

         
  </tbody>
</table> 
* You only can deactivate if all permissions removed and if there are no active patients under the account
</div>



           
                    
                    
                    </div>
                    <!-- ./chart-responsive -->
                  </div>
                  <!-- /.col -->
                  
                  <!-- /.col -->
                </div>
                <!-- /.row -->
              </div>
              <!-- /.card-body -->
             
              <!-- /.footer -->
            </div>
            <!-- /.card -->
</div>
			

           
 </div> <!--row -->
			
 

<!-- in active users -->


<div class="row">

 <div id="mypresentersTable" class="col-md-12">

            <!-- /.info-box -->

            <div class="card">
              <div  class="card-header">
                <h3 class="card-title"><i class="fas fa-user-tie text-info"></i> Manage In-Active Users</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <div class="row">
                  <div class="col-md-12">
                    <div class="chart-responsive">
<?php 

   ?>
                    
                       <!-- <input type="text" class="mytablesearchInput" id="mypresenterssearchInput" onkeyup="mypresentersTable()" placeholder="Search for speaker names.." title="Type in a speaker name"> -->
                                        <div  class="table-responsive"> 

                        <table id="myTable" class="table table-sm " cellspacing="0" width="100%">
                <thead>
                  <tr>
                
                    <th onclick="sortTable(0)" class="th-sm" scope="col" >Full Name <i class="fa fa-sort" aria-hidden="true"></i></th>
                    <th  class="th-sm" scope="col" >Login Name</th>
                    <th class="th-sm" scope="col" >Email</th>
                    <th class="th-sm" scope="col" >Position</th>
                    <th class="th-sm" scope="col" >Specilaity</th>
                    <th   class="th-sm" scope="col" >Status</th>
                    <!-- <th  onclick="sortTable(5)" class="th-sm" scope="col" >On Service <i class="fa fa-sort" aria-hidden="true"></i></th>
                    <th class="th-sm" scope="col" >Assign patients</th>
                    <th class="th-sm" scope="col" >Add patients</th>
                    <th class="th-sm" scope="col" >Manage patients</th>
                    <th class="th-sm" scope="col" >Modify patients</th> -->
                    
                    <!-- <th class="th-sm" scope="col" >Action</th> -->
                  </tr>
                </thead>
                <tbody id="searchtable">
              

                  <?php

                                                                         
usort($members, function($a, $b) {
  return $a['active'] <=> $b['active'];
});

// $decodedP=array();                       
            foreach($members as $s){
              if ($s['active'] !== '1'){
       echo " <tr class='eachrow' id='row".$s['member_id']."'>
     
      <td class='eachcol full_name'  scope='row' ><input class='txtdata'  name='full_name' id='full_name' value='".$s['full_name']."' style='text-align: center;' >
      <input class='txtdata' type='hidden' name='member_id' id='member_id' value='".$s['member_id']."' style='text-align: center;' >";

      ///// next lines are used for search JS
      echo "<p style='display:none;'>".$s['full_name']."</p>";
      foreach($position as $value)
      {
          if ($s['position'] == $value['id']){      
    
         echo  "<p style='display:none;'>".$value['position']."</p>";
    
        }
      }
      foreach($speciality as $sp)
      {
          if ($s['specialty_id'] == $sp['id']){      

       
          echo  "<p style='display:none;'>".$sp['specilaity']."</p>";
          }
        }


      /////////// End of search lines
      echo"
      
      </td>
      
      <td class='eachcol member_name'  scope='row' ><p>".$s['member_name']."</p></td>

      <td class='eachcol member_email'  scope='row' ><p>".$s['member_email']."</p></td>

      <td class='eachcol position'><select class='txtdata position form-control' style='width: 100%;'  name='position'>
      ";
      foreach($position as $value)
  {
      if ($s['position'] == $value['id']){      

      $position_name= $value['position'];

    }elseif ($s['position'] ==0){
        $position_name= 'Administrator';
    }}
      echo '<option selected value="' . $s['position'] . '">'. $position_name .'</option>';
        foreach($position as $value)
  {


      echo '<option value="' . $value['id'] . '">'.  $value['position']. '</option>';
      
  }
  echo '<option  value="0">Administrator</option>';

      echo"
      </select></td>

      <td class='eachcol speciality'> ";
      
if ($s['position'] == '3'){
                  echo"
                  <select class='txtdata speciality form-control' style='width: 100%;'  name='speciality'>
                  ";

                  foreach($speciality as $sp)
              {
                  if ($s['specialty_id'] == $sp['id']){      

                  $speciality_name= $sp['specilaity'];

                  echo '<option selected value="' . $s['specialty_id'] . '">'. $speciality_name .'</option>';
                    
                  }
                  echo '<option value="' . $sp['id'] . '">'.  $sp['specilaity']. '</option>';
                  
              }
                  echo"
                  </select>";
    }

      echo"
      </td>

      <td class='eachcol activate'>";
   if ($s['active'] == 1){
     echo"<input style='width: auto;' class='txtdata' type='checkbox' name='activate' value='1' checked>
     ";
   }else{
     echo " <input style='width: auto;' class='txtdata'  type='checkbox' name='activate'>";
   }
   echo "<label for='activate' style=' display: contents; '> Active</label><br> 
   </td>";

  //  <td class='eachcol service'>";
  //  if ($s['on_service'] == 1){
  //    echo"<input style='width: auto;' class='txtdata' type='checkbox' name='service' value='1' checked>
  //    ";
  //  }else{
  //    echo " <input style='width: auto;' class='txtdata'  type='checkbox' name='service'>";
  //  }
  //  echo "<label for='service' style=' display: contents; '> On Service</label><br> 
  //  </td>
   
  //  <td class='eachcol assignaccess'>";
  //  if ($s['assign_access'] == 1){
  //    echo"<input style='width: auto;' class='txtdata' type='checkbox' name='assignaccess' value='1' checked>
  //    ";
  //  }else{
  //    echo " <input style='width: auto;' class='txtdata'  type='checkbox' name='assignaccess'>";
  //  }
  //  echo "<label for='assignaccess' style=' display: contents; '> Allowed</label><br> 
  //  </td>

  //  <td class='eachcol addpatient'>";
  //  if ($s['add_new_patient'] == 1){
  //    echo"<input style='width: auto;' class='txtdata' type='checkbox' name='addpatient' value='1' checked>
  //    ";
  //  }else{
  //    echo " <input style='width: auto;' class='txtdata'  type='checkbox' name='addpatient'>";
  //  }
  //  echo "<label for='addpatient' style=' display: contents; '> Allowed</label><br> 
  //  </td>
   
  //  <td class='eachcol managepatient'>";
  //  if ($s['manage_patient'] == 1){
  //    echo"<input style='width: auto;' class='txtdata' type='checkbox' name='managepatient' value='1' checked>
  //    ";
  //  }else{
  //    echo " <input style='width: auto;' class='txtdata'  type='checkbox' name='managepatient'>";
  //  }
  //  echo "<label for='managepatient' style=' display: contents; '> Allowed</label><br> 
  //  </td>

  //  <td class='eachcol modifypatient'>";
  //  if ($s['modify_patient'] == 1){
  //    echo"<input style='width: auto;' class='txtdata' type='checkbox' name='modifypatient' value='1' checked>
  //    ";
  //  }else{
  //    echo " <input style='width: auto;' class='txtdata'  type='checkbox' name='modifypatient'>";
  //  }
  //  echo "<label for='modifypatient' style=' display: contents; '> Allowed</label><br> 
  //  </td>";
  //  echo"
  //  <td class='eachcol id' scope='row' ><input class='txtdata' name='id' value='".$s['member_id']."' style='display: none;'>
  //  ";

  //  if ($position_name== 'Administrator'){
  // }else{
  //  echo"
  //  <a class='btn btn-danger'  onclick='del(".$s['member_id'].")'  style='color: aliceblue;line-height: 2;padding: 0px 10%;margin: 2%;'>Delete</a>
  //  ";
  // }

  //  echo"
  //     </td>
  echo"
         </tr >
      ";
      // <textarea style='width: 100%;  padding-left: 5px;padding-right: 5px;'; oninput='auto_grow(this)' class='txtdata' name='procedures' value='".$s['PROCEDURES']."' >".$s['PROCEDURES']."</textarea>
      // onclick='discharge(".$s['ID'].")'
    // var_dump($decodedP) ;
}
    }
            // 

            ?>

         
  </tbody>
</table> 

</div>



           
                    
                    
                    </div>
                    <!-- ./chart-responsive -->
                  </div>
                  <!-- /.col -->
                  
                  <!-- /.col -->
                </div>
                <!-- /.row -->
              </div>
              <!-- /.card-body -->
             
              <!-- /.footer -->
            </div>
            <!-- /.card -->
</div>
			

           
  </div>

<!-- PAGE SCRIPTS -->

<!-- AdminLTE App -->
<script src="dist/js/adminlte.js"></script>

<!-- OPTIONAL SCRIPTS -->
<script src="dist/js/demo.js"></script>

</div><!--/. container-fluid -->

    </section>
    <!-- /.content -->


<!-- PAGE SCRIPTS -->    


  </div>
  <!-- /.content-wrapper -->


<?php

require 'footer.php';

?>


<script>
$(document).ready(function($) {
  
  $('.txtdata').on('change', function(){
var parent = $(this).parent('.eachcol').parent('.eachrow');
    var id = $(parent).find('.full_name').find('input[name$="member_id"]').val();
    var full_name = $(parent).find('.full_name').find('input[name$="full_name"]').val();
    // alert(id);
    var speciality = $(parent).find('.speciality').find('select').val();
    var position = $(parent).find('.position').find('select').val();
    var activatebox = $(parent).find('.activate').find('input[name$="activate"]');
    if(activatebox.prop('checked') === true){
          var activate = 1;
        }else{
          var activate = 0;
        }

        var onservicebox = $(parent).find('.service').find('input[name$="service"]');
    if(onservicebox.prop('checked') === true){
          var service = 1;
        }else{
          var service = 0;
        }
        var assignaccessbox = $(parent).find('.assignaccess').find('input[name$="assignaccess"]');
    if(assignaccessbox.prop('checked') === true){
          var accessbox = 1;
        }else{
          var accessbox = 0;
        }

        var addpatientsbox = $(parent).find('.addpatient').find('input[name$="addpatient"]');
    if(addpatientsbox.prop('checked') === true){
          var addbox = 1;
        }else{
          var addbox = 0;
        }

        var managepatient = $(parent).find('.managepatient').find('input[name$="managepatient"]');
    if(managepatient.prop('checked') === true){
          var managebox = 1;
        }else{
          var managebox = 0;
        }
    //  alert (scot);
    var modifypatient = $(parent).find('.modifypatient').find('input[name$="modifypatient"]');
    if(modifypatient.prop('checked') === true){
          var modifybox = 1;
        }else{
          var modifybox = 0;
        }
    
    
    var attribChanged = $(this).attr('name');
    data = {id: id, position: position,speciality:speciality, activate: activate,full_name:full_name, service:service, accessbox:accessbox,
      managebox:managebox, modifybox:modifybox, addbox:addbox};
    $.post('dmc-users-update.php', data, function(data){
    //   $(parent).html(data);
     
    });
    $(this).parent('.eachcol').css("backgroundColor", "#90EE90");

  });
});

$(document).ready(function(){
  $("#search").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#searchtable tr").filter(function() {
      $(this).toggle($(this).find('td:eq(0), td:eq(1), td:eq(2)').text().replace(/\s+/g, ' ').toLowerCase().indexOf(value) > -1)
    });
  });
});

</script>