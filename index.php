<?php
session_start();

require_once "Auth.php";
require_once "Util.php";

$auth = new Auth();
$db_handle = new DBController();
$util = new Util();

require_once "authCookieSessionValidate.php";
$active = "";
if ($isLoggedIn) {
    $util->redirect("dashboard.php");
}


if (! empty($_POST["login"])) {
    $isAuthenticated = false;
    /// 2 liner for the logout if inactive
    $_SESSION['last_login_timestamp'] = time(); 
    $_SESSION["name"] = $_POST["member_name"];

    $username = $_POST["member_name"];
    $password = $_POST["member_password"];
    

    $user = $auth->getMemberByUsername($username);
    if (password_verify($password, $user[0]["member_password"])) {
      $active = $user[0]["active"];
      $pass_date = $user[0]["pass_exp_date"];
      $expirydate = date('Y-m-d', strtotime("+3 months", strtotime($pass_date)));
      
      if (date('Y-m-d') > $expirydate){
             
          $_SESSION["member_id"] = $user[0]["member_id"]; 
          $util->redirect("change-password.php");
          
        }else {
          $isAuthenticated = true;
        }
    }

    if ($isAuthenticated) {
     
       
      if ($active == 1){
              
                $_SESSION["member_id"] = $user[0]["member_id"]; 
                    // Set Auth Cookies if 'Remember Me' checked
                    if (! empty($_POST["remember"])) {
                        setcookie("member_login", $username, $cookie_expiration_time);
                        
                        $random_password = $util->getToken(16);
                        setcookie("random_password", $random_password, $cookie_expiration_time);
                        
                        $random_selector = $util->getToken(32);
                        setcookie("random_selector", $random_selector, $cookie_expiration_time);
                        
                        $random_password_hash = password_hash($random_password, PASSWORD_DEFAULT);
                        $random_selector_hash = password_hash($random_selector, PASSWORD_DEFAULT);
                        
                        $expiry_date = date("Y-m-d H:i:s", $cookie_expiration_time);
                        
                        // mark existing token as expired
                        $userToken = $auth->getTokenByUsername($username, 0);
                        if (! empty($userToken[0]["id"])) {
                            $auth->markAsExpired($userToken[0]["id"]);
                        }
                        // Insert new token
                        $auth->insertToken($username, $random_password_hash, $random_selector_hash, $expiry_date);
                    } else {
                        $util->clearAuthCookie();
                    }
                
                    $util->redirect("dashboard.php");

      }else {
        $message = "Need for activation, contact your manager";
      }
    }else {
        $message = "Invalid Login";
    }

 
}

?>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>DMC Registry | Log in</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <a href="https://www.healthpro.ai/main/">  <img src="dist/img/logo.png" width="100%"></a>
  </div>
  <!-- /.login-logo -->
	
	<?php
            $msg = '';
            
            if (isset($_POST['login']) && !empty($_POST['username']) 
               && !empty($_POST['password'])) {
				
               if ($_POST['username'] == '1234' && 
                  $_POST['password'] == '1234') {
                  $_SESSION['valid'] = true;
                  $_SESSION['timeout'] = time();
                  $_SESSION['username'] = 'tutorialspoint';
                  
                  header("Location: http://www.yourwebsite.com/user.php"); 
				   		exit();
               }else {
                  $msg = 'Wrong username or password';
               }
            }
         ?>
  <div class="card">
    <div class="card-body login-card-body">
      <p class="login-box-msg">Log in</p>
      <div class="error-message" style="color:green;font-weight: 700;">
      <?php
      
      if (isset($_GET['s']) AND strcmp($_GET['s'], 'success') == 0){ 
          	echo "You registered successfully, Wait for your account activation";
} else if (isset($_GET['s']) AND $_GET['s']=='changed'){ 
  echo "Your password changed successfully, Please login";
}


?>
        </div>
		
      <form action="" method="post" id="frmLogin">

        <div class="input-group mb-3">
          <input class = "form-control" 
               name="member_name" type="text" placeholder="Username"
                value="<?php if(isset($_COOKIE["member_login"])) { echo $_COOKIE["member_login"]; } ?>" required autofocus>
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-user"></span>
            </div>
          </div>
        </div>
        <div class="input-group mb-3">
          <input type = "password" class = "form-control"
               name = "member_password" placeholder="Password" value="<?php if(isset($_COOKIE["member_password"])) { echo $_COOKIE["member_password"]; } ?>">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-8">
          <div class="error-message" style="color:red;"><?php if(isset($message)) { echo $message; } ?></div>

            <div class="icheck-primary">
				<input type="checkbox" name="remember" id="remember"
                <?php if(isset($_COOKIE["member_login"])) { ?> checked
                <?php } ?>  />				
           <label for="remember">
                Remember Me
                </label>
                <label>
              <a href="register.php">Dont have an account?</a>
              </label>
                 <label>
              <a href="forget-password.php">Reset Lost password?</a>
              </label>
            </div>
          </div>
          <!-- /.col -->
          <div class="col-4">
            <button type="submit" name="login" value="Login" class="btn btn-primary btn-block">Sign In</button>
          </div>
          <!-- /.col -->
        </div>
      </form>
 
    </div>
    <!-- /.login-card-body -->
  </div>
</div>
<!-- /.login-box -->

<!-- jQuery -->
<script src="plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>

</body>
</html>
