<?php 
session_start();

require ('dbconnect.php');
$user_id = $_SESSION["member_id"];
$access_PICU_patients=[0,2,3,4];
$access_PICU_endorsement=[0,2,4];
$access_PICU_control=[0];
$access_PICU_registrar=[0,2];
$access_PICU_consultant=[0,2,3];

$formationSQL = "SELECT * FROM members WHERE member_id = '".$user_id."'";
$result1 = $mysqli->query($formationSQL);
$user = $result1 -> fetch_array(MYSQLI_ASSOC);

$username1=$user['member_name'];
$_SESSION['position']=$user['position'];
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <?php
//    if(time() - $_SESSION['timestamp'] > 600) { //subtract new timestamp from the old one
//     echo"<script>alert('15 Minutes over!');</script>";
//     unset($_SESSION['username'], $_SESSION['password'], $_SESSION['timestamp']);
//     $_SESSION['logged_in'] = false;
//     header("Location: index.php"); //redirect to index.php
//     exit;
// } else {
//     $_SESSION['timestamp'] = time(); //set new timestamp
// }

  ?>
  <meta charset="utf-8">
  <!-- <meta name="viewport" content="width=1024"> -->
 <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">

  <title>DMC | Dashboard</title>

 
   <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
      <!-- Icons font CSS-->
    <link href="vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">
    <link href="vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
    
  <!-- overlayScrollbars -->
  <!-- <link rel="stylesheet" href="plugins/overlayScrollbars/css/OverlayScrollbars.min.css"> -->
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
	 
    <!-- Vendor CSS-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
    
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.js"></script>


    <link href="vendor/datepicker/daterangepicker.css" rel="stylesheet" media="all">
	 <!-- Main CSS-->
    <link href="css/main.css" rel="stylesheet" media="all">

<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.js"> </script>

<!-- <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script> -->
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

<script src="http://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.2/modernizr.js"></script>
<script>
	//paste this code under head tag or in a seperate js file.
	// Wait for window load
	$(window).load(function() {
		// Animate loader off screen
		$(".se-pre-con").fadeOut("slow");;
	});
</script>

<?php 

if(isset($_SESSION["name"]))  
{  

     if((time() - $_SESSION['last_login_timestamp']) > 600) // 900 = 15 * 60  
     {  
      echo "<script language='javascript'>\n";
      echo "alert('You have been In-Active for long time. Re-login...')\n";
      echo "window.location.href = 'logout.php';";
      echo "</script>\n"; 
      echo time() - $_SESSION['last_login_timestamp'];
     }  
     else  
     {  
          $_SESSION['last_login_timestamp'] = time();  

     }  
}  
else  
{  
  echo "<script language='javascript'>\n";
  echo "window.location.href = 'logout.php';";
  echo "</script>\n"; 
 
} 

     
     ?>
     
<style>


.select2-container--default .select2-selection--multiple .select2-selection__rendered li {
    list-style: none;
    color: black;
    
}
.select2-container--default .select2-selection--single .select2-selection__rendered {

    line-height: 15px;
}
.select2-container--default .select2-selection--multiple .select2-selection__choice__remove {
    cursor: pointer;
    display: contents;
    font-weight: bold;
    margin-right: 2px;
}
.cancelBtn{
color: black;
}
input{
  text-align: center;
    background-color: white;
    border: 1px solid #aaa;
    border-radius: 4px;
    cursor: text;
}
textarea{
  width:100%;
  padding-left: 2%;
    padding-right: 2%;
    background-color: white;
    border: 1px solid #aaa;
    border-radius: 4px;
    cursor: text;
}
label{
  width: 100%;
}

.no-js #loader { display: none;  }
.js #loader { display: block; position: absolute; left: 100px; top: 0; }
.se-pre-con {
	position: fixed;
	left: 0%;
	top: 0px;
	width: 100%;
	height: 100%;
	z-index: 100;
	background: url(dist/img/Preloader_3.gif) center no-repeat #fff;
}

</style>	
</head>
<body class="hold-transition sidebar-mini layout-fixed layout-navbar-fixed layout-footer-fixed">

<div class="wrapper">
    
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
    <a class="nav-item nav-link" data-widget="pushmenu" href="#" role="button">
      <li class="nav-item fas fa-bars">
        
        
        
      </li></a>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="dashboard.php" class="nav-link">Home</a>
      </li>
		<!--
      <li class="nav-item d-none d-sm-inline-block">
        <a href="#" class="nav-link">Contact</a>
      </li>
	  //-->
    </ul>

    <!-- SEARCH FORM -->
	  <!--
    <form class="form-inline ml-3">
      <div class="input-group input-group-sm">
        <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
        <div class="input-group-append">
          <button class="btn btn-navbar" type="submit">
            <i class="fas fa-search"></i>
          </button>
        </div>
      </div>
    </form>
//-->
	  
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="dashboard.php" class="brand-link">
      <img src="dist/img/logo-wt.png" alt="healthpro.Ai" width="110%" class="brand-image elevation-1"
           style="opacity: 1">
    
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="dist/img/minilogo.png" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <p style="color: white;font-weight: bolder;text-transform: capitalize;font-size: large;" class="d-block"> <?php echo $username1;?> </p>
          <a href="profile.php" class="d-block"> Edit Profile </a>

        


        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
         <li class="nav-item">
            <a href="dashboard.php" class="nav-link">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="active-list.php" target="_blank" class="nav-link">
              <i class="nav-icon fas  fa-book-open"></i>
              <p>
                Active Patients
              </p>
            </a>
          </li>
          <li class="nav-item">
    <a href="tb-patients.php" class="nav-link">
      <i class="nav-icon fas fa-book"></i>
      <p>
      Active TB Patients
      </p>
    </a>
</li>
         
		 <li class="nav-item">
            <a href="dmc-patients.php" class="nav-link">
              <i class="nav-icon fas fa-edit"></i>
              <?php
              if (!in_array($user['position'],$access_PICU_endorsement)){
              echo "<p>
              My Patients
              </p>";}else
              {echo "<p>
                All Patients
              </p>";}
              ?>
            </a>
      </li>
      
      <?php 
      // echo $_SESSION["name"];
    
      if (in_array($user['position'],$access_PICU_patients)){
         ?>

      <li class="nav-item">
            <a href="dmc-new-consultation.php" class="nav-link">
              <i class="nav-icon fas fa-edit"></i>
              <?php
              if (!in_array($user['position'],$access_PICU_endorsement)){
              echo "<p>
              My Consultations
              </p>";}else
              {echo "<p>
                All Consultations
              </p>";}
              ?>
            </a>
      </li>
      <li class="nav-item">
            <a href="dmc-new-admissions.php" class="nav-link">
              <i class="nav-icon fas fa-edit"></i>
              <p>
                New admissions
              </p>
            </a>
      </li>
          <?php 
       }
        if (in_array($user['position'],$access_PICU_control)){
          ?>

<li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#cmenu" role="button" aria-expanded="false" aria-controls="cmenu" >
              <i class="nav-icon fas fa-book"></i>
              <p>
              Registry
              </p>
            </a>
 </li>
 <div class="collapse" id="cmenu" style="background-color: rgb(255 255 255 / 7%);">
 <li class="nav-item">
            <a href="search.php" class="nav-link">
              <i class="nav-icon fas fa-search"></i>
              <p>
              Registry Search
              </p>
            </a>
 </li>
<li class="nav-item">
            <a href="48discharge.php" class="nav-link">
              <i class="nav-icon fas fa-book"></i>
              <p>
              Recent Discharges
              </p>
            </a>
 </li>
<li class="nav-item">
    <a href="48consultation.php" class="nav-link">
      <i class="nav-icon fas fa-book"></i>
      <p>
      Recent Consultations
      </p>
    </a>
</li>
<li class="nav-item">
    <a href="registry-tb-patients.php" class="nav-link">
      <i class="nav-icon fas fa-book"></i>
      <p>
      TB Registry
      </p>
    </a>
 </li>
<!--<li class="nav-item">
        <a href="longterm.php" class="nav-link">
          <i class="nav-icon fas fa-procedures"></i>
          <p>
            Long Term Patients
          </p>
        </a>
</li> -->
</div>

<li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#cmenu1" role="button" aria-expanded="false" aria-controls="cmenu1" >
              <i class="nav-icon fas fa-chart-bar"></i>
              <p>
              Statistics
              </p>
            </a>
 </li>
 <div class="collapse" id="cmenu1" style="background-color: rgb(255 255 255 / 7%);">
<li class="nav-item">
        <a href="statistics.php" class="nav-link">
          <i class="nav-icon fas fa-chart-bar"></i>
          <p>
            Physician Stats
          </p>
        </a>
</li>
<li class="nav-item">
        <a href="allstat.php" class="nav-link">
          <i class="nav-icon fas fa-chart-bar"></i>
          <p>
            Overall Stats
          </p>
        </a>
</li>
 </div>
<li class="nav-item">
        <a href="dmc-old-patients.php" class="nav-link">
          <i class="nav-icon fas fa-cog"></i>
          <p>
            Add old Patients
          </p>
        </a>
</li>
<li class="nav-item">
        <a href="control.php" class="nav-link">
          <i class="nav-icon fas fa-cog"></i>
          <p>
            Control Page
          </p>
        </a>
</li>

<?php }
?>
 
			 <li class="nav-item">
            <a href="logout.php" class="nav-link">
              <i class="nav-icon fas fa-sign-out-alt"></i>
              <p>
                Logout
              </p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <div class="se-pre-con"></div>
